### 问题现象

```
-- LIBOMPTARGET: Not building NVPTX deviceRTL by default on CUDA free system.
-- Clang version: 12.0.1
-- LLD version: 12.0.1
-- Enable editline support in LLDB: FALSE
-- Enable curses support in LLDB: TRUE
-- Enable LZMA compression support in LLDB: TRUE
-- Could NOT find Lua (missing: LUA_LIBRARIES LUA_INCLUDE_DIR) (Required is at least version "5.3")
-- Could NOT find LuaAndSwig (missing: LUA_LIBRARIES LUA_INCLUDE_DIR)
-- Enable Lua scripting support in LLDB: FALSE
-- Could NOT find Python3 (missing: Python3_LIBRARIES Python3_INCLUDE_DIRS Development) (found version "3.8.0")
CMake Error at /home/liwentao/xuyao/llvm-12/prebuilts/cmake/linux-x86/share/cmake-3.16/Modules/FindPackageHandleStandardArgs.cmake:146 (message):
  Could NOT find PythonAndSwig (missing: Python3_LIBRARIES
  Python3_INCLUDE_DIRS)
Call Stack (most recent call first):
  /home/liwentao/xuyao/llvm-12/prebuilts/cmake/linux-x86/share/cmake-3.16/Modules/FindPackageHandleStandardArgs.cmake:393 (_FPHSA_FAILURE_MESSAGE)
  /home/liwentao/xuyao/llvm-12/toolchain/llvm-project/lldb/cmake/modules/FindPythonAndSwig.cmake:71 (find_package_handle_standard_args)
  /home/liwentao/xuyao/llvm-12/toolchain/llvm-project/lldb/cmake/modules/LLDBConfig.cmake:48 (find_package)
  /home/liwentao/xuyao/llvm-12/toolchain/llvm-project/lldb/cmake/modules/LLDBConfig.cmake:59 (add_optional_dependency)
  /home/liwentao/xuyao/llvm-12/toolchain/llvm-project/lldb/CMakeLists.txt:21 (include)


-- Configuring incomplete, errors occurred!
See also "/home/liwentao/xuyao/llvm-12/out/llvm_make/CMakeFiles/CMakeOutput.log".
See also "/home/liwentao/xuyao/llvm-12/out/llvm_make/CMakeFiles/CMakeError.log".
Traceback (most recent call last):
  File "build.py", line 1848, in <module>
    main()
  File "build.py", line 1796, in main
    buildConfig.xunit_xml_output)
  File "build.py", line 473, in llvm_compile
    extra_env=llvm_extra_env)
  File "build.py", line 367, in build_llvm
    env=env)
  File "build.py", line 220, in invoke_cmake
    self.check_call([cmake_bin_path] + flags, cwd=out_path, env=env)
  File "build.py", line 266, in check_call
    subprocess.check_call(cmd, *args, **kwargs)
  File "/usr/lib/python2.7/subprocess.py", line 190, in check_call
    raise CalledProcessError(retcode, cmd)
subprocess.CalledProcessError: Command '['/home/liwentao/xuyao/llvm-12/prebuilts/cmake/linux-x86/bin/cmake', '-G', 'Ninja', '-DCMAKE_PREFIX_PATH=/hom-DCOMPILER_RT_USE_BUILTINS_LIBRARY=ON', '-DLIBCXXABI_USE_LLVM_UNWINDER=YES', '-DCMAKE_SHARED_LINKER_FLAGS=-fuse-ld=lld -L/home/liwentao/xuyao/llvm-12lunwind --rtlib=compiler-rt -stdlib=libc++', '-DLLDB_ENABLE_PYTHON=ON', '-DLIBCXXABI_USE_COMPILER_RT=ON', '-DLLVM_ENABLE_ASSERTIONS=OFF', '-DCMAKE_CXDER=ON', '-DCMAKE_MODULE_LINKER_FLAGS=-fuse-ld=lld -L/home/liwentao/xuyao/llvm-12/prebuilts/clang/harmonyos/linux-x86_64/clang-10.0.1/lib -lunwind --ING=llvm-project', '-DCLANG_VENDOR_BUILD_VERSION=', '-DLLVM_ENABLE_TERMINFO=OFF', '-DCOMPILER_RT_BUILD_XRAY=OFF', '-DLLVM_ENABLE_THREADS=ON', '-DLLVMo/xuyao/llvm-12/prebuilts/clang/harmonyos/linux-x86_64/clang-10.0.1/bin/clang++', '-DEXECUTION_ENGINE_USE_LLVM_UNWINDER=1', '-DLIBCXX_ENABLE_ABI_LINKLTO=Thin', '-DCMAKE_BUILD_TYPE=Release', '-DLLVM_ENABLE_LIBCXX=ON', '-DOPENMP_TEST_FLAGS=-Wl,-ldl', '-DLIBCXX_ENABLE_STATIC_ABI_LIBRARY=ON', '-DLLVM_ler-rt;clang-tools-extra;openmp;lldb', '-DLLDB_ENABLE_LIBEDIT=OFF', '-DCOMPILER_RT_BUILD_SANITIZERS=OFF', '-DCMAKE_C_COMPILER=/home/liwentao/xuyao/llbin/clang', '-DCLANG_VENDOR=OHOS (dev) ', '-DCMAKE_C_FLAGS=', '-DLLVM_BINUTILS_INCDIR=/usr/include', '-DLLVM_USE_NEWPM=ON', '-DCMAKE_EXE_LINKER_FLAGSng/harmonyos/linux-x86_64/clang-10.0.1/lib -lunwind --rtlib=compiler-rt -stdlib=libc++', '-DCOMPILER_RT_BUILD_LIBFUZZER=ON', '-DLIBCXX_USE_COMPILER_RPROF=OFF', '-DCMAKE_INSTALL_PREFIX=/home/liwentao/xuyao/llvm-12/out/llvm-install', '-DCLANG_BUILD_EXAMPLES=OFF', '-DLLVM_ENABLE_BINDINGS=OFF', '-DLLV_STATIC_LIBRARY=YES', '-DLIBOMP_ENABLE_SHARED=FALSE', '-DCMAKE_ASM_FLAGS=', '-DSANITIZER_ALLOW_CXXABI=OFF', '-DLLVM_TARGETS_TO_BUILD=AArch64;ARM;BPF;yao/llvm-12/toolchain/llvm-project/llvm']' returned non-zero exit status 1
```

### 解决办法

https://gitee.com/openharmony-sig/third_party_llvm-project/issues/I5I25A?from=project-issue

```
sudo apt-get install build-essential swig python3-dev libedit-dev libncurses5-dev binutils-dev
sudo apt-get remove python3.8
sudo apt-get -y purge python3.8
sudo apt-get -y autoremove
```

