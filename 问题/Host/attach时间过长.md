[LLDB] [HOS] attach时间过长问题 · Issue #I5IG24 · OpenHarmony/third_party_llvm-project - Gitee.com

解决方案
1. 应用自身加载的so较多较大，LLDB需要pull到本地进行符号查找等操作，因此attach耗时较长

2. 通过并行化pull so解决

3. 通过异步加载解决