[LLDB] fix repeating lock cache file with same uuid in different file path · Issue #I5WX0L · OpenHarmony/third_party_llvm-project - Gitee.com

解决方案

1. 重复加锁导致的卡死

2. 加锁最好通过trylock避免卡死

3. 通过增加判别，对于已经访问的ccache资源置invalid，避免重复加锁