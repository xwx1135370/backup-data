假如windows是你的服务端，执行
hdc_std kill
hdc_std -s 本地IP:PORT -m

linux是客户端
hdc_std -s Windows的IP:PORT shell

linux和windows上的HDC版本要一致

### 具体步骤参见以下示例 

1. 执行ipconfig查看windows（pc）上的ip
![ipconfig](https://foruda.gitee.com/images/1660704221915354331/屏幕截图.png "屏幕截图.png")

2. 在windows机器上运行hdc_std.exe kill
![hdc_std kill](https://foruda.gitee.com/images/1660704367186500939/屏幕截图.png "屏幕截图.png")

3. 在windows机器上（服务端）运行hdc_std.exe -s 本地IP:PORT -m
[图片上传中…(image-VMayF2OsHo9PPlGbmTVp)]
其中10.190.234.185为步骤1查询的ip，端口号为任意一个可用端口即可。

```
D:\hdc_std_1_1_2>hdc_std.exe -s 10.190.234.185:1234 -m
[I][2022-08-17 10:48:51.284][33f4][session.cpp:22] Program running. Ver: 1.1.2a Pid:14524
[I][2022-08-17 10:48:51.306][33f4][session.cpp:30] set UV_THREADPOOL_SIZE:128
[D][2022-08-17 10:48:51.307][33f4][session.cpp:41] loopMain init
[D][2022-08-17 10:48:51.343][33f4][base.cpp:820] Write mutext to C:\Users\XWX113~1\AppData\Local\Temp\.HDCServer.pid, pid:14524
[D][2022-08-17 10:48:51.408][33f4][server_for_client.cpp:80] channelHost ::ffff:10.190.234.185, port: 1234
[D][2022-08-17 10:48:51.411][33f4][host_usb.cpp:53] HdcHostUSB init
[D][2022-08-17 10:48:51.412][33f4][host_usb.cpp:255] USBHost loopfind mode
[D][2022-08-17 10:48:51.413][33f4][host_uart.cpp:497] StartupUARTWork
[D][2022-08-17 10:48:51.415][33f4][host_uart.cpp:456] StartUartSendThread.
[I][2022-08-17 10:48:51.415][33f4][host_uart.cpp:464] StartUartSendThread success.
[D][2022-08-17 10:48:51.416][3ed4][host_uart.cpp:60] UartWriteThread wait sendLock.
[W][2022-08-17 10:48:51.426][33f4][host_usb.cpp:91] libusb: warning [enumerate_hcd_root_hub] could not infer VID/PID of HCD root hub from 'ROOT\USB\0000'
[W][2022-08-17 10:48:51.427][33f4][host_usb.cpp:91] libusb: warning [enumerate_hcd_root_hub] could not infer VID/PID of HCD root hub from 'ROOT\USB\0000'
```
4. linux机器上执行./hdc_std -s Windows的IP:PORT shell
linux环境ip（7.189.3.108）

```
$ ll
total 13784
drwxr-xr-x  3 xuyao root       66 Aug 17 10:55 ./
drwxr-xr-x 12 xuyao root      226 Aug 17 10:55 ../
drwxr-xr-x  6 xuyao root      318 Aug 17 10:55 toolchains/
-rw-r--r--  1 xuyao root 14112102 Aug 17 10:54 toolchains-linux-3.2.5.5-Beta2.zip
$ cd toolchains/
$ ll
total 15240
drwxr-xr-x 6 xuyao root     318 Aug 17 10:55 ./
drwxr-xr-x 3 xuyao root      66 Aug 17 10:55 ../
-rwxr-xr-x 1 xuyao root 1960784 Jan  1  2001 ark_asm*
-rwxr-xr-x 1 xuyao root 4686264 Jan  1  2001 ark_disasm*
drwxr-xr-x 2 xuyao root      66 Aug 17 10:55 configcheck/
-rwxr-xr-x 1 xuyao root 3140472 Jan  1  2001 hdc_std*
-rw-r--r-- 1 xuyao root  104119 Jan  1  2001 id_defined.json
-rwxr-xr-x 1 xuyao root  236744 Jan  1  2001 idl*
drwxr-xr-x 2 xuyao root     267 Aug 17 10:55 lib/
-rwxr-xr-x 1 xuyao root  167944 Jan  1  2001 libpcre2.so*
-rwxr-xr-x 1 xuyao root  162176 Jan  1  2001 libselinux.so*
-rwxr-xr-x 1 xuyao root  811936 Jan  1  2001 libsepol.so*
-rwxr-xr-x 1 xuyao root  100784 Jan  1  2001 libusb_shared.so*
drwxr-xr-x 2 xuyao root     149 Aug 17 10:55 modulecheck/
-rw------- 1 xuyao root  465824 Jan  1  2001 NOTICE.txt
-rw-r--r-- 1 xuyao root     172 Jan  1  2001 oh-uni-package.json
-rwxr-xr-x 1 xuyao root 3562424 Jan  1  2001 restool*
drwxr-xr-x 2 xuyao root      31 Aug 17 10:55 syscapcheck/
-rwxr-xr-x 1 xuyao root  179744 Jan  1  2001 syscap_tool*
$ ./hdc_std -s 10.190.234.185:1234 shell
#

```