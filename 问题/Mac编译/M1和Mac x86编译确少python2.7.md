### 【问题现象】

![输入图片说明](https://foruda.gitee.com/images/1695641509083170324/41c5b86d_10384282.png "屏幕截图")

### 【问题分析】
![输入图片说明](https://foruda.gitee.com/images/1695641538525166545/df127ed4_10384282.png "屏幕截图")


### 【问题解决】
设置临时环境变量
export PATH="/Users/compiler/.pyenv/versions/2.7.18/bin:$PATH"

编译：`python3 ./toolchain/llvm-project/llvm-build/build.py --strip --build-lldb-static --build-python --build-ncurses --build-libedit --build-libxml2 --lldb-timeout`