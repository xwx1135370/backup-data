###  1. Sunlogin Remote Control

https://gitee.com/xwx1135370/llvm-related-issues/blob/master/%E5%B7%A5%E5%85%B7/SunloginClient_12.5.2.46968_x64.zip


 **Identification code** :  173 957 834
 **Verification code** :    cnp200
![输入图片说明](https://foruda.gitee.com/images/1691661825866003373/0781590a_10384282.png "屏幕截图")

Computer pwd: Hsu1995.08.24

### 2. steps

- Logging into the environment using MobaXtem （IP：123.60.114.105 useName:xuyao)

- Run `ssh l30044133@123.60.114.147 -p 2222`

- Run `sudo docker ps`

- Run `sudo docker exec -it 2fcfc911f4f1 /bin/bash` to enter Docker environment.

- Run `cd llvm/out/llvm_make/` 

    ![输入图片说明](https://foruda.gitee.com/images/1691662326197355734/96570437_10384282.png "屏幕截图")

- Run `../../prebuilts/cmake/linux-x86/bin/ninja -d stats -d explain -v check-lldb-unit ` . Will get stuck when running the `cd /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit && /usr/bin/python3.8 /home/openharmony/llvm/out/llvm_make/./bin/llvm-lit -sv /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit` command.

- Try to run `cd /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit && /usr/bin/python3.8 /home/openharmony/llvm/out/llvm_make/./bin/llvm-lit -vv /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit > output.txt` or 
`cd /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit && /usr/bin/python3.8 /home/openharmony/llvm/out/llvm_make/./bin/llvm-lit -av /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit > output.txt` get more information.
  
   We can observe the test cases of pass through output.txt. Through output.txt, it is possible to observe the test cases of pass and find that the test cases are executed to “PASS: lldb unit:: Process/gdb remote// ProcessGdbRemoteTests/8/37 (1042 of 1044)” will no longer only require. （As shown in files [output-av.txt](https://gitee.com/xwx1135370/llvm-lit/blob/master/2023-08-10-hsu/output-av.txt) and [output-vv.txt](https://gitee.com/xwx1135370/llvm-lit/blob/master/2023-08-10-hsu/output-vv.txt)）

   /home/openharmony/llvm/out/llvm_make/tools/lldb/unittests/Process/gdb-remote内容详见[gdb-remote](https://gitee.com/xwx1135370/llvm-lit/tree/master/2023-08-10-hsu/unittests/Process/gdb-remote)

 


NOTE：

https://gitee.com/liwentao_uiw/llvmopen-source-transfer-gitee/blob/ohos_toolchain/docker/Dockerfile


https://gitee.com/liwentao_uiw/llvmopen-source-transfer-gitee/blob/ohos_toolchain/llvm_test_script/run_docker.sh


Try:

1. Comment out `TEST_ F (GDBRemoteClientBaseTest, SendPacketAndReceiveResponseWithOutputSupport)` [https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/unittests/Process/gdb-remote/GDBRemoteClientBaseTest.cpp#L345]

--> "PASS: lldb-unit :: Process/gdb-remote/./ProcessGdbRemoteTests/8/36 (1042 of 1043)" 

2. Comment out `TEST_ F (GDBRemoteClientBaseTest, SendPacketAndReceiveResponseWithOutputSupport) `[https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/unittests/Process/gdb-remote/GDBRemoteClientBaseTest.cpp#L345] and `TEST_F(GDBRemoteClientBaseTest, InterruptNoResponse)`[https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/unittests/Process/gdb-remote/GDBRemoteClientBaseTest.cpp#L323]

--> "PASS: lldb-unit :: Host/./HostTests/26/76 (1040 of 1042)"

3. Single threaded：

`cd /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit && /usr/bin/python3.8 /home/openharmony/llvm/out/llvm_make/./bin/llvm-lit -vv --threads=1 /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit`

4. Single threaded ，Specify Execution Order
 
`cd /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit && /usr/bin/python3.8 /home/openharmony/llvm/out/llvm_make/./bin/llvm-lit -a --order=lexical --threads=1 /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit` 

5. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'API')
    ```
   ../../prebuilts/cmake/linux-x86/bin/ninja clean
   ../../prebuilts/cmake/linux-x86/bin/ninja -d stats -d explain -v check-lldb-unit
    ```   
  --> OK
![输入图片说明](https://foruda.gitee.com/images/1691725547716027040/7d429f68_10384282.png "屏幕截图")

6. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Breakpoint')
   --> OK
![输入图片说明](https://foruda.gitee.com/images/1691725931816160511/a438e4e6_10384282.png "屏幕截图")
  
7.  Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Core')
   --> OK
![输入图片说明](https://foruda.gitee.com/images/1691726781671697326/ea3e941b_10384282.png "屏幕截图")

8.  Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'DataFormatter')
  --> Ok
![输入图片说明](https://foruda.gitee.com/images/1691730312647708876/ed30a0b1_10384282.png "屏幕截图")

9.  Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Disassembler')
  -->Ok
  ![输入图片说明](https://foruda.gitee.com/images/1691730709310729996/1fc1bb0f_10384282.png "屏幕截图")

10.  Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Editline')
  --> `cd /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit && /usr/bin/python3.8 /home/openharmony/llvm/out/llvm_make/./bin/llvm-lit -vv --threads=1 /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit` failed
  ![输入图片说明](https://foruda.gitee.com/images/1691734451878043574/954b0698_10384282.png "屏幕截图")

11. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Expression')`
  --> Ok
![输入图片说明](https://foruda.gitee.com/images/1691734727582620197/bbece04c_10384282.png "屏幕截图")

12. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Host')`
  --> Ok
![输入图片说明](https://foruda.gitee.com/images/1691735011125401063/d5e018f1_10384282.png "屏幕截图")

13. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Instruction')`
  --> Ok
![输入图片说明](https://foruda.gitee.com/images/1691735253742394394/875ae355_10384282.png "屏幕截图")

14. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Interpreter')`
   -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691735534466163055/88573883_10384282.png "屏幕截图")

15. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Language')`
  -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691735809345280909/c662bf28_10384282.png "屏幕截图")

16. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'ObjectFile')`
  -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691736080265917055/cc6092f9_10384282.png "屏幕截图")

17. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Platform')`
  -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691736306510822436/01b5c539_10384282.png "屏幕截图")

18. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Process')`
  -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691736551617373228/09df89b9_10384282.png "屏幕截图")

19. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'ScriptInterpreter')`
  -->failed
 ![输入图片说明](https://foruda.gitee.com/images/1691737094970931303/085aab8e_10384282.png "屏幕截图")

20. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Signals')`
  -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691737591761369901/88a1961a_10384282.png "屏幕截图")

21. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Symbol')`
  -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691737849093134160/517d1a09_10384282.png "屏幕截图")

22. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'SymbolFile')`
  -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691738082926958490/f2c8c8b3_10384282.png "屏幕截图")

23. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Target')`
  -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691738335742128065/4a28b8e9_10384282.png "屏幕截图")

24. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'TestingSupport')`
  -->Failed
![输入图片说明](https://foruda.gitee.com/images/1691738580676752619/19d36991_10384282.png "屏幕截图")

25. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Thread')`
  -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691738843842494255/4241a4ca_10384282.png "屏幕截图")

26. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'UnwindAssembly')`
  -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691739079608776730/17d3d4de_10384282.png "屏幕截图")

27. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'Utility')`
  -->Ok
![输入图片说明](https://foruda.gitee.com/images/1691739418659152584/0135a988_10384282.png "屏幕截图")

29. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'debugserver')`
  -->Failed
![输入图片说明](https://foruda.gitee.com/images/1691739729478697426/b304e909_10384282.png "屏幕截图")

30. Change https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/test/Unit/lit.cfg.py#L19 to `config.test_source_root = os.path.join(config.lldb_obj_root, 'unittests', 'tools')`
  -->Stuck
![输入图片说明](https://foruda.gitee.com/images/1691740352644149680/d96fb879_10384282.png "屏幕截图")


31. Annotate the TEST_F in the [LLGSTest.cpp](https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/unittests/tools/lldb-server/tests/LLGSTest.cpp)
 --> Ok
![输入图片说明](https://foruda.gitee.com/images/1691742089412216700/6d5c289b_10384282.png "屏幕截图")

32. Annotate the first and third TEST_F in the [LLGSTest.cpp](https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/unittests/tools/lldb-server/tests/LLGSTest.cpp)
--> Ok
![输入图片说明](https://foruda.gitee.com/images/1691743245813182881/1787787f_10384282.png "屏幕截图")

32. Using ubuntu22.04 environment
  --> Ok
  ![输入图片说明](https://foruda.gitee.com/images/1691984233989448003/f2db1272_10384282.png "屏幕截图") 