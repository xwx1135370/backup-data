### 修改dockerfile，使用non-root

```
FROM swr.cn-north-4.myhuaweicloud.com/ci-service/openharmony-release-build-env-jnlp:1.0.3
WORKDIR /home/openharmony
ENV DEBIAN_FRONTEND noninteractive

# Add custom Openharmony user and give him sudo permissions
ARG user=Openharmony
ARG group=Openharmony
ARG uid=1000
ARG gid=1000
RUN groupadd -g ${gid} ${group} && \
    useradd -c "Custom user" -d /home/${user} -u ${uid} -g ${gid} -m ${user} && \
    usermod -a -G root ${user} && \
    echo "${user}   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${user}

RUN git config --global user.email p_spbcompilers@huawei.com \
        && git config --global user.name p_spbcompilers \
        && git config --global --add safe.directory '*' \
        && git config --global http.postBuffer 524288000 \
        && git config --global http.sslVerify false \
        && git config --global credential.helper store


ENV LANG=en_US.UTF-8 LANGUAGE=en_US.UTF-8 LC_ALL=en_US.UTF-8
```

### 使用新镜像跑门禁

- 下载镜像：docker pull swr.cn-north-4.myhuaweicloud.com/ci-service/openharmony-testcodearts-build-env-jnlp:1.0.3

- 创建带特权(--privileged)的恒久docker：`docker run -d --privileged=true -v /home/dwb/workspace/HSU/llvm-test-ci:/home/Openharmony -t swr.cn-north-4.myhuaweicloud.com/ci-service/openharmony-testcodearts-build-env-jnlp:1.0.3 /bin/bash`

- 运行docker：`docker exec -it <container_id> /bin/bash`

- 下载run_docker.sh: `wget https://gitee.com/liwentao_uiw/llvmopen-source-transfer-gitee/raw/ohos_toolchain/llvm_test_script/run_docker.sh`

- 下载llvm源码

- 运行run_docker.sh

- [运行结果](https://gitee.com/xwx1135370/llvm-lit/tree/master/2023-08-21)

### 分析交流：
![输入图片说明](https://foruda.gitee.com/images/1692668564858240365/90f15238_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1692668628165729063/0feb584d_10384282.png "屏幕截图")
```
export LD_LIBRARY_PATH="${PROJECT_OUT_PATH}/llvm_make/lib:${PROJECT_OUT_PATH}/llvm_make/lib/x86_64-unknown-linux-gnu:${PROJECT_PATH}/prebuilts/libedit/linux-x86/lib"
```
