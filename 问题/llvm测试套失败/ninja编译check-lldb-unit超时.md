
![输入图片说明](https://foruda.gitee.com/images/1691630989925454373/31da8239_10384282.png "屏幕截图")
编译check-lldb-unit超时可能有多种原因，以下是一些可能的原因：

1. 机器性能不足：编译check-lldb-unit需要大量的计算资源，如果机器性能不足，编译过程可能会超时。

2. 编译环境配置不当：如果编译环境配置不当，可能会导致编译过程出现问题，从而导致超时。

3. 编译过程中出现死锁：如果编译过程中出现死锁，可能会导致编译过程超时。

4. 编译过程中出现错误：如果编译过程中出现错误，可能会导致编译过程超时。

 **出错点：** 

https://gitee.com/liwentao_uiw/llvmopen-source-transfer-gitee/blob/ohos_toolchain/llvm_test_script/run_docker.sh#L115

尝试将 
```
../../prebuilts/cmake/linux-x86/bin/ninja check-lldb-unit | tee ${llvmts_logdir}/lldb_unit_${TIME}.txt
```
替换为
```
../../prebuilts/cmake/linux-x86/bin/ninja -d stats -d explain -v check-lldb-unit | tee ${llvmts_logdir}/lldb_unit_${TIME}.txt
```
日志：[lldb-unit](https://gitee.com/xwx1135370/llvm-lit/blob/master/2023-08-10-hsu/lldb-unit.txt)

![输入图片说明](https://foruda.gitee.com/images/1691651036801755920/3ee6dfb4_10384282.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1691651056484069075/fd74c340_10384282.png "屏幕截图")


卡在了
```
cd /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit && /usr/bin/python3.8 /home/openharmony/llvm/out/llvm_make/./bin/llvm-lit -sv /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit
```


手动执行
```
cd /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit && /usr/bin/python3.8 /home/openharmony/llvm/out/llvm_make/./bin/llvm-lit -sv /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit > output.txt
```
从output.txt中只能看到
![输入图片说明](https://foruda.gitee.com/images/1691648417650713134/9297cb3a_10384282.png "屏幕截图")

手动执行

```
cd /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit && /usr/bin/python3.8 /home/openharmony/llvm/out/llvm_make/./bin/llvm-lit -vv /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit > output.txt
```
获取到的[output.txt](https://gitee.com/xwx1135370/llvm-lit/blob/master/2023-08-10-hsu/output-vv.txt)

手动执行
```
cd /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit && /usr/bin/python3.8 /home/openharmony/llvm/out/llvm_make/./bin/llvm-lit -a /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit > output.txt
```
获取到的[output.txt](https://gitee.com/xwx1135370/llvm-lit/blob/master/2023-08-10-hsu/output-a.txt)

手动执行
```
cd /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit && /usr/bin/python3.8 /home/openharmony/llvm/out/llvm_make/./bin/llvm-lit -av /home/openharmony/llvm/out/llvm_make/tools/lldb/test/Unit > output.txt
```
获取到的[output.txt](https://gitee.com/xwx1135370/llvm-lit/blob/master/2023-08-10-hsu/output-av.txt)


/home/openharmony/llvm/out/llvm_make/tools/lldb/unittests/Process/gdb-remote内容详见[gdb-remote](https://gitee.com/xwx1135370/llvm-lit/tree/master/2023-08-10-hsu/unittests/Process/gdb-remote)
