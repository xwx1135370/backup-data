## 问题现象
![输入图片说明](https://foruda.gitee.com/images/1701929878059835819/a267559a_10384282.png "屏幕截图")

## 使用背景
https://gitee.com/liwentao_uiw/llvmopen-source-transfer-gitee/pulls/22
```
./run_docker.sh -t checksec -s
```

## 分析处理
从报错看，是在`check.py`脚本的第6行 "import jinja2"出的问题。找不到'jinja2'
![输入图片说明](https://foruda.gitee.com/images/1701930040211581706/15522104_10384282.png "屏幕截图")
查看静态编译时代码结构，可以看到在third_party目录下有jinja2。可以将此路径加载到搜索路径下。
![输入图片说明](https://foruda.gitee.com/images/1701930195529435246/6dfbb389_10384282.png "屏幕截图")
处理为
```
import sys
sys.path.append('./third_party')
import jinja2
```
![输入图片说明](https://foruda.gitee.com/images/1701930278023408785/89c92980_10384282.png "屏幕截图")

手动执行`/home/openharmony/fuzztrst`目录的`check.py`脚本。报错消除。

重新跑静态编译命令`./run_docker.sh -t checksec -s`.又报同样错。查看代码，发现/home/openharmony/fuzztrst/check.py修改没有了。确认库代码逻辑。发现run_docker.sh中有如下处理逻辑。会将库中的check.py覆盖到工作目录。因此需要修改`/home/openharmony/fuzztrst/llvmopen-source-transfer-gitee/llvm_test_script/checksec/check.py`才能生效。

![输入图片说明](https://foruda.gitee.com/images/1701930483854949269/fc1b6fae_10384282.png "屏幕截图")
