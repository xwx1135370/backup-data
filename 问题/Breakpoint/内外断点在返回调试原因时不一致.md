[[LLDB] [DevEco Studio]Time travel debug](https://gitee.com/openharmony/third_party_llvm-project/issues/I64UD1?from=project-issue)

[⚙ D140368 [lldb] Consider all breakpoints in breakpoint detection (llvm.org)](https://reviews.llvm.org/D140368)

解决方案
1. 原生逻辑对于内部生成的断点将会按照step-out完成的停止原因返回，而不是设置断点导致的

2. 回退对于内部断点的判断，统一视为断点