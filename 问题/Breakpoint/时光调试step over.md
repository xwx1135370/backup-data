预期结果：step over（对应lldb的next）停在断点行时，通过lldbapi获取的当前线程StopReason是eStopReasonBreakpoint，时光调试依据该reason来判断之前stepover经过的断点位置并实现回退到历史断点的功能。
实际结果：在有一些情况下step over停在断点行时的StopReason有写时候是eStopReasonPlanComplete，和step over到非断点行返回的StopReason相同，导致时光调试时无法判断step over经过的位置是否为断点，影响回退到历史断点的功能。

目前没定位出来该问题在哪个lldb版本先出现，ohos sdk拿最新的有，拿3.2.5.5也有，拿api9和api8也都有，就拿我附上的那个源码很容易就复现出来。
我们这边是服务端直接调用lldb api的，使用的是SBThread.h的GetStopReason()这个函数返回的lldb::StopReason。
我们这边也尝试过阅读lldb源码，目前能找到一些相关的代码在Thread.cpp里，但是不了解具体的判断逻辑。
关于问题定界，我们在服务端直接打印了各个thread在停止时的StopReason，发现是lldb的返回不符合预期，因此定界为lldb侧问题。

```
#include <map>
#include <set>
#include <list>
#include <deque>
#include <iostream>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <initializer_list>
#include <forward_list>
#include <optional>
#include <bitset>
#include <variant>
#include <atomic>

using namespace std;

int global_var1_1 = 0;
static double global_var2_1 = 100;
static double global_var2_1_use = 789;
char global_var3_1 = 'h';

int test_switch (int num) {
    int num_sign = 0;
    switch (num)
    {
        case 10: num_sign = num + 1 ; break;
        case 20: num_sign = num + 2 ; break;
        case 30: num_sign = num + 3 ; break;
        default: num_sign = num + 10 ; break;
    }
    return num_sign;
}

int test_while () {
    int i = 0;
    while(i < 10)
    {
        i = i + 1;
    }
    return 0;
}


int test_dowhile() {
    int i = 1;
    do{
        int j = 1;
        do{
            j++;
        } while (j <= 3) ;
        i++;
    } while (i <= 3) ;
    return 0;
}

void func() {
    static int i=0; //static variable
    int j=0; //local variable
    i++;
    j++;
}

int test_use_func()
{
    func();   //i= 1 and j= 1
    func();   //i= 2 and j= 1
    func();   //i= 3 and j= 1
    return 0;
}

int test_for () {
    int num;
    for(int i=1;i<=3;i++){
        for(int j=1;j<=3;j++){
            num = i + j ;
        }
    }
    return 0;
}

//递归，测试stepin 和调用栈查看
int factorial(int n)
{
    if(n<0)
        return(-1); /*Wrong value*/
    if(n==0)
        return(1);  /*Terminating condition*/
    else
    {
        return(n*factorial(n-1)); //打断点测试stepin/stepout
    }
}

float division(int x, int y) {
    if( y == 0 ) {
        throw "Attempted to divide by zero!";
    }
    return (x/y);
}


int test_try_catch () {
    int i = 25;
    int j = 0;
    float k = 0;
    try {
        k = division(i, j);
    }catch (const char* e) {
        ;
    }
    return 0;
}

// 测试变量查看及修改
int test_var(){
    double setuse = global_var2_1_use;
    int fornu;
    for (int i = 0;i<5;i++){
        fornu = i;
    }
    static int n_static = 1350;
    int n_arry_1[7] = {1,2,3,4,5,6,7};
    double balance[100] = {50.0};
    int int_blank[30] = {};
    long long_blank[31] = {};
    float float_blank[29] = {78.89};
    char site[7] = {'R', 'U', 'N', 'O', 'O', 'B', '\0'};
    char str_1[] = {'C','+','+','\0'};
    char str_2[4] = {'C','+','+','\0'};
    char str_3[] = "a\0b";
    char str_4[100] = "C++";
    int salary = 85000;
    signed int x = 23;
    unsigned int x2 = 2; //测试修改成负整数是否允许
    char test = 'h';
    signed char  sin_1 = 12; //signed 范围是-127到127，测试正常范围及超范围修改现象
    unsigned char  unsin_1 = -85; //unsigned char的范围是0到255，测试正常范围及超范围修改现象
    float area = 64.74;
    double volume = 134.64534;
    bool cond = false;
    long b = 4523232;
    long int c = 2345342;
    long double d = 233434.56343;
    short a = 12345;
    return 0;
}

class Student{
private:
    char *m_name;
    int m_age;
    float m_score;
public:
    //声明构造函数
    Student(char *name, int age, float score);
    //声明普通成员函数
    void show();
};

//定义构造函数
Student::Student(char *name, int age, float score){
    m_name = name;
    m_age = age;
    m_score = score;
}

//定义普通成员函数
void Student::show(){
    cout<<m_name<<"的年龄是"<<m_age<<"，成绩是"<<m_score<<endl;
}

int test_class(){
    //创建对象时向构造函数传参
    Student stu((char*)"小明", 15, 92.5); //断点，测试构造函数的stepin
    stu.show(); //断点，测试普通成员函数的stepin

    //创建对象时向构造函数传参
    Student *pstu = new Student((char*)"李华", 16, 96); //断点，测试构造函数的stepin
    pstu -> show(); //断点，测试普通成员函数的stepin
    return 0;
}
double test_division(int a, int b)
{
    if( b == 0 )
    {
        throw "Division by zero condition!";
    }
    return (a/b);
}

void test_catch_division(int x, int y){
    double z = 0;
    try {
        z = test_division(x, y);
    }catch (const char* msg) {
        cerr << msg << endl;
    }
}


int main()
{
    int a=1;
    int num_switch = test_switch(20);
    test_while ();
    test_dowhile();
    test_use_func();
    factorial(10);
    division(10,90);
    test_try_catch ();
    test_var();
    test_class(); //断点位置，下一步
    // 设置异常断点，并且异常断点被触发
    test_catch_division(2,0);
    
    //    // 设置异常断点在test_while中的断点被击中后才有效
    test_while();
    //
    //    // 设置异常断点，并且异常断点被触发
    test_catch_division(2,0);
    //
    //    // 设置异常断点，但是异常断点未被触发
    test_catch_division(2,1);
    //
    //    // 自定义异常类，被触发
    //    test_catch_MyException();
}
```