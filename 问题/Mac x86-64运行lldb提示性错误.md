 **【问题现象】** 
2023年5月30号版本
![输入图片说明](https://foruda.gitee.com/images/1686194992246571662/d9247c94_10384282.png "屏幕截图")

20230330版本
![输入图片说明](https://foruda.gitee.com/images/1686195047786165211/dc8b9eb6_10384282.png "屏幕截图")

`https://repo.huaweicloud.com/harmonyos/compiler/clang/12.0.1-530132/darwin/clang-530132-darwin-x86_64.tar.bz2` 版本报错同20230530


`https://repo.huaweicloud.com/harmonyos/compiler/clang/10.0.1-447847/darwin/clang-447847-darwin-x86_64.tar.bz2` 版本报错
![输入图片说明](https://foruda.gitee.com/images/1686204978624967025/e64b0ed2_10384282.png "屏幕截图")