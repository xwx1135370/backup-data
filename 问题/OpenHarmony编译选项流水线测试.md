### 背景
问题来源：共建姚翰
Linux x86环境
下载OpenHarmony代码，执行`pytest -vs --html ./out/build_option_report.html ./build/test/example/test_build_option.py`
- 问题1
![输入图片说明](https://foruda.gitee.com/images/1702608908189052820/b568fc84_10384282.png "屏幕截图")

- 问题2
https://gitee.com/openharmony/build/pulls/2655/files
性能测试目前也会报错
![输入图片说明](https://foruda.gitee.com/images/1702609022731751890/1280f39f_10384282.png "屏幕截图")
### 分析与解决

 - 问题1
 --》sudo dpkg-reconfigure dash,选择No。然后通过：ls -l /bin/sh确认下是shell

 - 问题2
 --》缺少系统库导致的，一般都发生在交叉编译时。通过确认它最终指定的targets信息为target-cpu是arm。可以基本确定是交叉编译引起的。
   OpenHarmony中有预下载交叉编译器。所在路径如下
   ![输入图片说明](https://foruda.gitee.com/images/1702609269779298523/22bcfae3_10384282.png "屏幕截图")
   test_build_option.py脚本里面涉及交叉编译，包含架构arm,arm64,x86_64。
   1.`./build/prebuilts_download.sh下载交叉编译器`
   2. 没跑成功脚本，应该是交叉编译器路径没带进去。将交叉编译器路径配置到环境变量里面。可参考库上的
    ![输入图片说明](https://foruda.gitee.com/images/1702609469351067907/3c8aec18_10384282.png "屏幕截图")
   
   https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-ide-3861-build.md
   wifiiot_hispark_pegasus的环境是通过这个文档配的
   结合文件和编译看编译产品确实是Hi3861，可参考md文件处理