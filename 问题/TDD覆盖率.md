https://llvm.org/docs/CommandGuide/llvm-cov.html#llvm-cov-gcov
https://maskray.me/blog/2020-09-27-gcov-and-llvm

编译参数添加： -fprofile-arcs ftest-coverage
```
例如： /home/xuyao/workspace/Hsu/LLVM_hsu/out/llvm-install/bin/clang++ -g hello-world.cpp -fprofile-arcs -ftest-coverage -o Gcov_test
```
### 标题x86-64位linux机器调试
- 场景一
![输入图片说明](https://foruda.gitee.com/images/1674012071647399028/16e88c82_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1674012079530450389/4c952058_10384282.png "屏幕截图")
但是无法解析
![输入图片说明](https://foruda.gitee.com/images/1674012151903422280/d79f500f_10384282.png "屏幕截图")
[图片上传中…(image-hCCRQmVHcmV4pZAEMp9t)]
![输入图片说明](https://foruda.gitee.com/images/1674012221668496848/b32da469_10384282.png "屏幕截图")

- 但是存在这样的情况
/home/xuyao/workspace/Hsu/LLVM_hsu/out/llvm-install/bin/clang++ -g hello-world.cpp -fprofile-arcs -ftest-coverage -o Gcov_test
生成hello-world.gcno文件。
然后使用lldb调试.生成了hello-world.gcda
再使用命令`/home/chenhao/workspace/Hsu/LLVM_hsu/out/llvm-install/bin/llvm-cov gcov -m hello-world.gcno`解析就是好的
![输入图片说明](https://foruda.gitee.com/images/1674026075178113015/5c9eb5c2_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1674026093212423969/83eeb58d_10384282.png "屏幕截图")


### 标题ohos设备调试报错
```
(lldb)expr （int)__gcov_dump()
error:Can't evalute the expression without a running targets due to:Interpreter doesn't handle one of the expression's opcodes
expr （int)__gcov_dump()
```
![输入图片说明](https://foruda.gitee.com/images/1674012749037792163/ed4e9a47_10384282.png "屏幕截图")

### 标题如果用gdb调试(返回值随机，但应该是异常的)
```
(gdb)call (int)__gcov_dump()
$1 = -1088888824
(gdb)print (int)__gcov_dump()
$2 = -1088888824
```


![输入图片说明](https://foruda.gitee.com/images/1674976047104653570/e88665ed_10384282.png "屏幕截图")
llvm-objdump --disassemble --full-contents --section=.text xxx.so


/home/chenhao/workspace/Hsu/ohos-sdk/linux/native/llvm/bin/clang++ --target=arm-linux-ohos --gcc-toolchain=/home/chenhao/workspace/Hsu/ohos-sdk/linux/native/llvm --sysroot=/home/chenhao/workspace/Hsu/ohos-sdk/linux/native/sysroot -g -fdata-sections -ffunction-sections -funwind-tables -fstack-protector-strong -no-canonical-prefixes -fno-addrsig -fprofile-arcs -ftest-coverage -Wa,--noexecstack -Wformat -Werror=format-security hello-world.cpp -o Gcov_dump_arm32

__gcov_dump定义
LLVM10 contains __gcov_flush(), and LLVM12 contains __gcov_dump(). Therefore, we don' need to check whether expr (int)__gcov_flush()` is successfully debugged in llvm12.
https://github.com/llvm/llvm-project/blob/release/10.x/compiler-rt/lib/profile/GCDAProfiling.c
https://github.com/llvm/llvm-project/blob/release/12.x/compiler-rt/lib/profile/GCDAProfiling.c
编译生成物

