### 相关命令
#./lldb-server p --server --listen unix-abstract:///com.example.myapplication/platform-1648104534646.sock（或./lldb-server p –server –listen “*:8080”）

F:\public\20220718_gdbserver_fport\20220719> **lldb** 
(lldb)  **log enable lldb all -f F:\log_gdbserver.txt** 
(lldb)  **platform select remote-ohos** 
  Platform: remote-ohos
 Connected: no
(lldb)   **platform connect unix-abstract-connect:///com.example.myapplication/platform-1648104534646.sock** 
  Platform: remote-ohos
    Triple: arm-unknown-linux-unknown
OS Version: 1 (5.10.79)
  Hostname: localhost
 Connected: yes
WorkingDir: /data/local/tmp
Kernel: #1 SMP Tue Mar 29 12:45:24 CST 2022
当connect完成，向HDC发送了fport tcp:62342 localabstract:/com.example.myapplication/platform-1648104534646.sock，其中pid为0(62342通过FindUnusedPort接口根据环境变量HDC_FORWARD_PORT_BASE_FOR_LLDB获取的
(lldb)  **attach 11117** （接口Status Process::Attach(ProcessAttachInfo &attach_info)）
Process 11117 stopped
*thread #1, name = 'Test', stop reason = signal SIGSTOP
    frame #0: 0xb6f25870 ld-musl-arm.so.1
->  0xb6f25870: svc    #0x0
    0xb6f25874: bl     0xb6ee6518
    0xb6f25878: cmp    r0, #0
    0xb6f2587c: ble    0xb6f258c0

Executable module set to "C:\Users\xwx1135370\.lldb\module_cache\remote-ohos\.cache\71D192AF-604C-0686-C19C-D690A8B5CA73-2682CC76\Test".
Architecture set to: arm-unknown-linux-unknown.
当“attach”完成，向HDC发送了fport tcp:62347 localabstract:gdbserver.cc818f，其中pid为11124
![输入图片说明](https://images.gitee.com/uploads/images/2022/0728/113002_8a402578_10384282.png "屏幕截图.png")
```
int main_gdbserver(int argc, char *argv[])  lldb-gdbserver.cpp
注意：手动在设备端执行./lldb-server gdbserver unix-abstract://gdbserver.cc818f --native-regs会返回“lldb-server-local_build”
```

(lldb)  **b 25** 
Breakpoint 1: where = Test`main + 92 at Test.c:25:9, address = 0x004f0ba4
(lldb)  **c** 
Process 11117 resuming
Process 11117 stopped
*thread #1, name = 'Test', stop reason = breakpoint 1.1
    frame #0: 0x004f0ba4 Test`main at Test.c:25:9
(lldb)  **detach** 
Process 11117 detached
当“detach”完成，设备侧pid为11124断掉。但是没有发送fport rm tcp:62347 localabstract:gdbserver.cc818f
(lldb) **platform disconnect** 
Disconnected from "localhost"
当“platform disconnect”完成，设备侧lldb-server断连。发送fport rm tcp:62342 localabstract:/com.example.myapplication/platform-1648104534646.sock，其中pid为0
