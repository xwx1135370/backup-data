### 问题说明
容器内
新出的p50镜像，c++调试，修改bundlename之后，调试失败，无法进入断点
```
./lldb-server p --server --listen unix-abstract:///com.example.myapplication_9com.examplecom.examplesssssss/platform-1648104534646.sock 

D:\SDK\openharmony\9\native\llvm\bin>lldb
(lldb) platform select remote-hos inner
  Platform: remote-hos
 Connected: no
 Container: yes
(lldb) platform connect unix-abstract-connect:///com.example.myapplication_9com.examplecom.examplesssssss/platform-1648104534646.sock
error: Invalid socket namespace
(lldb)
```
容器外也是这样的现象

使用hdc命令行执行正常发送
```
G:\hdc>hdc.exe fport tcp:62342 localabstract:/com.example.myapplication_9com.examplecom.examplesssssss/platform-1648104534646.sock
62342

G:\hdc>hdc.exe fport list
L2E0221B30005220 tcp:62342 localabstract:/com.example.myapplication_9com.examplecom.examplesssssss/platform-1648104534646.sock
```

https://man7.org/linux/man-pages/man7/unix.7.html

解决办法
在DevEco配置得lldb-server启动脚本start_lldb_server.sh中配置环境变量
```
export LLDB_DEBUGSERVER_DOMAINSOCKET_DIR=/data/local
```