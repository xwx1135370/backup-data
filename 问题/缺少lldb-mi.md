【问题现象】
![输入图片说明](https://foruda.gitee.com/images/1689903845094798118/c13a45dc_10384282.png "屏幕截图")

【分析确认】
查看build仓配置
https://gitee.com/openharmony/build/blob/master/prebuilts_download_config.json
![输入图片说明](https://foruda.gitee.com/images/1689903956026929757/8b43ab3c_10384282.png "屏幕截图")

下载https://repo.huaweicloud.com/harmonyos/compiler/clang/15.0.4-d1aa60/linux/clang_linux-x86_64-d1aa60-0630.tar.bz2

包里没有lldb-mi

【解决办法】

方法一： 修改https://gitee.com/openharmony/build/blob/master/prebuilts_download_config.json配置，找一个旧版本

方法二： 下载旧版本clang，在对应的目录放入lldb-mi
