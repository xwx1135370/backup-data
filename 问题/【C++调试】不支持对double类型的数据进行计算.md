### 问题现象
lldb 不支持double类型的表达式计算ldb-mi单板调试最新版本支持的：
(前提，ohos.toolchain.cmake的编译选项中包含-ffunction-sections list(APPEND OHOS_EXE_LINKER_FLAGS -Wl,--gc-sections)编译出的可执行文件，且代码中不包含*、/、==、符号信息)
-var-create - * “double变量 - 数值”
-var-create - * “double变量 + 数值”
lldb-mi单板调试最新版本不支持的：
-var-create - * “double变量 * 数值”
-var-create - * “double变量 / 数值”
-var-create - * “double变量 == 数值”
-var-create - * “double变量 运算 变量”
-var-create - * “double变量 % 数值”

### 问题分析
修改编译选项（https://blog.csdn.net/pengfei240/article/details/55228228）
查看是否有符号表__aeabi_ddiv
