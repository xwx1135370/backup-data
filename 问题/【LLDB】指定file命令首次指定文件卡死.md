### 【问题描述】

蓝区半容器代码，在删除C:\Users\xwx1135370\.lldb\module_cache\remote-hos\.cache目录下所有文件的基础上，首次使用file指定可执行文件，卡死

### 【场景分析】

 **场景一**  

 1. 工具链来源：  

   黄区流水线https://nexus.devops-spb.rnd.huawei.com/service/rest/repository/browse/compilers-team/llvm-builds/hos-os/daily/2022-09-26/642598/

 2.  测试步骤
```
- 删除C:\Users\xwx1135370\.lldb\module_cache\remote-android\.cache目录下的文件

- 使用adb.exe连接设备

- 在设备上运行lldb-server

- 在pc端运行lldb.exe

- 选择远端：platform select remote-android

- 连接：platfrm connect connect://localhost:8080

- 指定调试文件：file /data/ohos_data/lldb_test/lldb-server

```
3. 测试结果
![输入图片说明](https://foruda.gitee.com/images/1664329180263690477/d75791e1_10384282.png "屏幕截图")

 **场景二** 
 1. 工具链来源：  

   黄区流水线https://nexus.devops-spb.rnd.huawei.com/service/rest/repository/browse/compilers-team/llvm-builds/hos-os/daily/2022-09-26/642598/

 2.  测试步骤
```
- 删除C:\Users\xwx1135370\.lldb\module_cache\remote-hos\.cache目录下

- 使用adb.exe连接设备

- 在设备上运行lldb-server

- 在pc端运行lldb.exe

- 选择远端：platform select remote-hos

- 连接：platfrm connect connect://localhost:8080

- 指定调试文件：file /data/ohos_data/lldb_test/lldb-server

```
3. 测试结果
![输入图片说明](https://foruda.gitee.com/images/1664329280909242434/d2fe17dc_10384282.png "屏幕截图")

 **场景三** 
 1. 工具链来源：  

   黄区流水线https://nexus.devops-spb.rnd.huawei.com/service/rest/repository/browse/compilers-team/llvm-builds/hos-os/daily/2022-09-26/642598/

 2.  测试步骤
```
- 删除C:\Users\xwx1135370\.lldb\module_cache\remote-hos

- 使用adb.exe连接设备

- 在设备上运行lldb-server

- 在pc端运行lldb.exe

- 选择远端：platform select remote-hos

- 连接：platfrm connect connect://localhost:8080

- 指定调试文件：file /data/ohos_data/lldb_test/lldb-server

```
3. 测试结果
![输入图片说明](https://foruda.gitee.com/images/1664329355367837028/279e734d_10384282.png "屏幕截图")

**场景四** 
 1. 工具链来源：  

   黄区流水线https://nexus.devops-spb.rnd.huawei.com/service/rest/repository/browse/compilers-team/llvm-builds/hos-os/daily/2022-09-26/642598/

 2.  测试步骤
```
- 删除C:\Users\xwx1135370\.lldb\module_cache\remote-android

- 使用adb.exe连接设备

- 在设备上运行lldb-server

- 在pc端运行lldb.exe

- 选择远端：platform select remote-android

- 连接：platfrm connect connect://localhost:8080

- 指定调试文件：file /data/ohos_data/lldb_test/lldb-server

```
3. 测试结果
![输入图片说明](https://foruda.gitee.com/images/1664329423509829231/d4ea3c50_10384282.png "屏幕截图")

**场景五** 
 1. 工具链来源：  

   蓝区工具链https://repo.huaweicloud.com/harmonyos/compiler/clang/12.0.1-971024/windows/clang-971024-windows-x86_64-20220827.tar.bz2


 2.  测试步骤
```
- 删除C:\Users\xwx1135370\.lldb\module_cache\remote-android\.cache目录下的文件

- 使用hdc.exe连接设备

- 在设备上运行lldb-server

- 在pc端运行lldb.exe

- 选择远端：platform select remote-android

- 连接：platfrm connect connect://localhost:8080

- 指定调试文件：file /data/ohos_data/lldb_test/lldb-server

```
3. 测试结果
![输入图片说明](https://foruda.gitee.com/images/1664329491441654819/5ad29016_10384282.png "屏幕截图")

**场景六** 
 1. 工具链来源：  

   蓝区工具链https://repo.huaweicloud.com/harmonyos/compiler/clang/12.0.1-971024/windows/clang-971024-windows-x86_64-20220827.tar.bz2


 2.  测试步骤
```
- 删除C:\Users\xwx1135370\.lldb\module_cache\remote-android

- 使用hdc.exe连接设备

- 在设备上运行lldb-server

- 在pc端运行lldb.exe

- 选择远端：platform select remote-android

- 连接：platfrm connect connect://localhost:8080

- 指定调试文件：file /data/ohos_data/lldb_test/lldb-server

```
3. 测试结果
![输入图片说明](https://foruda.gitee.com/images/1664329524953970641/793782cd_10384282.png "屏幕截图")

**场景七** 
 1. 工具链来源：  

   半容器编译包

 2.  测试步骤
```
- 删除C:\Users\xwx1135370\.lldb\module_cache\remote-hos

- 使用hdc.exe连接设备

- 在设备上运行lldb-server

- 在pc端运行lldb.exe

- 选择远端：platform select remote-hos

- 连接：platfrm connect connect://localhost:8080

- 指定调试文件：file /data/ohos_data/lldb_test/lldb-server

```
3. 测试结果
![输入图片说明](https://foruda.gitee.com/images/1664329609153676115/f3996e6d_10384282.png "屏幕截图")

**场景八** 
 1. 工具链来源：  

   半容器编译包

 2.  测试步骤
```
- 删除C:\Users\xwx1135370\.lldb\module_cache\remote-hos\.cache目录中的文件

- 使用hdc.exe连接设备

- 在设备上运行lldb-server

- 在pc端运行lldb.exe

- 选择远端：platform select remote-hos

- 连接：platfrm connect connect://localhost:8080

- 指定调试文件：file /data/ohos_data/lldb_test/lldb-server

```
3. 测试结果
![输入图片说明](https://foruda.gitee.com/images/1664329660353866748/91408633_10384282.png "屏幕截图")
