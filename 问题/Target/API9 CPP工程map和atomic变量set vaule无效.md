【DevEco Studio】【SDV】【需求引入】【HOS/OHOS】API9 CPP工程map和atomic变量set vaule无效

```
#include <map>
#include <set>
#include <list>
#include <deque>
#include <iostream>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <initializer_list>
#include <forward_list>
#include <optional>
#include <bitset>
#include <variant>
#include <atomic>

using namespace std;

void IntVarTest()
{
    int int_type = 2;
    std::map<int, int> intA = {{1,2},{3,4}};
}

void VarTest()
{
    std::vector<int> v = {1, 2, 3};
    std::vector<int> v2 = {11, 22, 33};

    std::string singleStr = "this is string";
    std::map<int, std::string> intMap = {{1, "one"}, {2, "two"}};
    std::map<std::string, std::string> strMap = {{"1", "one"}, {"2", "two"}};
    string singleStr2 = "this is string";
    map<int, string> intMap2 = {{1, "one"}, {2, "two"}};
    map<string, string> strMap2 = {{"1", "one"}, {"2", "two"}};

    std::set<int> se = {3, 4, 5};  /////////// no effect
    std::multiset<int> mSet = {4, 4, 5};  /////////// no effect
    std::list<int> li = {7, 8, 9};
    std::queue<int> q;  /////////// no effect
    q.push(11);
    q.push(12);
    std::deque<int> dq = {7, 8, 9}; /////////// no effect
    dq.push_back(13);
    dq.push_back(14);

    std::vector<int> vecArray[2] = {{1, 2, 3}, {4, 5, 6}};
    std::map<int, std::string> mapArray[2];
    mapArray[0].insert({1, "one"});
    mapArray[0].insert({2, "two"});
    mapArray[1].insert({3, "I"});
    mapArray[1].insert({4, "II"});
    std::forward_list<int> forwardList = {11, 22, 33};    /////////// no effect
    //std::optional<double> optionalDouble = 2.0;   // only available from C++17 onwards
    std::bitset<8> b(12);  // 00001100
    //std::variant<Another, Empty> variantObj = another2; // only available from C++17 onwards
    std::atomic<int> atomicInt(7);

    std::unordered_set<int> unSet = {3, 4, 5};   /////////////////////  lldb crash
    std::unordered_map<int, int> unMap = {{1, 100}, {2, 200}}; //////////////////  lldb crash

    string sss = "qweqwe";
    cout << sss << endl;
}

int main()
{
    IntVarTest();
    VarTest();
}

```