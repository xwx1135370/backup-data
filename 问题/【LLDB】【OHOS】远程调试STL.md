工具路径：http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.5/20220810_020030/version-Release_Version-OpenHarmony 3.1.7.5-20220810_020030-ohos-sdk.tar.gz

调试设备：Hi3516 arm32位

[源码路径](https://gitee.com/xwx1135370/llvm-related-issues/tree/master/Demo/STL_test)

1. 使用cmd命令窗口是，不在lldb所在目录运行，而是相对目录运行.查看STL崩溃
无此现象

2. 查看std::list<int> li = {7, 8, 9}变量值
![输入图片说明](https://foruda.gitee.com/images/1663226088656938409/5709152b_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1663294068078173044/d8d5203c_10384282.png "屏幕截图")

3. 查看std::forward_list<int> forwardList = {11, 22, 33} 变量值
![输入图片说明](https://foruda.gitee.com/images/1663226198127077543/46f9f9f1_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1663294090999473630/d359689a_10384282.png "屏幕截图")

4. 查看string sss = "qweqwe"; 变量值，不符合预期
![输入图片说明](https://foruda.gitee.com/images/1663226342497181212/dab39862_10384282.png "屏幕截图")

5. STL中有string类型，不符合预期

6 查看std::queue<int> q; q.push(11); q.push(12); 不符合预期
![输入图片说明](https://foruda.gitee.com/images/1663226413004520931/a34177b2_10384282.png "屏幕截图")

7 查看std::deque<int> dq = {7, 8, 9};dq.push_back(13); dq.push_back(14);不符合预期
![输入图片说明](https://foruda.gitee.com/images/1663226769605148504/ddad359b_10384282.png "屏幕截图")

分析解决：
根据queue和 deque报错信息，猜测因为环境变量没有设置。
https://blog.csdn.net/qq_43302566/article/details/121537908  
设置环境变量，重启PC，进行调试，问题解决
![PYTHONPATH](https://foruda.gitee.com/images/1663294630004075797/3cb2951b_10384282.png "屏幕截图")
![q](https://foruda.gitee.com/images/1663294887360190813/357b6b5a_10384282.png "屏幕截图")
![dq](https://foruda.gitee.com/images/1663294906758154135/e3578f46_10384282.png "屏幕截图")