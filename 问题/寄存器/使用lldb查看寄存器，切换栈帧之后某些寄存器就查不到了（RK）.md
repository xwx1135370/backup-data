 **首帧** 
![输入图片说明](https://foruda.gitee.com/images/1672882060467671304/e15a34be_10384282.png "屏幕截图")

 **切了栈帧** 
![输入图片说明](https://foruda.gitee.com/images/1672882107429790995/4fafaa18_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1672882122838034079/602feb14_10384282.png "屏幕截图")

 **分析**
read的时当前栈帧保存的.每次栈帧，函数都会把要用的register压栈里面
 ![输入图片说明](https://foruda.gitee.com/images/1672882217019568108/cac13686_10384282.png "屏幕截图")
切栈帧之后，有些寄存器可以查到，有些查不到。PC指针的值时对的，因为PC是每次都会被压栈的，其他的不一定

将编译的二进制文件反汇编
```
llvm-objdump -S libentry.so
llvm-objdump -d libentry.so

```
![输入图片说明](https://foruda.gitee.com/images/1672883507645269177/890befae_10384282.png "屏幕截图")
在调用basic时会将w8寄存器的数据放在栈里面。[sp,#64]栈的偏移