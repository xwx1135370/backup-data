### 【问题描述】
海思芯片上，ARM64设备，整个进程数据断点仅能触发一次，后续不会再触发。
[3.3 Issue 3: Watchpoints does not work on some ARM64 devices if any of them is triggered once](https://gitee.com/openharmony/third_party_llvm-project/issues/I7ODSS?from=project-issue)

### 【分析】


### 【结论】
海思内核和硬件在看，目前未收到反馈，只说近年年底会在新硬件上支持硬件数据断点。
