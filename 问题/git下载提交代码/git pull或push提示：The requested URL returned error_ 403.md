Git pull 或 push 提示：The requested URL returned error: 403

### 【案例】

![输入图片说明](https://foruda.gitee.com/images/1690167540112454680/da3d44e2_10384282.png "屏幕截图")

### 【分析确认】

1. 保证自己的账号拥有项目的访问权限

2. 确保提交代码的环境的公钥配置到了gitee（或codehub等）上。

3. 如果使用git + https操作项目，直接提示403，并没有让输入用户名密码，这种情况是开启了密码缓存。可以打开.gitconfig确认。
   
   使用下面命令清除密码缓存的配置.一般--global项配置了即可。 
   ```
   git config --local --unset credential.helper
   git config --global --unset credential.helper
   git config --system --unset credential.helper
   ```
   清除之后再次操作push
   此时会让输入用户名密码。

>  **说明** 
>
> 这种情况一般是由于配置了缓存密码`git config --global credential.helper store
`
>
> 且提交代码的环境有多人公用，于是user.name 和密码不配套导致的。
>
> 密码缓存在.git-credentials文件中，如果不确定可以删除，重新请求一次就会重新缓存了
>
