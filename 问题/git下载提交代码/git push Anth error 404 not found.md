### 【案例】
![输入图片说明](https://foruda.gitee.com/images/1690180324163588077/89da62ec_10384282.png "屏幕截图")

### 【分析】

使用git remote -v查看远端是否存在，若存在。则只需要再次设置一下url即可。

使用命令

```
git remote set-url gitee xxx
```
这里的xxx是库的地址。