https://gitee.com/openharmony/third_party_llvm-project/blob/master/llvm/include/llvm/ADT/StringRef.h#L289
```
/// Check if this string starts with the given \p Prefix.
    LLVM_NODISCARD
    bool startswith(StringRef Prefix) const {
      return Length >= Prefix.Length &&
             compareMemory(Data, Prefix.Data, Prefix.Length) == 0;
    }
```

```
    // Workaround memcmp issue with null pointers (undefined behavior)
    // by providing a specialized version
    static int compareMemory(const char *Lhs, const char *Rhs, size_t Length) {
      if (Length == 0) { return 0; }
      return ::memcmp(Lhs,Rhs,Length);
    }
```
https://blog.csdn.net/weixin_45380951/article/details/100699751