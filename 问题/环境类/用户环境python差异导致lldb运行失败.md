[[LLDB] measure the influence of disable 'LLDB_ENABLE_PYTHON' for darwin-x86-64/darwin-arm64 triple · Issue #I5O6NT · OpenHarmony/third_party_llvm-project - Gitee.com](https://gitee.com/openharmony/third_party_llvm-project/issues/I5O6NT?from=project-issue)

[[LLDB] [BUG] 使用llvm10工具编译调试cpp，在调试过程中查看stl变量会导致调试崩溃 · Issue #I5M9OV · OpenHarmony/third_party_llvm-project - Gitee.com](https://gitee.com/openharmony/third_party_llvm-project/issues/I5M9OV?from=project-issue)


解决方案：
1. 静态链接python，需要在环境中设置正确的PYTHON_PATH包搜索路径

2. 关闭python，会导致部分依赖python脚本解析的stl解析功能失效
