问题现象
![输入图片说明](https://foruda.gitee.com/images/1672802703894206055/d1bd1906_10384282.png "屏幕截图")

user模式下
```
$ ps -ef| grep com.example.myapplication
shell 2979 1914 8 05:30:36 pts/0 00:00:00 grep com.example.myapplication
```

root模式
```
# ps -ef| grep com.example.myapplication
20010038 3040  242 05:32:13 ?     00:00:00 com.example.myapplication
root     3070 3025 05:32:26 pts/0 00:00:00 grep com.example.myapplication
```

分析结论
lldb查不到对应的pid信息
刷新新镜像（最好选1月的镜像）
