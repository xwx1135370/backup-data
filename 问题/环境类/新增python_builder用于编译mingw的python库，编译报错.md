[python_builder.py](https://gitee.com/xwx1135370/llvm-related-issues/blob/master/%E9%97%AE%E9%A2%98/python_builder.py)

【问题现象】
![输入图片说明](https://foruda.gitee.com/images/1691475964025279538/93f970cf_10384282.png "屏幕截图")


【分析】
`which autoreconf`未在环境上找到相关工具

临时处理：`apt-get install binutils-dev pkg-config autoconf autoconf-archive`

【黄区问题现象】
![输入图片说明](https://foruda.gitee.com/images/1696751153387615453/d119070a_10384282.png "屏幕截图")

【分析】
python代码未更新，缺少x86_64-w64-mingw32处理

【更新代码】
![输入图片说明](https://foruda.gitee.com/images/1696751233943385874/f7b55b7d_10384282.png "屏幕截图")
https://gitee.com/openharmony/third_party_python/pulls/49/files