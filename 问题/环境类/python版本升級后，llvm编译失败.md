https://gitee.com/openharmony/third_party_llvm-project/issues/I6EF97?from=project-issue
### 问题现象
![输入图片说明](https://foruda.gitee.com/images/1678067776808498795/a51f12ff_10384282.png "屏幕截图")
```
def build_mingw64_headers(self):
        headers_dir = self.build_config.repo_root('third_party', 'mingw-w64', 'mingw-w64-headers', 'build')
        if os.path.isdir(headers_dir):
            shutil.rmtree(headers_dir)
        os.makedirs(headers_dir)
        os.chdir(headers_dir)
        print(os.getcwd())
        cmd = ['../configure', '--prefix=%s' % self.prefix,
            '--enable-idl', '--with-default-win32-winnt=0x600',
            '--with-default-msvcrt=ucrt']
        self.env['INSTALL'] = "install -C"
        self.check_call(cmd, env=self.env)
Failed->self.check_call(['make', 'install'], env=self.env)
```

### 问题原因
mingw.py中没有把PATH变量传递给子进程。

环境上的python版本从3.8版本升级到3.10版本
![python3.8](https://foruda.gitee.com/images/1678068021344985490/2bea6849_10384282.png "屏幕截图")
![python3.10](https://foruda.gitee.com/images/1678068069004805974/4f936513_10384282.png "屏幕截图")
版本升级后，subprocess.check_call接口说明，如果改了env，需要把一些系统相关变量传进入
https://docs.python.org/zh-cn/3.9/library/subprocess.html?highlight=subprocess%20check_call
![输入图片说明](https://foruda.gitee.com/images/1678069102595637340/60632a3c_10384282.png "屏幕截图")

https://docs.python.org/3/library/subprocess.html#subprocess.Popen

## 解决办法
https://gitee.com/xwx1135370/third_party_llvm-project/commit/651b7035c167a36367b09e8b2baab127b79fe775
```
        self.env = {
            "CC": "%s/../bin/clang" % self.prefix,
            "CXX": "%s/../bin/clang" % self.prefix,
            "AR": "%s/../bin/llvm-ar" % self.prefix,
            "DLLTOOL": "%s/../bin/llvm-dlltool" % self.prefix,
            "LD": "%s/../bin/ld.lld" % self.prefix,
            "RANLIB": "%s/../bin/llvm-ranlib" % self.prefix,
            "STRIP": "%s/../bin/llvm-strip" % self.prefix,
            "LDFLAGS": common_flags,
            "CFLAGS": common_flags,
            "CXXFLAGS": common_flags,
            "PATH": os.environ["PATH"],
        }
```
