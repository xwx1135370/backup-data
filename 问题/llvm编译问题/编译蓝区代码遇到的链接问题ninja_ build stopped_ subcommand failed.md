### 问题现象
[图片上传中…(image-fnLEa9i6LvacRKDBdW7V)]
```
FAILED: lib/clang/12.0.1/lib/linux/libclang_rt.fuzzer_no_main-x86_64.a
: && /home/xuyao/workspace/blue_llvm12/prebuilts/cmake/linux-x86/bin/cmake -E remove lib/clang/12.0.1/lib/linux/libclang_rt.fuzzer_no_main-x86_64.a && /usbclang_rt.fuzzer_no_main-x86_64.a  projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerCrossOver.cpp.o projects/compiler-rt/lib/fuzzer/CMwTrace.cpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerDriver.cpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64./compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerExtFunctionsWeak.cpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerE-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerExtraCounters.cpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerFork.cpp.o prTfuzzer.x86_64.dir/FuzzerIO.cpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerIOPosix.cpp.o projects/compiler-rt/lib/fuzzer/CMakecpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerLoop.cpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/Fuzzeer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerMutate.cpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerSHA1.cpp.o projects/compiler-rt/FuzzerTracePC.cpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerUtil.cpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x8ompiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerUtilFuchsia.cpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerUtilLinuakeFiles/RTfuzzer.x86_64.dir/FuzzerUtilPosix.cpp.o projects/compiler-rt/lib/fuzzer/CMakeFiles/RTfuzzer.x86_64.dir/FuzzerUtilWindows.cpp.o && /usr/bin/ranlrt.fuzzer_no_main-x86_64.a && cd /home/xuyao/workspace/blue_llvm12/out/llvm_make/projects/compiler-rt/lib/fuzzer/cxx_x86_64_merge.dir && /usr/bin/ld --who12/out/llvm_make/lib/clang/12.0.1/lib/linux/libclang_rt.fuzzer_no_main-x86_64.a --no-whole-archive /home/xuyao/workspace/blue_llvm12/out/llvm_make/project_64/lib/libc++.a -r -o fuzzer_no_main.o && /usr/bin/objcopy --localize-hidden fuzzer_no_main.o && /home/xuyao/workspace/blue_llvm12/prebuilts/cmake/linux-ace/blue_llvm12/out/llvm_make/lib/clang/12.0.1/lib/linux/libclang_rt.fuzzer_no_main-x86_64.a && /usr/bin/ar qcs /home/xuyao/workspace/blue_llvm12/out/llvmrt.fuzzer_no_main-x86_64.a fuzzer_no_main.o
/home/xuyao/workspace/blue_llvm12/out/llvm_make/lib/clang/12.0.1/lib/linux/libclang_rt.fuzzer_no_main-x86_64.a: member /home/xuyao/workspace/blue_llvm12/obclang_rt.fuzzer_no_main-x86_64.a(FuzzerCrossOver.cpp.o) in archive is not an object
[2931/5577] Linking CXX shared module lib/LLVMgold.so
ninja: build stopped: subcommand failed.
Traceback (most recent call last):
  File "build.py", line 2017, in <module>
    main()
  File "build.py", line 1984, in main
    build_config.xunit_xml_output)
  File "build.py", line 518, in llvm_compile
    extra_env=llvm_extra_env)
  File "build.py", line 399, in build_llvm
    install=True)
  File "build.py", line 221, in invoke_ninja
    self.check_call([ninja_bin_path] + ninja_list + ninja_target, cwd=out_path, env=env)
  File "build.py", line 290, in check_call
    subprocess.check_call(cmd, *args, **kwargs)
  File "/usr/lib/python2.7/subprocess.py", line 190, in check_call
    raise CalledProcessError(retcode, cmd)
subprocess.CalledProcessError: Command '['/home/xuyao/workspace/blue_llvm12/prebuilts/cmake/linux-x86/bin/ninja']' returned non-zero exit status 1
```
环境上链接关系：
![输入图片说明](https://foruda.gitee.com/images/1662360265892850425/f8011e25_10384282.png "屏幕截图")

### 解决办法
- 清空历史编译（未解决）
- sudo apt install lld-10
- cd /usr/bin
- sudo ln -f ld.lld-10 ld
![输入图片说明](https://foruda.gitee.com/images/1662360517415559222/44f99049_10384282.png "屏幕截图")

7.189.31.53
chenhao
ch@091100