现象
```
--Could Not find Python3(missing:Python3_LIBRARIES Python3_INCLUDE_DIRS Development)(found version "3.8.0")
CMake Error at /home/xuyao/workspace/Hsu/LLVM_15/prebuilts/cmake/linux-x86/share/cmake-3.16/Modules/FindPackageHandleStandardArgs.cmake:146(message):
  Could Not find PythonAndSwig(missing:Python3_LIBRARIES
  Python3_INCLUDE_DIRS)
Call Stack(most recent call first):
  /home/xuyao/workspace/Hsu/LLVM_15/prebuilts/cmake/linux-x86/share/cmake-3.16/Modules/FindPackageStanardArgs.camke:393(_FPHSA_FAILURE_MESSAGE)
  /home/xuyao/workspace/Hsu/LLVM_15/toolchain/llvm-project/lldb/cmake/modules/FindPythonAndSwig.cmake:71(find_package_handle_standard_args)
  /home/xuyao/workspace/Hsu/LLVM_15/toolchain/llvm-project/lldb/cmake/modules/LLDBConfig.camke:48(find_package)
  /home/xuyao/workspace/Hsu/LLVM_15/toolchain/llvm-project/lldb/cmake/modules/LLDBConfig.cmake:59(add_optional_dependency)
  /home/xuyao/workspace/Hsu/LLVM_15/toolchain/llvm-project/lldb/CMakeLists.txt:21(include)
```
toolchain/llvm-project/lldb/cmake/modules/FindPythonAndSwig.cmake

处理：
1. 获取正确的INCLUDE_DIR等目录
```
获取Python3_EXECUTABLE
python3 -c "from distutils.sysconfig import get_python_inc;
print(get_python_inc())"
获取Python3_LIBRARIES 
python3 -c "import distutils.sysconfig as sysconfig; print(sysconfig.get_config_var('LIBDIR'))"
获取python3
which python3

```
2. cmake设置正确路径
```
diff --git a/lldb/cmake/modules/FindPythonAndSwig.cmake b/lldb/cmake/modules/FindPythonAndSwig.cmake
index 562d5307e8c8..ca4df2535ddd 100644
--- a/lldb/cmake/modules/FindPythonAndSwig.cmake
+++ b/lldb/cmake/modules/FindPythonAndSwig.cmake
@@ -46,6 +46,10 @@ if(NOT Python3_INCLUDE_DIRS AND PYTHON_INCLUDE_DIRS)
   set(Python3_INCLUDE_DIRS ${PYTHON_INCLUDE_DIRS})
 endif()
 
+set(Python3_EXECUTABLE "/usr/bin/python3")
+set(Python3_LIBRARIES "/usr/lib")
+set(Python3_INCLUDE_DIRS "/usr/include/python3.8")
+
 if(Python3_LIBRARIES AND Python3_INCLUDE_DIRS AND Python3_EXECUTABLE AND SWIG_EXECUTABLE)
   set(PYTHONANDSWIG_FOUND TRUE)
 else()
```
