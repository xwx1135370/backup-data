### 问题现象
![输入图片说明](https://foruda.gitee.com/images/1703151278927267054/108d60de_10384282.png "屏幕截图")

### 分析
从报错信息看，是ohos_componments_suport未使用导致的，报错在gn中

### 背景
OpenHarmony中新需求“添加部件使能开关”。主要处理，gn更新，OpenHarmony代码适配。
https://gitee.com/openharmony/build/pulls/2682/files
https://gitee.com/openharmony/developtools_hdc/pulls/838


### 处理
规避方案：
回退pthread_create
https://gitee.com/openharmony/developtools_hdc/pulls/684

正式方案：更新llvm中预编译的gn工具（https://repo.huaweicloud.com/harmonyos/compiler/gn/1101/?C=M&O=D）
> 目前Mac平台未编译适配出来gn,Mac包归档进展咨询下面的相关方
> 特性相关方：任康00843264 雷光宇 30030200 陈明星 WX1286406 赵礼辉 WX1025643 
