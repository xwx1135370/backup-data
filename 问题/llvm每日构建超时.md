## 背景
每日构建每个卷限制时长4小时，即240min。
每日构建异常，门禁正常。切都从镜像openharmony-build-env-jnlp:1.0.3切换到了openharmony-testcodearts-build-env-jnlp:1.0.4

## 问题现象
每日构建以连续多天失败
![输入图片说明](https://foruda.gitee.com/images/1701929622737112168/f42af053_10384282.png "屏幕截图")
从门禁维护人员提供的信息看，是在执行ndk编译任务，没跑完，超时了导致的。

## 修复pr
https://gitee.com/liwentao_uiw/llvmopen-source-transfer-gitee/pulls/28