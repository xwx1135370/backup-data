### 现状与现象
libxml2的buildle.json中配置的component名字和BUILD.gn中的动态库名称一致
![输入图片说明](https://foruda.gitee.com/images/1702540757726526751/d28a1527_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1702540879500823689/319ac58d_10384282.png "屏幕截图")

当在rk3568中添加了libxml2部件（即支持单独编译）
![输入图片说明](https://foruda.gitee.com/images/1702541001616763241/255717c4_10384282.png "屏幕截图")
使用此命令编译`./build.sh --product-name rk3568 --build-target libxml2`
报错“ ninja: error: unknown target 'libxml2', did you mean 'libwm'?”
![输入图片说明](https://foruda.gitee.com/images/1702541082659558067/95628b43_10384282.png "屏幕截图")

### 原因
重名导致的
![输入图片说明](https://foruda.gitee.com/images/1702541257213759391/345fae94_10384282.png "屏幕截图")
https://gitee.com/openharmony/build/blob/master/docs/%E7%BC%96%E8%AF%91%E9%85%8D%E7%BD%AE%E6%8C%87%E5%AF%BC%E6%96%87%E6%A1%A3.md#%E5%A6%82%E4%BD%95%E5%8D%95%E7%BC%96%E4%B8%80%E4%B8%AA%E6%A8%A1%E5%9D%97

### 处理
使用命令`./build.sh --product-name rk3568 --build-target third_party/libxml2:libxml2`编译