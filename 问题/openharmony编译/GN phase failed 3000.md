### 【问题现象】
```
[91m[OHOS ERROR][0m Traceback (most recent call last):
[91m[OHOS ERROR][0m   File "/data/xxxxxx/OH/versions/build/hb/services/gn.py", line 186, in _execute_gn_gen_cmd
[91m[OHOS ERROR][0m     SystemUtil.exec_command(gn_gen_cmd, self.config.log_path)
[91m[OHOS ERROR][0m   File "/data/xxxxxx/OH/versions/build/hb/util/system_util.py", line 63, in exec_command
[91m[OHOS ERROR][0m     raise OHOSException(
[91m[OHOS ERROR][0m exceptions.ohos_exception.OHOSException: Please check build log in /data/xxxxxx/OH/versions/out/generic_generic_arm_64only/hisi_newbaltimore_newphone_standard/build.log
[91m[OHOS ERROR][0m 
[91m[OHOS ERROR][0m During handling of the above exception, another exception occurred:
[91m[OHOS ERROR][0m 
[91m[OHOS ERROR][0m Traceback (most recent call last):
[91m[OHOS ERROR][0m   File "/data/l00522436/OH/versions/build/hb/containers/status.py", line 47, in wrapper
[91m[OHOS ERROR][0m     return func(*args, **kwargs)
[91m[OHOS ERROR][0m   File "/data/l00522436/OH/versions/build/hb/services/gn.py", line 188, in _execute_gn_gen_cmd
[91m[OHOS ERROR][0m     raise OHOSException('GN phase failed', '3000')
[91m[OHOS ERROR][0m exceptions.ohos_exception.OHOSException: GN phase failed
[91m[OHOS ERROR][0m 
[91m[OHOS ERROR][0m Code:      3000
[91m[OHOS ERROR][0m 
[91m[OHOS ERROR][0m Reason:    GN phase failed
[91m[OHOS ERROR][0m 
[91m[OHOS ERROR][0m Solution:  Please check the compile log at out/{compiling product}/build.log, If you could analyze build logs. 
[91m[OHOS ERROR][0m 		Or you can try the following steps to solve this problem:
[91m[OHOS ERROR][0m 		  1. cd to OHOS root path
[91m[OHOS ERROR][0m 		  2. run 'hb clean --all' or 'rm -rf out build/resources/args/*.json'.
[91m[OHOS ERROR][0m 		  3. repo sync
[91m[OHOS ERROR][0m 		  4. repo forall -c 'git lfs pull'
[91m[OHOS ERROR][0m 		  5. bash build/prebuilts_download.sh
[91m[OHOS ERROR][0m 		  6. rebuild your product or component
[91m[OHOS ERROR][0m 
[91m[OHOS ERROR][0m 		If you still cannot solve this problem, you could post this problem on: 
[91m[OHOS ERROR][0m 		  https://gitee.com/openharmony/build/issues
[91m[OHOS ERROR][0m 
```


### 【解决办法】

```
rm -rf out build/resources/args/*.json
```
