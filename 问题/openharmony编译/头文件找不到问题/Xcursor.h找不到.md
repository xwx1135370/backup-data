### 问题现象

![输入图片说明](https://foruda.gitee.com/images/1686621329716714768/99e0f26c_10384282.png "屏幕截图")

### 分析

使用find命令查找Openharmony整体代码中Xcursor.h。未找到。

我们知道编译器搜索的默认路径为:

/usr/include：系统级别的头文件。

/usr/local/include：本地安装软件的头文件。

./：当前目录。

在默认路径中查找Xcursor.h，均未找到。故，环境没有安装对应的库。

### 解决办法

使用apt-get 安装一下libxcursor-dev库即可
