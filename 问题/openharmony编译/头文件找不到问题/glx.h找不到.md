### 问题现象

![输入图片说明](https://foruda.gitee.com/images/1686620789379547262/a6e1416d_10384282.png "屏幕截图")

### 分析

1. 使用find命令查找Openharmony整体代码中glx.h

 ```
./workspace/master/third_party/mesa3d/include/GL/glx.h
 ```
2. 查看编译出错的cpp编译参数，未找到-I ${path}/third_party/third_party/include这样的编译参数

我们知道编译器搜索的默认路径为:

> /usr/include：系统级别的头文件。
>
> /usr/local/include：本地安装软件的头文件。
>
> ./：当前目录。

在默认路径中查找glx.h，均未找到。故，环境没有安装对应的库。

### 解决办法

    使用apt-get 安装一下libgl1-mesa-dev库即可
