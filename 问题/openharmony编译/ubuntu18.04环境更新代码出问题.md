###  **【问题现象】** 

![输入图片说明](https://foruda.gitee.com/images/1684980140232098691/934cbcb3_10384282.png "屏幕截图")

###  **【问题原因】** 


使用Python调用requests模块时报urllib3 和chardet不匹配的告警信息

###  **【解决方案】** 


协助冲突的urllib3和chardet模块，然后重新安装并升级requests模块，之后问题即可得到解决，其卸载和安装命令如下:

```
sudo pip uninstall urllib3 chardet
 
sudo pip install --upgrade requests
```