### 问题现象

![输入图片说明](https://foruda.gitee.com/images/1686811191748765362/f323bef6_10384282.png "屏幕截图")

### 分析

build仓合入记录： https://gitee.com/openharmony/build/pulls/2006

下载 https://repo.huaweicloud.com/harmonyos/compiler/clang/15.0.4-57fd63_sp3/linux/clang_linux-x86_64-57fd63-0530_sp3.tar.bz2

llvm归档仓里有报错缺少的文件。

### 结论
下载openharmony代码不全导致的。可按照目录层级关系下载https://repo.huaweicloud.com/harmonyos/compiler/clang/15.0.4-57fd63_sp3/linux/clang_linux-x86_64-57fd63-0530_sp3.tar.bz2到prebuilt目录下，或者清掉工具链，重新下载。
