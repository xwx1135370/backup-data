### 举例说明

 **前提** 
- 个人仓为[llvm代码仓](https://gitee.com/xwx1135370/third_party_llvm-project.git)
- 从个仓到主仓的提交分支为 https://gitee.com/xwx1135370/third_party_llvm-project.git -b `master_cache` 到 https://gitee.com/openharmony/third_party_llvm-project.git -b `master`
- push了多次commit信息在个人分支master_cache上（一个PR）

 **解决办法** 
- 先下载主仓的代码 
```
git clone https://gitee.com/openharmony/third_party_llvm-project.git -b master
```
- 添加远端 
```
git remote add Hsu_cache https://gitee.com/xwx1135370/third_party_llvm-project.git
```
- fetch到新建的个人远端
```
git fetch Hsu_cache
```
- 将个仓master_cache分支的代码进行同步
- 添加修改的代码
```
git add.
```
- commit提交(合一提交的话用-s选项)
```
git commit -s/amend（首次提交/非首次提交）
```
- push到远端个人仓要合一的分支
```
git push Hsu_cache HEAD:master_cache -f
```
![输入图片说明](https://foruda.gitee.com/images/1677633408538621454/ee9e6ba3_10384282.png "push的commit信息合一.PNG")
```