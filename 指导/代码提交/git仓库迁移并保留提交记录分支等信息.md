新建一个空的文件夹，依次执行下面的命令，即可实现将git仓库从 https://gitee.com/xwx1135370/project_name.git迁移到https://gitee.com/xwx1135370/new_project_name.git且保留提交记录，分支等信息。
不过需要注意的是，在执行最后一步的时候，一定要慎重，因为如果https://gitee.com/xwx1135370/new_project_name.git这个仓库有内容的话，会直接被覆盖，且没有提醒。

```
git clone --bare https://gitee.com/xwx1135370/project_name.git

cd project_name.git

git push --mirror https://gitee.com/xwx1135370/new_project_name.git
```