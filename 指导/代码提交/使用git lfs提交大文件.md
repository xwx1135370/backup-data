- 根据提示，我们先安装git-lfs
-  执行git lfs install
-  找到项目文件下大文件 find ./ -size +100M
-  执行git lfs track "{上面搜索到的大文件路径}"
-  执行git add .gitattributes
-  git add {上面搜索到的大文件路径}
-  git commit -m "Add large file"
-  git push origin

 **问题** 
如果是提交到码云，在同步代码到码云的时候，可能会遇到以下问题
- WARNING: Authentication error: Authentication required: LFS only supported repository in paid enterprise.
--》git config lfs.https://gitee.com/{your_gitee}/{your_repo}.git/info/lfs.locksverify false
- batch response: LFS only supported repository in paid enterprise.
--》是因为付费才可以使用 rm .git/hooks/pre-push


git提交文件带可执行权限： `git update-index --chmod=+r <file>`