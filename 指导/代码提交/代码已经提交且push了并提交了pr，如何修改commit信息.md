举例：
### 前提
- 个人仓为llvm代码仓 git clone https://gitee.com/xwx1135370/third_party_llvm-project.git -b master_cache
- 从个人仓到贮藏的提交分支为https://gitee.com/xwx1135370/third_party_llvm-project.git -b master_cache 到 https://gitee.com/openharmony/third_party_llvm-project.git -b master
- 以及将代码push到了个人分支master_cache上，提了pr

### 解决办法
在原来提交了commit信息的代码里面，使用如步骤进行更新commit信息
```
git commit --amend
```
- 填写commit信息
- 保存
- push到远端个人仓要更新commit信息的分支
```
git push origin HEAD:master_cache -f
```