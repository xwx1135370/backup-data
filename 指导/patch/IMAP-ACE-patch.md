```
#ACETAO侵入式修改
rm -rf "$IMAPM"/../open_source/acetao/source/ACE_wrappers/* || exit 1
cd "$IMAPM"/../open_source/acetao/source/
cp -af ACE_TAO/* ACE_wrappers/ || exit 1

cd -
cp -af "$IMAPM"/conf/config-linux-euler.h "$IMAPM"/../open_source/acetao/source/ACE_wrappers/ace/ || exit 1
cp -af "$IMAPM"/conf/config-linux-euler-aarch64.h "$IMAPM"/../open_source/acetao/source/ACE_wrappers/ace/ || exit 1
patch -p1 -d "$IMAPM"/../open_source/acetao/source/ACE_wrappers < "$IMAPM"/../open_source/acetao/huawei_patch/makefile.patch || exit 1
if [ $? -ne 0 ]; then
    echo "patch failed."
    exit 1
fi
```