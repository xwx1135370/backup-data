- 默认C++工程中的`EntryAbility.ts`文件（如`D:\DevEcoStudioProjects\MyApplication\entry\src\main\ets\entryability\EntryAbility.ts`）中添加以下代码
   ```
   import testNapi from 'libentry.so'
   hilog.info(0x0000, 'testTag', 'Test NAPI 2 + 3 = %{public}d', testNapi.add(2, 3));
   ```
   ![输入图片说明](https://foruda.gitee.com/images/1695807043464491322/1ce1783b_10384282.png "屏幕截图")


- 选择混合调试
   ![输入图片说明](https://foruda.gitee.com/images/1695807140883089222/48798ab7_10384282.png "屏幕截图")