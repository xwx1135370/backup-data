### 一、拉代码编译

主干master代码
```
repo init -u https://gitee.com/openharmony/manifest.git -b master --no-repo-verify
```

更新代码
```
repo sync -c
```
更新二进制
```
repo forall -c 'git lfs pull' 
```

准备
```
./build/prebuilts_download.sh
```

| 类型  |编译命令   |
|---|---|
| Rk3568（arm32）  |./build.sh --product-name rk3568   |
| Rk3568（aarch64）  | ./build.sh --product-name rk3568 --gn-args use_musl=true --target-cpu arm64|
|Rk3568（x86_64） | ./build.sh --product-name rk3568 --ccache  --target-cpu x86_64  |
| SDK包  | ./build.sh --product-name ohos-sdk –-ccache  |
| Hi3516  | ./build.sh --product-name Hi3516DV300  |



### 二、生成hap包

编译生成测试套命令
```
test/xts/acts/build.sh suite=acts system_size=standard target_subsystem=compileruntime
```

测试套生成路径
```
/out/hi3516dv300/suites/haps/
```

xts用例路径
```
test/xts/acts/compileruntime/xml_lib_standard/src/main/js/test/
```

> **注意：** 
> 每次编译生成测试套前，执行 `rm -rf out/hi3516dv300/obj/test/` 清理下缓存，否则无法生成新的hap包



### 三、git上库
查看当前改动情况
```
git status 
```
提交当前改动
```
git add . 
``` 
提交
```
git commit --signoff 
```
进入编辑模式，在第一行加上一句改动描述
将改动提交到git私仓上
```
git push 
```

