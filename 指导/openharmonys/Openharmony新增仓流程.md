Openharmony新增仓流程
### 1 【背景】

当Openharmony系统开发者在系统调试时，使用llvm工具链中的lldb调试时环境配置略显繁杂，且命令自动补齐，TUI等功能缺失，导致开发者使用网上下载的gdb调试，经常出现版本匹配问题。需改善。

lldb支持自动命令行补齐和TUI调试，需要依赖三方库libedit和libncurses。

火车头版本：

https://ostms.rnd.huawei.com/osmanageweb/#/software-detail?versionCode=0583044480&domain=1&releaseTrain=CleanSource%20V5.1

https://ostms.rnd.huawei.com/osmanageweb/#/software-detail?versionCode=0583050686&domain=1&releaseTrain=CleanSource%20V5.1

### 2【流程】

#### 2.1 架构sig评审申报

每周二评审，需要准备评审PPT。（一般周日前提交申请，存在不通过的情况）

申请表： https://shimo.im/sheets/StzhuFkEk38enrnl/MODOC

PPT模板：
 
> 注意：
> - 申请表里面有一栏“隶属sig”很关键，在2.2中会用到。
> - 由于需要申请的libedit和libncurses库是给llvm工具链中的lldb使用，保持与https://gitee.com/openharmony/third_party_llvm-project和https://gitee.com/openharmony/third_party_lldb-mi一致的sig，即“sig_compileruntime”
> - 如果是编译构建的，挂到build，即“sig_buildsystem”

#### 2.2 Sig申请建仓以及代码上传

相关干系人：龙翠 wx5331242、 马明帅 00812780、董金光 00643971。

申请建仓地址： http://ci.openharmony.cn/workbench/sig/repository
 
 
提交代码：

https://gitee.com/openharmony-sig/third_party_libedit

https://gitee.com/openharmony-sig/third_party_ncurses


#### 2.3 Sig毕业评审

需要上架构SIG和QA SIG评审申报，要准备相应的材料。（双周一次）

架构SIG申报表：

https://shimo.im/sheets/StzhuFkEk38enrnl/MODOC

PPT模板：同2.1

QA SIG评审申报表：

https://shimo.im/sheets/AQrrKb4pJCUFYkHR/MODOC

评审参考（需要找回应SIG负责人核对）：

https://gitee.com/openharmony/community/blob/master/sig/sig_qa/guidance_for_incubation_project_graduation_cn.md

#### 2.4 毕业之后转移仓到主线上

相关干系人：马明帅00812780

找马明帅转移仓到主线上。



其他参考源：
http://3ms.huawei.com/km/blogs/details/12307789
https://gitee.com/openharmony/community/blob/master/sig/sig_architecture/sig_architecture_cn.md#/openharmony/community/blob/master/sig/sig_architecture/meetings/OpenHarmony_thirdparty_opensource_software_selection_analysis_templateV1.0.pptx