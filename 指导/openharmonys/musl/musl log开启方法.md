# 获取Musl Log
Musl Log的系统属性配置在/etc/param/musl.para中，默认情况下为关闭状态：
```
musl.log.enable=false
```
## 获取MUSL Log有以下两种方式：

### 1. param(shell)指令
通过param指令配置仅在本次启动生效，重启开发板后恢复/etc/param/musl.para中的配置。

1) 连接开发板
```
hdc_std shell
```
2) 通过系统属性指令更改Musl Log系统属性
```
hdc shell hilog -Q pidoff
hdc shell hilog -G 1G
hdc shell param set musl.log.enable true
hdc shell param set musl.ld.debug.dlclose true
```
3) 查看Musl Log系统属性
```
param get musl.log.enable
```
4) 获取Log
```
hilog | grep MUSL

#### **2. 更改配置文件**

更改配置文件后，需重新启动方可配置生效。

1. 连接开发板
hdc_std shell

2. 获取写入权限

```\
mount -o remount,rw /
更改Musl Log系统属性

echo musl.log.enable=true > /etc/param/musl.para
echo musl.log.ld.all=true >> /etc/param/musl.para
重启开发板

reboot
重启开发板后，连接开发板

hdc_std shell
获取Log

hilog | grep MUSL
Linker Log按应用配置
Linker Log有以下3种常用配置：

开启所有应用的Linker Log
musl.log.ld.all=true
只开启指定应用的Linker Log（your_app_name为指定应用名，如com.ohos.camera）
musl.log.ld.all=false
musl.log.ld.app.your_app_name=true
开启除指定应用外的Linker Log
musl.log.ld.all=true
musl.log.ld.app.your_app_name=false
当musl.log.ld.all为true时，将输出所有应用的Log。由于Log输出较多，可能会导致系统卡顿，建议只开启指定应用的Log。

配置Linker Log有以下两种方式：

1. param(shell)指令
通过param指令配置仅在本次启动生效，重启开发板后恢复/etc/param/musl.para中的配置。

连接开发板

hdc_std shell
通过系统属性指令更改Linker Log系统属性，只开启系统相机应用的Linker Log

param set musl.log.enable true
param set musl.log.ld.all false
param set musl.log.ld.app.com.ohos.camera true
查看Linker Log系统属性

param get musl.log.enable
param get musl.log.ld.all
param get musl.log.ld.app.com.ohos.camera
开启应用，仅获取Linker Log

hilog | grep MUSL-LDSO
## 2. 更改配置文件
更改配置文件后，需重新启动方可配置生效。

1) 连接开发板
```
hdc_std shell
```
2) 获取写入权限
```
mount -o remount,rw /
```
3) 更改Linker Log系统属性，只开启系统相机应用的Linker Log
```
echo musl.log.enable=true > /etc/param/musl.para
echo musl.log.ld.all=false >> /etc/param/musl.para
echo musl.log.ld.app.com.ohos.camera=true >> /etc/param/musl.para
```
4) 重启开发板
```
reboot
```
5) 重启开发板后，连接开发板
```
hdc_std shell
```
6)开启应用，仅获取Linker Log
```
hilog | grep MUSL-LDSO
```
## 添加log信息
相关log函数在porting/linux/user/src/internal/musl_log.h中，使用时添加#include "musl_log.h"时即可引用到该头文件，之后使用MUSL_LOGI等函数即可添加log信息

## 替换musl
1) 首先需要自己编译ld-musl-aarch64.so.1
2) 获取权限：hdc shell "mount -o remount,rw /"
3) 将文件上传到机器中：hdc file send ld-musl-aarch64.so.1 /data/local/tmp
4) 添加执行权限：hdc shell "chmod +x /data/local/tmp/ld-musl-aarch64.so.1"
5) 替换：hdc shell "cp -f /data/local/tmp/ld-musl-aarch64.so.1 /lib/ld-musl-aarch64.so.1"

## 建议
开启musl的debug log时候可以先把这个关闭，不然日志太多会丢失

![输入图片说明](https://foruda.gitee.com/images/1699932538180264527/397c0806_10384282.png "屏幕截图")