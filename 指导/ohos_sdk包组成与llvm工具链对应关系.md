此处以llvm10（https://repo.huaweicloud.com/harmonyos/compiler/clang/10.0.1-480513）和
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony
3.1.7.5/20220810_020030/version-Release_Version-OpenHarmony 3.1.7.5-20220810_020030-
ohos-sdk.tar.gz举例说明

### 1 libc++_static.a 和 libc++_shared.so 文件对应关系
注意：ndk和ohos-sdk对应关系

|  文件名称 | llvm路径  | ohos_sdk路径  |
|---|---|---|
|libc++_static.a   |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\x86_64-linux-hos\c++\libc++_static.a   |version-Release_Version-OpenHarmony 3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\x86_64-linux-hos\c++\libc++_static.a   |
|   | libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-liteos-ohos\c++\a7_softfp_neon-vfpv4\libc++_static.a  | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_softfp_neon-vfpv4\libc++_static.a  |
|   | libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-liteos-ohos\c++\a7_softfp_neon-vfpv4\libc++_static.a  |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_softfp_neon-vfpv4\libc++_static.a   |
|   | libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-liteos-ohos\c++\a7_soft\libc++_static.a  | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_soft\libc++_static.a  |
|   |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-liteos-ohos\c++\a7_hard_neon-vfpv4\libc++_static.a   | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_hard_neon-vfpv4\libc++_static.a  |
|   | libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-liteos-ohos\c++\libc++_static.a  |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\libc++_static.a   |
|   |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-linux-ohos\c++\a7_softfp_neon-vfpv4\libc++_static.a   | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_softfp_neon-vfpv4\libc++_static.a  |
|   |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-linux-ohos\c++\a7_soft\libc++_static.a   | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_soft\libc++_static.a  |
|   | libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-linux-ohos\c++\a7_hard_neon-vfpv4\libc++_static.a  |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_hard_neon-vfpv4\libc++_static.a   |
|   | libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-linux-ohos\c++\libc++_static.a   | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\libc++_static.a   |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-linux-hos\c++\libc++_static.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-hos\c++\libc++_static.a |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\aarch64-linux-ohos\c++\libc++_static.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-ohos\c++\libc++_static.a |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\aarch64-linux-hos\c++\libc++_static.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-hos\c++\libc++_static.a |
|libc++_shared.so |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\x86_64-linux-hos\c++\libc++_shared.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\x86_64-linux-hos\c++\libc++_shared.so |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-liteos-ohos\c++\a7_softfp_neon-vfpv4\libc++_shared.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_softfp_neon-vfpv4\libc++_shared.so |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-liteos-ohos\c++\a7_soft\libc++_shared.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_soft\libc++_shared.so |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-liteos-ohos\c++\a7_hard_neon-vfpv4\libc++_shared.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_hard_neon-vfpv4\libc++_shared.so |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-liteos-ohos\c++\libc++_shared.so | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\libc++_shared.so|
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-linux-ohos\c++\a7_softfp_neon-vfpv4\libc++_shared.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_softfp_neon-vfpv4\libc++_shared.so |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-linux-ohos\c++\a7_soft\libc++_shared.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_soft\libc++_shared.so |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-linux-ohos\c++\a7_hard_neon-vfpv4\libc++_shared.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_hard_neon-vfpv4\libc++_shared.so |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-linux-ohos\c++\libc++_shared.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\libc++_shared.so |
| | libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\arm-linux-hos\c++\libc++_shared.so|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-hos\c++\libc++_shared.so |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\aarch64-linux-ohos\c++\libc++_shared.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-ohos\c++\libc++_shared.so |
| |libcxx-ndk-480513-windows-x86_64\libcxx-ndk\lib\aarch64-linux-hos\c++\libc++_shared.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-hos\c++\libc++_shared.so |

### 2 __config文件对应关系
注意：ndk和ohos-sdk对应关系
|文件名称   | llvm路径  |ohos_sdk路径   |
|---|---|---|
|__config   | libcxx-ndk-480513-windows-x86_64\libcxx-ndk\include\c++\v1\__config  | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\include\c++\v1\__config  |

### 3 share目录对应关系
|目录名称   | llvm路径  |ohos_sdk路径   |
|---|---|---|
| share|clang-480513-windows-x86_64-20220815\share\* |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\share\* |

### 4 libexec目录对应关系
|目录名称   | llvm路径  |ohos_sdk路径   |
|---|---|---|
|libexec |clang-480513-windows-x86_64-20220815\libexec\* |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\libexec\* |

### 5 bin目录对应关系
|目录名称   | llvm路径  |ohos_sdk路径   |
|---|---|---|
|bin |clang-480513-windows-x86_64-20220815\bin\* |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\bin\*|

### 6 libc++abi.a文件对应关系
|文件名称   | llvm路径  |ohos_sdk路径   |
|---|---|---|
|libc++abi.a |clang-480513-windows-x86_64-20220815\lib\x86_64-linux-hos\c++\libc++abi.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\x86_64-linux-hos\c++\libc++abi.a |
| |clang-480513-windows-x86_64-20220815\lib\arm-liteos-ohos\c++\a7_softfp_neon-vfpv4\libc++abi.a | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_softfp_neon-vfpv4\libc++abi.a|
| |clang-480513-windows-x86_64-20220815\lib\arm-liteos-ohos\c++\a7_soft\libc++abi.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_soft\libc++abi.a |
| | clang-480513-windows-x86_64-20220815\lib\arm-liteos-ohos\c++\a7_hard_neon-vfpv4\libc++abi.a|D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_hard_neon-vfpv4\libc++abi.a |
| |clang-480513-windows-x86_64-20220815\lib\arm-liteos-ohos\c++\libc++abi.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\libc++abi.a |
| |clang-480513-windows-x86_64-20220815\lib\arm-linux-ohos\c++\a7_softfp_neon-vfpv4\libc++abi.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_softfp_neon-vfpv4\libc++abi.a |
| |clang-480513-windows-x86_64-20220815\lib\arm-linux-ohos\c++\a7_soft\libc++abi.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_soft\libc++abi.a |
| | clang-480513-windows-x86_64-20220815\lib\arm-linux-ohos\c++\a7_hard_neon-vfpv4\libc++abi.a|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_hard_neon-vfpv4\libc++abi.a |
| |clang-480513-windows-x86_64-20220815\lib\arm-linux-ohos\c++\libc++abi.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\libc++abi.a |
| | clang-480513-windows-x86_64-20220815\lib\arm-linux-hos\c++\libc++abi.a|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-hos\c++\libc++abi.a |
| |clang-480513-windows-x86_64-20220815\lib\aarch64-linux-ohos\c++\libc++abi.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-ohos\c++\libc++abi.a |
| | clang-480513-windows-x86_64-20220815\lib\aarch64-linux-hos\c++\libc++abi.a|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-hos\c++\libc++abi.a |
| |clang-480513-windows-x86_64-20220815\lib\libc++abi.a | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\libc++abi.a|
### 7 10.0.1目录对应关系
| 目录名称  | llvm路径  |ohos_sdk路径   |
|---|---|---|
| 10.0.1  | clang-480513-windows-x86_64-20220815\lib\clang\10.0.1\*  |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\clang\10.0.1\*   |
### 8 v1目录对应关系
注意：除了__config外，其他均一致
| 目录名称  | llvm路径  |ohos_sdk路径   |
|---|---|---|
|v1 |clang-480513-windows-x86_64-20220815\include\c++\v1\* |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\include\c++\v1\* |
### 9 ohos-sdk的current目录对应关系
| 目录名称  | llvm路径  |ohos_sdk路径   |
|---|---|---|
|current |clang-480513-windows-x86_64-20220815\lib\clang\10.0.1\* |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\clang\10.0.1\* |
### 10 libunwind.a文件对应关系
| 文件名称  | llvm路径  |ohos_sdk路径   |
|---|---|---|
|libunwind.a | clang-480513-windows-x86_64-20220815\lib\x86_64-linux-hos\libunwind.a|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\x86_64-linux-hos\libunwind.a |
| |clang-480513-windows-x86_64-20220815\lib\arm-liteos-ohos\a7_softfp_neon-vfpv4\libunwind.a | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\a7_softfp_neon-vfpv4\libunwind.a|
| |clang-480513-windows-x86_64-20220815\lib\arm-liteos-ohos\a7_soft\libunwind.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\a7_soft\libunwind.a |
| |clang-480513-windows-x86_64-20220815\lib\arm-liteos-ohos\a7_hard_neon-vfpv4\libunwind.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\a7_hard_neon-vfpv4\libunwind.a |
| |clang-480513-windows-x86_64-20220815\lib\arm-liteos-ohos\libunwind.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\libunwind.a |
| |clang-480513-windows-x86_64-20220815\lib\arm-linux-ohos\a7_softfp_neon-vfpv4\libunwind.a | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\a7_softfp_neon-vfpv4\libunwind.a|
| | clang-480513-windows-x86_64-20220815\lib\arm-linux-ohos\a7_soft\libunwind.a| version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\a7_soft\libunwind.a|
| | clang-480513-windows-x86_64-20220815\lib\arm-linux-ohos\a7_hard_neon-vfpv4\libunwind.a|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\a7_hard_neon-vfpv4\libunwind.a |
| |clang-480513-windows-x86_64-20220815\lib\arm-linux-ohos\libunwind.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\libunwind.a |
| | clang-480513-windows-x86_64-20220815\lib\arm-linux-hos\libunwind.a|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-hos\libunwind.a |
| |clang-480513-windows-x86_64-20220815\lib\aarch64-linux-ohos\libunwind.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-ohos\libunwind.a |
| |clang-480513-windows-x86_64-20220815\lib\aarch64-linux-hos\libunwind.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-hos\libunwind.a |
### 11 libomp.a文件对应关系
|文件名称   | llvm路径  | ohos_sdk路径  |
|---|---|---|
|libomp.a   | clang-480513-windows-x86_64-20220815\lib\x86_64-linux-hos\libomp.a  |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\x86_64-linux-hos\libomp.a   |
|   |clang-480513-windows-x86_64-20220815\lib\arm-linux-hos\libomp.a   | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-hos\libomp.a  |
| |clang-480513-windows-x86_64-20220815\lib\aarch64-linux-hos\libomp.a | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-hos\libomp.a|
### 12 libomp5.a文件对应关系
|文件名称   | llvm路径  | ohos_sdk路径  |
|---|---|---|
|libomp5.a | clang-480513-windows-x86_64-20220815\lib\x86_64-linux-hos\libiomp5.a|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\x86_64-linux-hos\libiomp5.a |
| | clang-480513-windows-x86_64-20220815\lib\arm-linux-hos\libiomp5.a|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-hos\libiomp5.a | 
| | clang-480513-windows-x86_64-20220815\lib\aarch64-linux-hos\libiomp5.a|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-hos\libiomp5.a |
### 13 libgomp.a文件对应关系
|文件名称   | llvm路径  | ohos_sdk路径  |
|---|---|---|
|libgomp.a |clang-480513-windows-x86_64-20220815\lib\x86_64-linux-hos\libgomp.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\x86_64-linux-hos\libgomp.a | 
| | clang-480513-windows-x86_64-20220815\lib\arm-linux-hos\libgomp.a|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-hos\libgomp.a |
| |clang-480513-windows-x86_64-20220815\lib\aarch64-linux-hos\libgomp.a | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-hos\libgomp.a|
### 14 libarcher_static.a文件对应关系
|文件名称   | llvm路径  | ohos_sdk路径  |
|---|---|---|
| | clang-480513-windows-x86_64-20220815\lib\x86_64-linux-hos\libarcher_static.a|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\x86_64-linux-hos\libarcher_static.a |
| |clang-480513-windows-x86_64-20220815\lib\aarch64-linux-hos\libarcher_static.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-hos\libarcher_static.a |
### 15 libarcher.so文件对应关系
|文件名称   | llvm路径  | ohos_sdk路径  |
|---|---|---|
|libarcher.so |clang-480513-windows-x86_64-20220815\lib\x86_64-linux-hos\libarcher.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\x86_64-linux-hos\libarcher.so |
| | clang-480513-windows-x86_64-20220815\lib\aarch64-linux-hos\libarcher.so|version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-hos\libarcher.so |
### 16 libc++.a文件对应关系
|文件名称   | llvm路径  | ohos_sdk路径  |
|---|---|---|
|libc++.a |clang-480513-windows-x86_64-20220815\lib\libc++.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\libc++.a |
### 17 libc++.so和libc++.a文件对应关系
|文件名称   | ohos_sdk路径  | 功能说明  |
|---|---|---|
|libc++.so |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\x86_64-linux-hos\c++\libc++.so |链接符号，用于链接libc++_shared.so |
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_softfp_neon-vfpv4\libc++.so | 链接符号，用于链接libc++_shared.so|
| | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_soft\libc++.so|链接符号，用于连接libc++_shared.so |
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_hard_neon-vfpv4\libc++.so |链接符号，用于连接libc++_shared.so |
| | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\libc++.so|链接符号，用于连接libc++_shared.so |
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_softfp_neon-vfpv4\libc++.so | 链接符号，用于连接libc++_shared.so|
| | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_soft\libc++.so|链接符号，用于连接libc++_shared.so |
| | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_hard_neon-vfpv4\libc++.so|链接符号，用于连接libc++_shared.so |
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\libc++.so | 链接符号，用于连接libc++_shared.so|
| | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-hos\c++\libc++.so|链接符号，用于连接libc++_shared.so |
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-ohos\c++\libc++.so | 链接符号，用于连接libc++_shared.so|
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-hos\c++\libc++.so |链接符号，用于连接libc++_shared.so |
|libc++.a |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\x86_64-linux-hos\c++\libc++.a |链接符号，用于连接libc++_static.a和libc++abi.a |
| | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_softfp_neon-vfpv4\libc++.a| 链接符号，用于连接libc++_static.a和libc++abi.a|
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_soft\libc++.a|链接符号，用于连接libc++_static.a和libc++abi.a |
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\a7_hard_neon-vfpv4\libc++.a | 链接符号，用于连接libc++_static.a和libc++abi.a|
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-liteos-ohos\c++\libc++.a | 链接符号，用于连接libc++_static.a和libc++abi.a|
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_softfp_neon-vfpv4\libc++.a | 链接符号，用于连接libc++_static.a和libc++abi.a|
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_soft\libc++.a | 链接符号，用于连接libc++_static.a和libc++abi.a|
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\a7_hard_neon-vfpv4\libc++.a |链接符号，用于连接libc++_static.a和libc++abi.a |
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-ohos\c++\libc++.a  |链接符号，用于连接libc++_static.a和libc++abi.a |
| | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\arm-linux-hos\c++\libc++.a|链接符号，用于连接libc++_static.a和libc++abi.a |
| |version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-ohos\c++\libc++.a |链接符号，用于连接libc++_static.a和libc++abi.a | 
| | version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\llvm\lib\aarch64-linux-hos\c++\libc++.a|链接符号，用于连接libc++_static.a和libc++abi.a |



