### 相关资料
- [Overview — The LLDB Debugger (llvm.org)](https://lldb.llvm.org/design/overview.html#api)

- [OpenHarmony/lldb_tutorial.md · liwentao/wiki - 码云 - 开源中国 (gitee.com)](https://gitee.com/liwentao_uiw/wiki/blob/master/OpenHarmony/lldb_tutorial.md#https://gitee.com/link?target=https%3A%2F%2Flldb.llvm.org%2Fdesign%2Foverview.html%23api)

- [CppCon 2018: Simon Brand “How C++ Debuggers Work” - YouTube](https://www.youtube.com/watch?v=0DDrseUomfU)

### OHOS Platform
* [ELF clang/lib/Basic/TargetInfo.cpp · OpenHarmony/third_party_llvm-project - 码云 - 开源中国 (gitee.com)](https://gitee.com/openharmony/third_party_llvm-project/blob/master/clang/lib/Basic/TargetInfo.cpp)

* DWARF (source code address mapping)

* Arch

AARCH64/ARM/X86_64

* Ptrace

[lldb/source/Plugins/Process/POSIX · OpenHarmony/third_party_llvm-project - 码云 - 开源中国 (gitee.com)](https://gitee.com/openharmony/third_party_llvm-project/tree/master/lldb/source/Plugins/Process/POSIX)

tracing

read/write memory

read/write register

* [LLDB support lldb/source/Plugins/Platform/OHOS/PlatformOHOS.cpp · OpenHarmony/third_party_llvm-project](https://gitee.com/openharmony/third_party_llvm-project/blob/master/lldb/source/Plugins/Platform/OHOS/PlatformOHOS.cpp)

### Breakpoint
相关案例：[LLDB] [DevEco Studio]Time travel debug · Issue #I64UD1 · OpenHarmony/third_party_llvm-project - Gitee.com 

1. Hardware breakpoints

     set special regs which will result in breaks (limited num, can break on read/write/exec)
2. Software breakpoints

     unlimited num, can break on exec
```
dotraplinkage void notrace
do_int3(struct pt_regs *regs, long error_code) {
  //...
  debug_stack_usage_inc();
  cond_local_irq_enable(regs);
  do_trap(X86_TRAP_BP, SIGTRAP, "int3", regs, error_code, NULL);
  cond_local_irq_disable(regs);
  debug_stack_usage_dec();
}
```
invoke handler by trigger interupt

 
DWARF info

function name (mangling)
map address

### Stepping

相关案例：[[LLDB][DevEco stduio]Need to click "Step into" twice to enter the method .Need to click "Run to Cursor" twice to take effect · Issue #I64W4S](https://gitee.com/openharmony/third_party_llvm-project/issues/I64W4S?from=project-issue)
```
ptace(PTRACK_SINGLESTEP, debuggee_pid, nullptr, nullptr);
```
* Instruction

* Step over & Step in

    1. set breakpoint at return address

    2. next inst in this function or callee

* Step out

Read/Write Memory
* Read
```
ptrace(PTRACE_GETREGS, pid, nullptr, &regs);
```

* Write
```
auto data = ptrace(PTRACE_PEEKDATA, m_pid, address, nullptr);
data | = 1;
```
Expression Evaluation


### 命令
settings append targets.exec-search-paths "User/ide-debug/Myapplication/entry/build/default/intermediates/cmake/default/obj/arm64-v8a"

settings show

显示环境变量： _regexp-env
设置环境变量：_regexp-env <name>=<value>

