1. 例如，我的wsl中对应的windows机器的路径在/mnt
![输入图片说明](https://foruda.gitee.com/images/1675854553584540308/32cb8da2_10384282.jpeg "114aed52-3433-4ec3-96c7-e927265576d9.jpg")
2. hdc.exe在windows机器的E:\workSpace\toolchains盘路径放着，则对应的wsl上的路径为/mnt/e/workSpace/toolchains/
![输入图片说明](https://foruda.gitee.com/images/1675854609798046147/a0ee09c1_10384282.jpeg "35884b2b-8a56-4991-9f45-17ce217c6e29.jpg")
3. 将windows上hdc.exe的路径加入到wsl的环境变量里面export PATH=/mnt/e/workSpace/toolchains:$PATH （修改~/.bashrc即可）
![输入图片说明](https://foruda.gitee.com/images/1675854644005430159/9fcc7cff_10384282.jpeg "69075d4c-7b14-4e4d-9aa4-de21ac6f0f03.jpg")
4. 使得环境变量生效：source ~/.bashrc
5. 确认hdc.exe是否加入环境变量里面
![输入图片说明](https://foruda.gitee.com/images/1675854716493702354/3d634196_10384282.jpeg "5898a021-88fb-46d9-8ce1-3dfa0655effc.jpg")
6. 插入设备
7. 在wsl上运行hdc.exe list targets确认是否可以识别设备
![输入图片说明](https://foruda.gitee.com/images/1675854738627895559/4f85ec52_10384282.jpeg "a580ba2f-ecb1-4e95-ac47-7de66320360e.jpg")
8. 进入设备，运行lldb-server
![输入图片说明](https://foruda.gitee.com/images/1675854784489481712/95c982c5_10384282.jpeg "lldb-server.jpg")
9. 在wls里面windows对应路径的lldb.exe目录运行lldb进行连接调试
![输入图片说明](https://foruda.gitee.com/images/1675854821035521902/0537721c_10384282.jpeg "lldb.jpg")