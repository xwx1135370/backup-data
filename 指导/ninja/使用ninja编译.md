-v: 在使用ninja编译时，可以通过在命令行中添加参数`-v`或`--verbose`来显示编译过程。这样可以看到每个编译步骤的详细信息，包括编译命令、编译器输出等。

例子： 
```
pushd ${WORK_DIR}/llvm/out/llvm_make
../../prebuilts/cmake/linux-x86/bin/ninja check-lldb-unit | tee ${llvmts_logdir}/lldb_unit_${TIME}.txt
./bin/llvm-lit -sv tools/lldb/test/Shell/ --max-time 300  | tee ${llvmts_logdir}/lldb_shell_${TIME}.txt &
./bin/llvm-lit -sv tools/lldb/test/API/ --max-time 300  | tee ${llvmts_logdir}/lldb_api_${TIME}.txt &
popd
```

`ninja -d stats -d explain -v check-lldb-unit`
1. `-d stats`：输出构建统计信息，包括每个目标的构建时间、生成的文件数量等。

2. `-d explain`：输出构建过程中每个命令的详细信息，包括命令行参数、输入输出文件等。

3. `-v`：输出详细的构建信息，包括每个目标的依赖关系、构建命令等。

4. `check-lldb-unit`：指定要构建的目标名称。

用ninja指定下生成物路径
ninja -C <build_dir> --verbose libc  > <output_dir>/<output_file>
