### C++远程调试步骤
- 编译debug版本
- 检测板子连接状态/版本/是否可执行
- 推调试版本/数据/模型到板子上
- 推lldb-server以及相关so到板子上
- 修改lldb-server以及相关so权限
- 启动lldb-server开调试端口
- IDE启动lldb-mi，设置远程调试，连接lldb-server端口，设置工作目录，设置待调试文件，启动调试
### 环境准备
1. 小海思板端环境
```
#uname -a
Linux (none) 4.14.171_hi3796cv300 #1 SMP Tue Jun 2 11:08:05 CST 2020 aarch64 GNU/Linux
[1]+   Done                       telnets
#
```
- 挂载目录
```
mount -t nfs -o nolock 10.247.21.199:/home/nfs/0521 /root
```
```
#df -h
Filesystem                      Size        Used     Available Use%  Mounted on
/dev/root                     372.0M      295.3M      68.9M    81%     /
devtempfs                       2.5G        24.0K      2.5G     0%     /dev
10.247.21.199：/home/nfs/0521  94.2G       49.1G      40.9G    55%     /root
```
- 复制driver和so文件到板端
```
 cp -af /root/driver/lib64/share/* /usr/lib64 &&

    cp -af /root/driver/obj64/sample/npu/ide_daemon /etc &&

    cp -af /root/driver/obj64/sample/npu/slog /etc &&

    cp -af /root/driver/obj64/sample/npu/profiling /etc &&

    cp /root/pkg/acllib/lib64/libascendcl.so /usr/lib64 &&

    cp /root/pkg/acllib/lib64/libruntime.so /usr/lib64
```
- 启动ada和slog服务
```
cd /etc/slog ——slog进程

cd /etc/ide-deamon ——ada 进程

./se*.sh start
```

2 本地交叉编译环境
编译SOD debug版本需要此步骤
- 安装编译器（交叉编译工具）
下载aarch64-hi100-linux.tgz
解压到某目录
- 设置环境变量
以MinStudio安装用户在安装环境上
方式1 指定如下命令设置环境变量
```
export PATH=$PATH:/XXXXXX/aarch64-hi100-linux/bin——XXX安装目录

export LANG="C"
```
但是每次打卡新会话都要重新export变量

方式2 写入环境变量一劳永逸
```
vi ~/.bashrc 末尾增加上面的两行export

source ~/.bashrc
```
### LLDB远程调试
1 server端
- 环境准备
![输入图片说明](https://foruda.gitee.com/images/1676856887491180506/111c1986_10384282.png "server.PNG")
- 导入lldb动态库，启动lldb-server打开调试端口
```
export LD_LIBRARY_PATH=$(dirname $0)/../../../lldb:$LD_LIBRARY_PATH

./lldb-server platform --listen "*:18967" –server
```
2 client客户端
- 建立/Open Ascend-APP工程，编译SOC & Debug版本
Build Configuration@dggphicprd32872
Configuration Name | Build-Configuration
Target             | SOC
Target OS          | lhisilinux
Target Architecture| aarch64
Build Type         | Debug
- 启动lldb-mi建立连接
```
cd /home/custom/gao-mindstudio/MindStudio-ubuntu/tools/llvm/bin

./lldb-mi
platform select remote-linux——指定远程调试模式
platform connect connect://10.175.82.187:18967——远端IP:调试端口
platform settings -w /root/HIAI_PROJECTS/workspace_mind_studio/MyApp42_e0ea75b4/out
file workspace_mind_studio_MyApp42——指定远端可执行文件所在目录和文件名称
```
![输入图片说明](https://foruda.gitee.com/images/1676857416847608386/6c7134da_10384282.png "platform.PNG")
server端收到连接请求并响应：
![输入图片说明](https://foruda.gitee.com/images/1676857535325137069/a58863d1_10384282.png "server-connect.PNG")
- 启动调试
breakpoint set file main -line 18  ———————————设置断点
run                                ————————————启动调试
.....(准备工作已完成，可以开始执行各种lldb调试指令)