完成了把ramdisk从boot.img (hi3516平台是boot.img，rk3568平台是boot_linux.img)拆出作为单独分区镜像
hi3516 第一次烧写包含ramdisk.img的镜像前需要先进行一次擦除全器件，烧写成功后，后续再烧不需要擦除(2022年5月9日主干分支上午11点之后的版本）
### 环境准备

1. 软件包下载
软件包下载路径: http://www.hihope.org/download/download.aspx
软件包名称：Hi3516-HiTool 、HiUSBBurnDriver、usb-patch-for-win8win10、CH341SER
2. 烧录工具安装
- Hitool![输入图片说明](https://foruda.gitee.com/images/1661911814487151910/48b544fa_10384282.png "屏幕截图")直接双击Hitool.exe文件
- 双击HiUSBBurnDriver目录下的InstallDriver.exe安装USB驱动.
![输入图片说明](https://foruda.gitee.com/images/1661911907021520174/974313b1_10384282.png "屏幕截图")
- 双击usb-patch-for-win8win10.reg，安装注册。
![输入图片说明](https://foruda.gitee.com/images/1661911946951127865/bc5e9c7f_10384282.png "屏幕截图")
3. 串口转USB驱动安装
运行DEVSETUP64.exe或SETUP.EXE
![输入图片说明](https://foruda.gitee.com/images/1661911982852361774/a9dd20f8_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1661911991458287318/9d3b50dd_10384282.png "屏幕截图")
右击“计算机”，“管理”中查看“设备管理器”中端口是否安装成功。
（注意：一般查看端口是否安装成功前，需要先接入板子上的“串口USB”线，然后重启一下本地计算机）
![输入图片说明](https://foruda.gitee.com/images/1661912007969776797/449e1f57_10384282.png "屏幕截图")
4. 检查PC是否正确连接开发板的串口
- 运行“MobaXterm”工具
- 选择“Session”->“serial”,选择正确的串口和速率创建会话
![输入图片说明](https://foruda.gitee.com/images/1661912101880591262/94ecbcad_10384282.png "屏幕截图")
- 将串口转USB线缆的4pin插头插入开发板。
[图片上传中…(image-Ku5zYSeDvjXgVcimjBnb)]
- 给开发板断电再上电（注意此时开发板后方的Type-C线缆不要连接PC的USB口（Type-C线具有上点功能），否则断电操作会失败）
- MobaXterm能看到打印信息，说明PC正确连接开发板的串口。
[图片上传中…(image-nZzBj9rSP1X3TjJSe6cC)]
- 退出MobaXterm，避免串口占用导致后续烧录失败
### 镜像包准备
1. 获取开发板类型为Hi3516DV300对应的构建成功的组件包
http://ci.openharmony.cn/dailybuilds
![输入图片说明](https://foruda.gitee.com/images/1661912837680102926/a7ed37ce_10384282.png "屏幕截图")
2. 获取镜像文件
需要的镜像文件在上面下载的“hispark_taurus_L2”组件包里。
[图片上传中…(image-fxLgQjz6dFLvbNuREVVP)]

### 板子烧录
- 连接开发板背后的Type-C线缆和开发板的串口线（在reset按键旁边）
- 运行HiTool烧录工具
- 选择正确的开发板型号”Hi3516DV300”
[图片上传中…(image-1SOsWzdWtkIqWrBp5V3T)]
- 选择“USB口”和“烧写eMMC”，确认各项设置正确
![输入图片说明](https://foruda.gitee.com/images/1661914524139747588/dbede848_10384282.png "屏幕截图")
- 将Type-C数据先插入开发板背后端口，但是不要连接PC上的USB端口
[图片上传中…(image-0w4kNI9TcD3zlHv5PKy1)]
- 点击“烧写”按钮，按住开发板Reset按钮不放，将Type-C线连接PC上的USB口
[图片上传中…(image-XxTAcyBnxTncxWZGr7Ub)]
- 当出现进度条，说明烧录已经开始了，可以松开Reset键。
![输入图片说明](https://foruda.gitee.com/images/1661914598901031143/3727c67b_10384282.png "屏幕截图")
- 等待进度条完成，HiTool的控制台页面会有烧录过程提示
- 当出现类似提示，表示烧录成功
![输入图片说明](https://foruda.gitee.com/images/1661914620693677077/ea8bd4b3_10384282.png "屏幕截图")

### 问题
1. 烧录完成，板子启动不起来
解决办法：使用MobaXterm连接单板,进行如下设置即可
```
setenv bootargs 'mem=640M console=ttyAMA0,115200 mmz=anonymous,0,0xA8000000,384M clk_ignore_unused androidboot.selinux=permissive rootdelay=10 Hi3516usbmode=host hardware=Hi3516DV300 init=/init root=/dev/ram0 rw blkdevparts=mmcblk0:1M(boot),15M(kernel),20M(updater),2M(misc),3307M(system),256M(vendor),-(userdata)'; setenv bootcmd 'mmc read 0x0 0x82000000 0x800 0x4800; bootm 0x82000000' saveenv reset
```
2. 执行hdc_std shell提示“[Fail]ExecuteCommand need connect-key?”
![输入图片说明](https://foruda.gitee.com/images/1661914687255375525/1cc19a20_10384282.png "屏幕截图")
解决办法：1）更新板子上的插线，排除线的问题；2）更换连接的主机上的端口，排除端口占用问题；3）设置启动项，排除环境设置问题；4）更换板子，排除板子问题；5）设置启动项；6）防止进程占用
hdc_std.exe kill  
	hdc_std.exe -l4 start  
	hdc_std.exe list targets
hdc_std.exe shell


使用ipop工具设置启动项命令（也可使用MobaXterm）：
步骤1 连接串口线，断开板子后面的type-C线
步骤2 打开IPOP中的“终端工具“工具，进行设置。
[图片上传中…(image-yu8C5wLhBCGGVuM21qVM)]
setenv bootargs 'mem=640M console=ttyAMA0,115200 mmz=anonymous,0,0xA8000000,384M clk_ignore_unused androidboot.selinux=permissive rootdelay=10 hardware=Hi3516DV300 init=/init root=/dev/ram0 rw blkdevparts=mmcblk0:1M(boot),15M(kernel),20M(updater),2M(misc),3307M(system),256M(vendor),-(userdata)'; setenv bootcmd 'mmc read 0x0 0x82000000 0x800 0x4800; bootm 0x82000000' saveenv reset

3. 安装驱动之后，连接串口转USB线到本地计算机，设备管理器中找不到USB端口或者USB端口总是丢失问题
解决办法：
1）确保安装工具版本匹配一致，最好是新版本；
2）路径安装路径保持全英文；
3）安装完工具之后，一定要重启本地计算机；
4）若还是识别存在问题，可更换与本机计算机连接的USB端口
