### gdb静态调试工具获取

https://github.com/marcinguy/arm-gdb-static
![输入图片说明](https://foruda.gitee.com/images/1662341791091449905/06ff9a19_10384282.png "屏幕截图")
### 将调试工具和可执行文件以及源码传到Hi3516DV300开发板上
- 将pc上的gdbserver传到开发板上（可忽略步骤）：hdc_std file send F:\gitHub\arm-gdb-static\gdbserver /data/local/tmp
- 将pc上的gdbstatic传到开发板上：hdc_std file send F:\gitHub\arm-gdb-static\gdbstatic /data/local/tmp
- 将pc上的可执行文件Cons传到开发板上：hdc_std file send F:\gitHub\arm-gdb-static\Cons /data/local/tmp
- 将pc上的源代码传到开发板上：hdc_std file send D:\lldb\xuyao\hello.cpp /data/local/tmp
### gdb调试OHOS系统可执行文件
- 远程执行命令或进入交互命令环境（cmd命令行输入）：hdc_std shell
- 进入调试工具和可执行文件所在路径：cd /data/local/tmp
- 添加执行权限：chmod +x Cons gdbserver gdbstatic
- 启动调试：./gdbstatic Cons
- 查看源码：l
问题：查看源码时会提示找不到源码文件（编译源码是在pc上的D:\lldb\xuyao路径下使用clang编译的）
![输入图片说明](https://foruda.gitee.com/images/1662341948192232330/e37f4686_10384282.png "屏幕截图")
解决：使用GDB重定位源文件目录
(gdb)set substitute-path D:\lldb\xuyao /data/local/tmp
清除所有的自定义的源文件搜索路径信息:
(gdb)directory 
(gdb)dir
加一个源文件路径到当前路径的前面，指定多个路径，可以使用“:”
(gdb)dir dirname
(gdb)dir /opt/:/usr/include/
显示定义了的源文件搜索路径：
(gdb)show dir
- 设置日志文件：set logging file filename
- 开始拷贝：set loggin on
- 在第41行“{”处打断点：b 41
- 在构造函数调用处打断点
- 运行可执行文件：r
- 进行步入调试
- 停止日志拷贝:set logging off
- 退出gdb,进入filename中，查看所有线程堆栈


https://github.com/hugsy/gdb-static
arm选gdb-7.10.-arm6v
aarch64选gdb-8.3.1-aarch64-le