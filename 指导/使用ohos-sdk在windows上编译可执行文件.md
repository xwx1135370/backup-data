使用ohos-sdk在windows上编译可执行文件

 **arm32位** 
```
D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build-tools\cmake\bin\cmake.exe -G "Ninja" -D OHOS_STL=c++_shared -D OHOS_ARCH=armeabi-v7a -D CMAKE_MAKE_PROGRAM=D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build-tools\cmake\bin\ninja.exe -D OHOS_PLATFORM=OHOS -D CMAKE_TOOLCHAIN_FILE=D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build\cmake\ohos.toolchain.cmake ..

D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build-tools\cmake\bin\cmake.exe --build .
```
或者
```
D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build-tools\cmake\bin\cmake.exe -G "MinGW Makefiles" -D OHOS_STL=c++_shared -D OHOS_ARCH=armeabi-v7a -D OHOS_PLATFORM=OHOS -D CMAKE_TOOLCHAIN_FILE=D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build\cmake\ohos.toolchain.cmake ..

D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build-tools\cmake\bin\cmake.exe --build .
```

 **aarch64位**
 ```
D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build-tools\cmake\bin\cmake.exe -G "Ninja" -D OHOS_STL=c++_shared  -D CMAKE_MAKE_PROGRAM=D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build-tools\cmake\bin\ninja.exe -D OHOS_PLATFORM=OHOS -D CMAKE_TOOLCHAIN_FILE=D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build\cmake\ohos.toolchain.cmake ..

D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build-tools\cmake\bin\cmake.exe --build .
```
或者
```
D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build-tools\cmake\bin\cmake.exe -G "MinGW Makefiles" -D OHOS_STL=c++_shared -D OHOS_PLATFORM=OHOS -D CMAKE_TOOLCHAIN_FILE=D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build\cmake\ohos.toolchain.cmake ..

D:\version-Release_Version-OpenHarmony3.1.7.5-20220810_020030-ohos-sdk\ohos-sdk\windows\native-windows-3.1.7.5-Release\native\build-tools\cmake\bin\cmake.exe --build .
```

**编译参数说明**
- OHOS_STL
可选值`c++_shared`(表动态编译)和`c++_static`（表静态编译）

- OHOS_ARCH 指定架构
详细参考 https://gitee.com/openharmony/build/blob/master/ohos/ndk/cmake/ohos.toolchain.cmake#93
可选值`arm64-v8a`,`armeabi-v7a`和`x86_64`

