![输入图片说明](https://foruda.gitee.com/images/1678351580494145189/14bfbecf_10384282.png "屏幕截图")
环境：7.189.3.108 xuyao 123456 fVyS!6tzK9ZU
** 蓝区搞测试套门禁时候遇到的问题 **
这两个issues是当时从黄区切换到蓝区时，搞得测试的东西遇到的嘛？
https://gitee.com/openharmony/third_party_llvm-project/issues/I5P1TS?from=project-issue

https://gitee.com/openharmony/third_party_llvm-project/issues/I5QKB7?from=project-issue


https://rnd-gitlab-msc.huawei.com/rus-os-team/compilers/llvm/llvm-test-suite

[跑测试套时遇到问题，俄延回复](https://gitee.com/xwx1135370/llvm-related-issues/wikis/%E6%8C%87%E5%AF%BC/%E5%85%B3%E4%BA%8Edocker-%E4%BF%84%E5%BB%B6%E7%9A%84%E5%9B%9E%E5%A4%8D)
 **黄区or绿区** 
https://llvm.org/docs/TestSuiteGuide.html

https://rnd-gitlab-msc.huawei.com/rus-os-team/compilers/testing/automation/llvm-ci/-/blob/master/docker/Dockerfile.build
https://confluence-msc.rnd.huawei.com/display/Compilers2025/LLVM+CI+description
![输入图片说明](https://foruda.gitee.com/images/1675654841954253251/d5be90f0_10384282.png "屏幕截图")
```
# Pull a docker container CI build used to run tests
docker pull nexus.devops-spb.rnd.huawei.com/huawei/llvm-build:latest
 
# Run a container with /bin/bash entry point and remove after usage
# Please note: change <host-host-repo-path> to the directory with HOS toolchain you synchronized with repo init && repo sync
docker container run -it --network=host --privileged --entrypoint /bin/bash --rm -v <host-hos-repo-path>:/data/hos-toolchain nexus.devops-spb.rnd.huawei.com/huawei/llvm-build:latest
 
# Go to HOS toolchain
cd /data/hos-toolchain
 
# Start complete build process (linux, windows, libs, check-api, ...)
cd toolchain/llvm-build
./build.py
 
# An example of fast build for linux only
./build.py --no-lto --no-build windows,libs,check-api,lldb-mi
 
# Cd into build output directory
cd $WORKSPACE/out/llvm_make
 
# Set-up LD_LIBRARY_PATH correctly
export LD_LIBRARY_PATH="$WORKSPACE/llvm_make/lib:$WORKSPACE/prebuilts/libedit/linux-x86/lib"
 
# Run all set of unit tests using `ninja`
ninja check-all
```
Here <host-hos-repo-path> - is a path to a toolchain sources

比如，在docker外的/home/chenhao/workspace/Hsu/llvm_master目录下下载了llvm的代码，则<host-hos-repo-path>就是/home/chenhao/workspace/Hsu/llvm_master
![输入图片说明](https://foruda.gitee.com/images/1675771172061702704/4d8636d5_10384282.png "屏幕截图")


 **蓝区**
 https://gitee.com/xwx1135370/wiki/blob/master_hsu/OpenHarmony/llvm-project_test.md

https://gitee.com/openharmony/third_party_llvm-project/issues/I5P1KY?from=project-issue


https://github.com/llvm/llvm-test-suite
![输入图片说明](https://foruda.gitee.com/images/1678930892686769244/61942849_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1678930907842406037/6c34ee0f_10384282.png "屏幕截图")
