### 1. 烧录HiSpark AICam Hi3516DV300单板

[烧录指导](https://gitee.com/xwx1135370/llvm-related-issues/wikis/%E6%8C%87%E5%AF%BC/%E7%83%A7%E5%BD%95HiSpark%20AICam%20Hi3516DV300%E6%9D%BF%E5%AD%90)

### 2. 烧录RK3568单板

[烧录指导](https://gitee.com/xwx1135370/llvm-related-issues/wikis/%E6%8C%87%E5%AF%BC/%E7%83%A7%E5%BD%95RK%203568%E5%BC%80%E5%8F%91%E6%9D%BF?sort_id=5995342)

### 3. 调试相关工具准备

 **工具来源自CI构建(黄区和蓝区都可)** 

获取路径1 CI每日构建链接： http://ci.openharmony.cn/dailys/dailybuilds
获取路径2 https://repo.huaweicloud.com/harmonyos/compiler/clang/?C=M&O=D
注意：lldb.exe与liblldb.dll搭配使用；lldb-mi.exe与LLVM.dll搭配使用。两组搭配任选其一即可。
- lldb.exe(windows-x86_64)：ohos-sdk压缩包中native-windows-xxxx-xxxx.zip中 (例如：version-Daily_Version-ohos-sdk-20220210_003125-ohos-sdk\ohos-sdk\windows\native-windows-3.1.5.1-Beta1\llvm\bin)
- liblldb.dll(windows-x86_64)：ohos-sdk压缩包中native-windows-xxxx-xxxx.zip中(例如：version-Daily_Version-ohos-sdk-20220210_003125-ohos-sdk\ohos-sdk\windows\native-windows-3.1.5.1-Beta1\llvm\bin)
- lldb-mi.exe(window-x86_64)：ohos-sdk压缩包中native-windows-xxxx-xxxx.zip中（例如： version-Daily_Version-ohos-sdk-20220210_003125-ohos-sdk\ohos-sdk\windows\native-windows-3.1.5.1-Beta1\llvm\bin）
- LLVM.dll(window-x86_64)：（例如：version-Daily_Version-ohos-sdk-20220210_003125-ohos-sdk\ohos-sdk\windows\native-windows-3.1.5.1-Beta1\llvm\bin）
（注意版本升级之后，LLVM.dll更名为libLLVM.dll）
- lldb-server：ohos-sdk压缩包中native-linux-xxxx-xxx.zip中
例如： 
（OHOS ARM 32位）version-Daily_Version-ohos-sdk-20220210_003125-ohos-sdk\ohos-sdk\linux\native-linux-3.1.5.1-Beta1\llvm\lib\clang\10.0.1\bin\arm-linux-ohos\lldb-server
 (HOS ARM 32位）version-Daily_Version-ohos-sdk-20220210_003125-ohos-sdk\ohos-sdk\linux\native-linux-3.1.5.1-Beta1\llvm\lib\clang\10.0.1\bin\arm-linux-hos\lldb-server
（OHOS ARM 64位）version-Daily_Version-ohos-sdk-20220210_003125-ohos-sdk\ohos-sdk\linux\native-linux-3.1.5.1-Beta1\llvm\lib\clang\10.0.1\bin\ aarch64-linux-ohos\lldb-server
（OHOS ARM 64位）version-Daily_Version-ohos-sdk-20220210_003125-ohos-sdk\ohos-sdk\linux\native-linux-3.1.5.1-Beta1\llvm\lib\clang\10.0.1\bin\ aarch64-linux-hos\lldb-server
（HOS x86-64 64位）version-Daily_Version-ohos-sdk-20220210_003125-ohos-sdk\ohos-sdk\linux\native-linux-3.1.5.1-Beta1\llvm\lib\clang\10.0.1\bin\ x86_64-linux-hos\\lldb-server

> **注意:**
> 调试的可执行程序要和调试工具基于相同的平台、相同操作系统
> 可通过file +可执行文件 获取详细信息

- hdc_std.exe(windows)：（例如:version-Daily_Version-ohos-sdk-20220210_003125-ohos-sdk\ohos-sdk\windows\toolchains-windows-3.1.5.1-Beta1）

### 4. [如何使用ohos-sdk编译可执行文件](https://gitee.com/xwx1135370/llvm-related-issues/wikis/%E6%8C%87%E5%AF%BC/%E4%BD%BF%E7%94%A8ohos-sdk%E5%9C%A8windows%E4%B8%8A%E7%BC%96%E8%AF%91%E5%8F%AF%E6%89%A7%E8%A1%8C%E6%96%87%E4%BB%B6)


### 5. 调试
- 使用hdc_std将lldb-server上传到开发板hdc_std file send E:\openHarmony\linux\lldb-server /data/local/tmp

- 使用hdc_std将可执行文件上传到开发板hdc_std file send E:\demo\build\Test /data/local/tmp

> **注意:**
> 如果编译的是.cpp文件，动态依赖库，还需要上传libc++_shared.so到/system/lib（或/system/lib64）目录。静态依赖库，则不需要
>  要挂载，否则上传不上去哈 。可以使用`mount -o rw,remount -t auto / `或 `hdc_std shell mount -o rw,remount /`


- 进入开发板：hdc_std shell
```
E:\demo\build>hdc file send E:\demo\build\Test /data/local/tmp
FileTransfer finish, Size:81768 time:24ms rate:3407.00kB/s

E:\demo\build>hdc shell
#cd /data/local/tmp/
#ls -l
total 16312
-rw-r--r-- 1 root 4493610    81768 1970-01-01 00:45 Test
drwx------ 2 root system      4096 1970-01-01 00:02 battery
-rw-r--r-- 1 root 4325674  1479893 1970-01-01 00:45 hdc.log
-rwxrwxrwx 1 root 4727082 14872752 1970-01-01 00:19 lldb-server
drwx------ 2 root system      4096 1970-01-01 00:02 ohos-fgu
drwx------ 2 root system      4096 1970-01-01 00:02 ohos_charger
-rwx------ 1 root 4727082   244228 1970-01-01 03:05 sh
#chmod 777 Test
#ls -l
total 16312
-rwxrwxrwx 1 root 4493610    81768 1970-01-01 00:45 Test
drwx------ 2 root system      4096 1970-01-01 00:02 battery
-rw-r--r-- 1 root 4325674  1482329 1970-01-01 00:46 hdc.log
-rwxrwxrwx 1 root 4727082 14872752 1970-01-01 00:19 lldb-server
drwx------ 2 root system      4096 1970-01-01 00:02 ohos-fgu
drwx------ 2 root system      4096 1970-01-01 00:02 ohos_charger
-rwx------ 1 root 4727082   244228 1970-01-01 03:05 sh
#
```
- 建立连接./lldb-server p --server --listen "*:8080"（或./lldb-server p --server --listen unix-abstract:///com.example.myapplication/platform-1648104534646.sock）


> **注意：**
> 1) 如果环境上连接着多台设备，可尝试如下方法，其中7001005458323933328a521c38183800为设备SN号
> 2) platform select remote-hos inner在半容器环境容器内调试时使用；
     platform select remote-hos或platform select remote-android在hos平台使用；
     platform select remote-ohos在ohos平台使用
>
> ./lldb-server p --server --listen unix-abstract:///com.example.myapplication/platform-1648104534646.sock
>
> D:\SDK\openharmony\9\native\llvm\bin>lldb
> (lldb) platform select remote-hos inner             //半容器连接
>   Platform: remote-hos
> Connected: no
> Container: yes
> (lldb) platform connect unix-abstract-connect:///[7001005458323933328a521c38183800]/com.example.myapplication/platform-1675926058847.sock


- cmd打开一个新窗口，运行可执行文件（可执行文件带用户输入）
```
E:\demo\build>hdc_std shell
#cd /data/local/tmp/
#./Test
Please input tow nums:

```

-  cmd打开一个新窗口，运行lldb
```
E:\openHarmony\\windows>lldb
(lldb) platform select remote-ohos    （如果调试的是RK板子或单框架ohos原型机，使用`platform select remote-ohos`;如果是API8 hos平台设备使用`platform select remote-hos`或platform select remote-Android）
 Platform: remote-ohos
Connected: no
(lldb) platform connect connect://localhost:8080（或platform connect unix-abstract-connect:///com.example.myapplication/platform-1648104534646.sock）
 Platform: remote-hos
   Triple: arm-unknown-linux-android
   Kernel: #1 SMP Mon Feb 21 14:10:40 CST 2022
 Hostname: localhost
Connected: yes
WorkingDir: /data/local/tmp
(lldb)

```
- 调试
```
(lldb) attach Test（或attach pid）
Process 2335 stopped
* thread #1, name = 'Test', stop reason = signal SIGSTOP
    frame #0: 0xb6f0671c ld-musl-arm.so.1
->  0xb6f0671c: svc    #0x0
    0xb6f06720: bl     0xb6ec73c4
    0xb6f06724: cmp    r0, #0
    0xb6f06728: ble    0xb6f0676c

Executable module set to "C:\Users\yaoxuy\.lldb\module_cache\remote-hos\.cache\50C5CF5D-F3C0-CFE2-EACC-B833719F2B97-3A966D7C\Test".
Architecture set to: arm-unknown-linux-android.
(lldb) b Add
Breakpoint 1: where = Test`Add + 12 at Test.c:5:13, address = 0x004ddac0
(lldb) b 21
Breakpoint 2: where = Test`main + 104 at Test.c:21:25, address = 0x004ddb5c
(lldb) b 25
Breakpoint 3: where = Test`main + 124 at Test.c:25:17, address = 0x004ddb70

```
- 在可执行文件窗口按要求输入参数

-  继续调试
```
(lldb) continue
Process 2335 resuming
Process 2335 stopped
* thread #1, name = 'Test', stop reason = breakpoint 2.1
    frame #0: 0x004ddb5c Test`main at Test.c:21:25
  18
19	     if ( i > j)
20       {
-> 21           k = subtraction(i,j);
  22       }
  23       else
  24       {
(lldb) s
Process 2335 stopped
* thread #1, name = 'Test', stop reason = step in
    frame #0: 0x004ddae0 Test`subtraction(a=89, b=2) at Test.c:10:13
   7
   8    int subtraction(int a, int b)
   9    {
-> 10       return (a - b);
   11   }
   12
   13   int main()
(lldb) print a
(int) $0 = 89
(lldb) print b
(int) $1 = 2
(lldb) n
Process 2335 stopped
* thread #1, name = 'Test', stop reason = step over
    frame #0: 0x004ddb68 Test`main at Test.c:21:11
   18
   19       if ( i > j)
   20       {
-> 21           k = subtraction(i,j);
   22       }
   23       else
   24       {
(lldb) print j
(int) $2 = 2
(lldb) print i
(int) $3 = 89
(lldb) n
Process 2335 stopped
* thread #1, name = 'Test', stop reason = step over
    frame #0: 0x004ddb6c Test`main at Test.c:22:5
   19       if ( i > j)
   20       {
   21           k = subtraction(i,j);
-> 22       }
   23       else
   24       {
   25           k = Add(i,j);
(lldb) n
Process 2335 stopped
* thread #1, name = 'Test', stop reason = step over
    frame #0: 0x004ddb88 Test`main at Test.c:28:23
   25           k = Add(i,j);
   26       }
   27
-> 28       printf("k = %d\n",k);
   29       return 0;
   30   }
(lldb) n
Process 2335 stopped
* thread #1, name = 'Test', stop reason = step over
    frame #0: 0x004ddba8 Test`main at Test.c:29:5
   26       }
   27
   28       printf("k = %d\n",k);
-> 29       return 0;
   30   }
(lldb) print k
(int) $4 = 87
```


> **注意：** 
> - 如果可执行文件不带输入，调试时使用file /data/local/tmp/Test指定
> - 一般如果在设备上运行可执行文件Test时，如果报错“/bin/sh Test: No such file or directory”。都是可执行文件与设备架构不一致导致的。可以file命令确认可执行文件架构，file命令确认设备上能运行的应用的架构进行确认。
> - 在设备上运行可执行文件Test是，如果报错"xxxxx libc++_shared.soxxxx",则可能是设备的/system/lib目录（或/system/lib64目录）没有libc++_shared.so这个库。


命令扩充：
- 设置加载库路径
  settings append target.exec-search-paths "C:\Users\s00653037\DevEcoStudioProjects\MyApplication75\entry\build\default\intermediates\cmake\default\obj\arm64-v8a"
- 加载符号
  target symbols add xxx.so
- 设置源码路径
  [说明]：其中“/buildbot/path”为原始代码编译时的路径，“/my/path”为当前调试机器上源代码路径
  [使用场景]：当需要调试的ELF文件和调试的机器不是同一台机器时使用。比如，在linux机器上编译的so,在windows平台调试时；比如，编译和调试都是在windows平台，但是编译的机器和调试的机器不是同一台。
  settings set target.source-map /buildbot/path /my/path  

其他详细命令参考 https://github.com/llvm-mirror/lldb/blob/master/docs/use/map.rst 或 https://lldb.llvm.org/use/map.html

```
pro handle SIGRTMIN+1 -p true -s false -n true
```
```
pro handle SIGCHLD
```

```
lldb process:gdb-remote packets:posix all
```

调试openharmony某个子系统的，除了将子系统的应用带debug信息外，还需要编译一个带debug信息的libc.so替换到设备的/system/lib(或/system/lib64)目录下，同时也要换ld-musl-arm.so.1(或ld-musl-aarch64.so)[ld-musl文件实际上是lic.so更名后的文件]