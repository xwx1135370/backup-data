Nholiavin Nikolai(wx1139628)2023-01-19 19:07
Hi! I asked around about the docker image, and it seems this dockerfile: https://gitee.com/urandon/third_party_llvm-project/blob/llvm-15.0.4-ohos-fix-build/llvm-build/Dockerfile is enough to build successfully. I'll try to test it too to be sure

Kholiavin Nikolai(wx1139628)2023-01-19 20:55
Kholiavin Nikolai
Hi! I asked around about the docker image, and it seems this dockerfile: https://gitee.com/urandon/third_party_llvm-project/blob/llvm-15.0.4-ohos-fix-build/llvm-build/Dockerfile is enough to build successfully. I'll try to test it too to be sure
Well the build passes, but at least lldb tests require that there is a regular non-root user running them. In the repo that you linked I found this: https://gitee.com/liwentao_uiw/llvmopen-source-transfer-gitee/blob/ohos_llvm_support/Dockerfile.build , which is an older version of docker image that is used in gitlab CI. The part relating to the creation of the user can be copied to the gitee dockerfile (also I think sudo is missing in the source image, it's needed to install it too)

Kholiavin Nikolai
Well the build passes, but at least lldb tests require that there is a regular non-root user running them. In the repo that you linked I found this: https://gitee.com/liwentao_uiw/llvmopen-source-transfer-gitee/blob/ohos_llvm_support/Dockerfile.build , which is an older version of docker image that is used in gitlab CI. The part relating to the creation of the user can be copied to the gitee dockerfile (also I think sudo is missing in the source image, it's needed to install it too)
So, all in all, you can see if https://gitee.com/nkholiavin/third_party_llvm-project/blob/dockerfile/llvm-build/dockerfile-18.04 (ubuntu 18, based on some openharmony image) or https://gitee.com/nkholiavin/third_party_llvm-project/blob/dockerfile/llvm-build/dockerfile-20.04 (based on pure ubuntu 20, with all the packages listed there) work for you. To build you should mount the usual prepared environment to be accessible from the container. At least one lldb test fails on that setup (Reproducer/TestProcessList.test), but other than that should work fine.
