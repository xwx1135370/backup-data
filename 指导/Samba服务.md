https://www.dbs724.com/420181.html

要在Linux服务器上安装Samba，可以按照以下步骤操作：

打开终端窗口，以管理员权限登录到服务器。

更新服务器软件包列表：

```
sudo apt update
```
安装Samba软件包：
```
sudo apt-get install samba
```
安装完成后，编辑Samba配置文件：
```
sudo nano /etc/samba/smb.conf
```
在配置文件中，您可以设置共享目录、访问权限等。根据您的需求进行配置。

保存并退出配置文件。

检查Samba配置文件的语法是否正确：
```
testparm
```
如果没有错误信息，则表示配置文件正确。

创建Samba用户并设置密码：
```
sudo smbpasswd -a 用户名
```
替换"用户名"为您要创建的Samba用户的实际用户名。然后输入密码并确认。

重启Samba服务使配置生效：
```
sudo systemctl restart smbd
```
或者，如果您的服务器使用的是SysV init系统：

```
sudo service smbd restart
```
配置服务器防火墙，允许Samba服务的通信。

如果您使用的是ufw防火墙，可以运行以下命令启用Samba服务：

```
sudo ufw allow samba
```

建议建立两个账户，一个是普通账户，日常编译开发用；一个有sudo权限的管理员账户（不建议直接root账户操作，形使root权限时需要加sudo命令并输密码，比较安全）

创建普通账号

useradd --create-home -s /bin/bash username

passwd username (在出现的密码提示符中，给user指定登录密码)

创建管理员账号

useradd --create-home -s /bin/bash username

passwd username (在出现的密码提示符中，给myadmin指定登录密码)

usermod -aG sudo username