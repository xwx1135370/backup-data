如果你在 Python 脚本中使用了编译参数，可以通过以下两种方式将这些参数传递给 Shell 脚本：

1. 使用 `subprocess` 模块在 Python 脚本中启动 Shell 脚本，并将编译参数作为命令行参数传递给 Shell 脚本。例如：

```python
import subprocess

# 编译参数
compile_args = ["-O3", "-march=native"]

# 启动 Shell 脚本，并将编译参数作为命令行参数传递
subprocess.call(["./build.sh"] + compile_args)
```

在 Shell 脚本中，可以通过 `$1`、`$2` 等变量获取传递的命令行参数。

2. 在 Python 脚本中生成一个包含编译参数的配置文件，然后在 Shell 脚本中读取该配置文件。例如：

```python
# 编译参数
compile_args = ["-O3", "-march=native"]

# 将编译参数写入配置文件
with open("config.txt", "w") as f:
    f.write("
".join(compile_args))

# 启动 Shell 脚本
subprocess.call(["./build.sh"])
```

在 Shell 脚本中，可以通过 `source` 命令读取配置文件中的参数。例如：

```bash
# 读取配置文件中的参数
source config.txt

# 使用参数编译代码
gcc $compile_args -o my_program my_program.c
```