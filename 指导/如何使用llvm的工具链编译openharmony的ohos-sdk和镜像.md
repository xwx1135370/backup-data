### 1. openharmony代码下载与编译
[代码下载与编译](https://gitee.com/xwx1135370/llvm-related-issues/wikis/%E6%8C%87%E5%AF%BC/%E6%8B%89%E4%BB%A3%E7%A0%81%E7%BC%96%E8%AF%91)

### 2. 替换llvm工具链编译openharmony的ohos-sdk包和镜像包

  可以结合以下文件查看到llvm工具链的来源以及去向

  https://gitee.com/openharmony/build/blob/master/prebuilts_download.sh#L159

  https://gitee.com/openharmony/build/blob/master/prebuilts_download_config.json#L160

   ![输入图片说明](https://foruda.gitee.com/images/1671078365540269355/48827cb3_10384282.png "屏幕截图")
   https://repo.huaweicloud.com/openharmony/compiler/clang/12.0.1-36cd05/

举例说明：
- 将clang_linux-x86_64-36cd05-20221030.tar.bz2解压，然后放在~/workspace/Hsu_master/master/prebuilts/clang/ohos/linux-x86_64/llvm目录下
![输入图片说明](https://foruda.gitee.com/images/1671075497038140715/1139c0f8_10384282.png "屏幕截图")
- 将libcxx-ndk_linux-x86_64-36cd05-20221030.tar.bz2解压，然后放在~/workspace/Hsu_master/master/prebuilts/clang/ohos/linux-x86_64/libcxx-ndk目录下
![输入图片说明](https://foruda.gitee.com/images/1671075524490248758/f03d1f7a_10384282.png "屏幕截图")
- 拷贝~/workspace/Hsu_master/master/prebuilts/clang/ohos/linux-x86_64/llvm/include和~/workspace/Hsu_master/master/prebuilts/clang/ohos/linux-x86_64/libcxx-ndk/include/libcxx-ohos/include目录下所有的文件到~/workspace/Hsu_master/master/prebuilts/clang/ohos/linux-x86_64/llvm_ndk
- 将clang_windows-x86_64-36cd05-20221030.tar.bz2解压，然后放在~/workspace/Hsu_master/master/prebuilts/clang/ohos/windows-x86_64/llvm目录下
![输入图片说明](https://foruda.gitee.com/images/1671075562744176031/dbe66013_10384282.png "屏幕截图")
- 将libcxx-ndk_windows-x86_64-36cd05-20221030.tar.bz2解压，然后放在~/workspace/Hsu_master/master/prebuilts/clang/ohos/windows-x86_64/libcxx-ndk目录下
![输入图片说明](https://foruda.gitee.com/images/1671075596406464737/045f7d94_10384282.png "屏幕截图")

注意：Openharmony编译不用llvm生成的sysroot中的文件，依赖来自https://gitee.com/openharmony/third_party_musl