### Mac arm64
![输入图片说明](https://foruda.gitee.com/images/1701308201630767934/9114b5b5_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1701308214672511263/31010fa1_10384282.png "屏幕截图")
- 编译完成
![输入图片说明](https://foruda.gitee.com/images/1701310864387996084/61a3d0b1_10384282.png "屏幕截图")
- 静态库编译成功
![输入图片说明](https://foruda.gitee.com/images/1701310868579982922/72f556c4_10384282.png "屏幕截图")
- 动态库没有编译出来
![输入图片说明](https://foruda.gitee.com/images/1701313443704144765/ac00414d_10384282.png "屏幕截图")
[查询结果](https://gitee.com/xwx1135370/llvm-related-issues/blob/master/arkui-x%E8%B7%A8%E5%B9%B3%E5%8F%B0%E9%AA%8C%E8%AF%81/libutil_dylib.txt)
- 不支持单独编译动态库
![输入图片说明](https://foruda.gitee.com/images/1701313477774837153/9246a857_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1701313492400191149/7113b3fe_10384282.png "屏幕截图")

### Mac x86-64
![输入图片说明](https://foruda.gitee.com/images/1701308249579174046/767ccbd4_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1701308258162825724/f90a3dc0_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1701308264441921180/0bfdc968_10384282.png "屏幕截图")
- 编译完成
![输入图片说明](https://foruda.gitee.com/images/1701315628556272053/0a1080fa_10384282.png "屏幕截图")
- 静态库编译成功，动态库未编译出来
![输入图片说明](https://foruda.gitee.com/images/1701315692562002858/3df02450_10384282.png "屏幕截图")


### 打桩特殊处理
- 添加配置
   ![输入图片说明](https://foruda.gitee.com/images/1701655178432878681/56d8b2b3_10384282.png "屏幕截图")
- 编译
   `./build.sh --product-name arkui-x -target-os ios --gn-args enable_auto_pack=true --target-name commonlibrary/c_utils/base:utils`
   ![输入图片说明](https://foruda.gitee.com/images/1701671840103778629/eeb03406_10384282.png "屏幕截图")
   ![输入图片说明](https://foruda.gitee.com/images/1701672107172161030/666765ac_10384282.png "屏幕截图")
   ![输入图片说明](https://foruda.gitee.com/images/1701672660353815683/544fc551_10384282.png "屏幕截图")
- 动态库编译成功
   ![输入图片说明](https://foruda.gitee.com/images/1701671802157757934/8278133e_10384282.png "屏幕截图")
