### 问题1

![输入图片说明](https://foruda.gitee.com/images/1697781687572347995/14bf9f26_10384282.png "屏幕截图")

处理：路径修改为third_party/vixl,而不是third_party/vixl/

### 问题2

![输入图片说明](https://foruda.gitee.com/images/1697781615083382556/c85c20ba_10384282.png "屏幕截图")

处理：docker镜像中缺少库，修改编译命令为`
yes y | apt install libxinerama-dev libxcursor-dev libxrandr-dev libxi-dev && ./build.sh --product-name rk3568 --build-target libvixl_frontend_static`


### 问题3

![输入图片说明](https://foruda.gitee.com/images/1697781585416760843/a2da1220_10384282.png "屏幕截图")

规避：将找不到的组件加入白名单中

![输入图片说明](https://foruda.gitee.com/images/1697787635415212959/356cba6e_10384282.png "屏幕截图")


### sig门禁最终配置

![输入图片说明](https://foruda.gitee.com/images/1699258555423405043/c915c909_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1699258608179659940/43cf869b_10384282.png "屏幕截图")

编译命令:`yes y | apt install libxinerama-dev libxcursor-dev libxrandr-dev libxi-dev && ./build.sh --product-name rk3568 --build-target libvixl_frontend_static`
![输入图片说明](https://foruda.gitee.com/images/1699258699640555629/d8d2718b_10384282.png "屏幕截图")
关联仓预编译：`rm -rf co_build && git clone https://gitee.com/openharmony-sig/cicd co_build -b co_build && sh co_build/apply_patches.sh $manifest_file $buildDir && bash build/prebuilts_download.sh`

关联仓代码路径：`.repo,.gn,prebuilts,applications,build,developtools,driviers,kernel,arkcompiler,build.py,foundation,napi_generator,test,base,build.sh,commonlibrary,docs,interface,productdefine,third_party,vendor,device,qemu-run,ide`