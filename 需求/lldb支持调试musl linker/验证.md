1. 下载[demo](https://gitee.com/xwx1135370/oh-ndk-demo)
2. 导入环境变量
   Linux: `export OH_NDK_HOME=$HOME/Downloads/ohos-sdk/current/ohos-sdk/linux/native`
   Windows(受脚本配置限制，所以要将'\'，改为'/'):`export OH_NDK_HOME=D:/SDK/openharmony/9/native`
3. aarch64设备debug版本应用编译：`./build_oh_arm64v8a.sh -DCMAKE_BUILD_TYPE=Debug`
   arm设备debug版本应用编译：`./build_oh_armv7a.sh -DCMAKE_BUILD_TYPE=Debug`
   ![输入图片说明](https://foruda.gitee.com/images/1702367946437745298/fe1e8411_10384282.png "屏幕截图")
4.在“xx/oh-ndk-demo/out/oh-arm/libc_test”目录下获取编译生成件。如`memcpy_test`
5. 将编译出来的可执行文件和llvm中的lldb-server上传到设备上。修改权限，并运行lldb-server
![输入图片说明](https://foruda.gitee.com/images/1702004599341795403/18441317_10384282.png "屏幕截图")
6. 运行lldb，连接远端，进行调试
![输入图片说明](https://foruda.gitee.com/images/1702004683364085729/1cdd8b02_10384282.png "屏幕截图")




