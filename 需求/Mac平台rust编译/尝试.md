在xxx/commonlibrary/c_utils/bundle.json中将xxx/commonlibrary/c_utils/BUILD.gn中定义的ohos_rust_shared_library("utils_rs")添加进去（目的：使得编译时能找到这个target name（utils_rs））
![输入图片说明](https://foruda.gitee.com/images/1704447086041291023/88f09398_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1704447151060007612/723487f1_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1704675770991229400/70981152_10384282.png "屏幕截图")

- 编译：./build.sh --product-name rk3568 --build-target commonlibrary/c_utils/base:utils_rs
![输入图片说明](https://foruda.gitee.com/images/1704448402503269005/541625ab_10384282.png "屏幕截图")
编译失败 [日志](https://gitee.com/xwx1135370/llvm-related-issues/blob/master/rust/build_utils_rs.log)
![输入图片说明](https://foruda.gitee.com/images/1704448761443638952/9e49fa8c_10384282.png "屏幕截图")


- 编译：`./build.sh --product-name rk3568 --build-target commonlibrary/c_utils/base:utils_rs --no-prebuilt-sdk`
  Mac上原则上目前只支持OpenHarmony sdk编译，如果指定product-name为rk3568，尝试加`--no-prebuilt-sdk`看是否能编译通过
![输入图片说明](https://foruda.gitee.com/images/1704448809073452694/7e67723e_10384282.png "屏幕截图")
编译失败 [日志](https://gitee.com/xwx1135370/llvm-related-issues/blob/master/rust/build_utils_rs_no_sdk.log)
![输入图片说明](https://foruda.gitee.com/images/1704449260393085779/76f7d662_10384282.png "屏幕截图")

- 编译 ./build.sh --product-name rk3568 --build-taget commonlibrary/c_utils/base:utils_rs --no-prebuilt-sdk --gn-args full_mini_debug=false --keep-ninja-going --gn-args is_use_check_deps=false | tee utils_rs_no_sdk.log
 Mac上原则上目前只支持OpenHarmony sdk编译，参考sdk编译参数，添加了`--gn-args full_mini_debug=false --keep-ninja-going --gn-args is_use_check_deps=false`
![输入图片说明](https://foruda.gitee.com/images/1704449461668397154/cc6cf35c_10384282.png "屏幕截图")
编译失败 [日志](https://gitee.com/xwx1135370/llvm-related-issues/blob/master/rust/utils_rs_no_sdk.log)
![输入图片说明](https://foruda.gitee.com/images/1704449856978788006/ef9b0b8f_10384282.png "屏幕截图")


在xxx/commonlibrary/c_utils/bundle.json中将xxx/commonlibrary/c_utils/BUILD.gn中定义的ohos_rust_shared_library("utils_rs")添加进去;修改xxx/build/config/BUILDCONFIG.gn
（根据上个编译报错信息看，是不支持Mac上编译ohos类型的so编译的，所以修改is_mac = true进行尝试）
![输入图片说明](https://foruda.gitee.com/images/1704449932337275791/6d93d7de_10384282.png "屏幕截图")

编译:./build.sh --product-name rk3568 --build-taget commonlibrary/c_utils/base:utils_rs --no-prebuilt-sdk --gn-args full_mini_debug=false --keep-ninja-going --gn-args is_use_check_deps=false | tee utils_rs_no_sdk_mac.log
编译失败 [日志](https://gitee.com/xwx1135370/llvm-related-issues/blob/master/rust/utils_rs_no_sdk_mac.log)
![输入图片说明](https://foruda.gitee.com/images/1704451013352192664/4cd31ff0_10384282.png "屏幕截图")

