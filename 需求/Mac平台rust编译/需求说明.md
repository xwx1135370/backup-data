### 背景
   需求来源：李锦程
   之前刘家良他们搞rust的时候，发现Mac上不支持rust编译，会失败，而.bundle.json里要暴露一个rust的innerkit出去，于是他就用一个group封装了一下，里面用host_os编译控制了下，mac平台不编译。现在OH那边给了个整改需求，innerkit里暴露出去的不能是grop了，应该只能直接暴露ohos_rust_shared_library,但是不确定现在Mac上支持rust的编译不，我看有些库上的其他模块是直接ohos_rust_shared_library当innerkit暴露出去了，应该是多平台已经支持了。
![输入图片说明](https://foruda.gitee.com/images/1704446568261879410/56fc98ba_10384282.png "屏幕截图")
  之前暴露的是utils_rust，Mac上是空的。

   代码仓：xxx/commonlibrary/c_utils/base/BUILD.gn

### 处理说明
    1. bundle.json里要暴露一个rust的innerkit出去，让外部可以依赖
    2. 涉及多平台，之前Mac上没法编译ohos_rust_shared_library，所以没法直接以ohos_rust_shared_library暴露出去，要不mac上就直接编译了，编译会报错。
    3. 当时的处理方式暴露一个grop，grop里用了host_os来控制，Mac上名义上暴露了utils_rust，实际上里面啥都没
    4. 现在Mac上不确定是不是支持rust了，也就是不是可以group这个方式了