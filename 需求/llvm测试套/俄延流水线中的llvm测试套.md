1 使用hos-build任务跑出工具链包

2 使用hos_aarach64-linux-ohos_linux_lldb-api跑lldb测试套

- docker image： nexus.devops-spb.rnd.huawei.com/huawei/llvm-test:v1.55

- 下载 https://rnd-gitlab-msc.huawei.com/rus-os-team/compilers/aosp/platform/manifest

- 下载 https://nexus.devops-spb.rnd.huawei.com/repository/pypi/packages/ci-utils/0.0.14/CI_Utils-0.0.14-py3-none-any.whl

- 下载 https://nexus.devops-spb.rnd.huawei.com/repository/pypi/packages/ci-utils/0.0.14/CI_Utils-0.0.14-py3-none-any.whl

- 下载 https://files.pythonhosted.org/packages/ec/f4/e3885b301f8871a53f00ed0d5326901f1d810e1dc17230bc09b4f3d4adaf/simplejson-3.18.1-cp38-cp38-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_17_x86_64.manylinux2014_x86_64.whl 
- 下载 git clone https://rnd-gitlab-msc.huawei.com/rus-os-team/compilers/testing/automation/llvm-ci.git -b master --depth=1
- 下载 git clone https://gitee.com/openharmony-sig/third_party_llvm-project.git -b master --depth=1
- 下载hos-build任务生成的工具链包，如
```
  Downloading "clang-749157-linux-x86_64.tar.bz2" with "https://nexus.devops-spb.rnd.huawei.com/repository/compilers-team/llvm-builds/hos-bz/daily/2023-02-03/749157/clang-749157-linux-x86_64.tar.bz2"
__INFO__: Downloading "ohos-sysroot-749157.tar.bz2" with "https://nexus.devops-spb.rnd.huawei.com/repository/compilers-team/llvm-builds/hos-bz/daily/2023-02-03/749157/ohos-sysroot-749157.tar.bz2"
```
- 解压包
- /builds/rus-os-team/compilers/aosp/platform/manifest/root/third_party_llvm-project /builds/rus-os-team/compilers/aosp/platform/manifest/root
- 映射端口 adb forward tcp:1234 tcp:1234
- 清空历史adb shell 'rm -rf /data/local/tmp/lldb_api_test_aarch64_ohos && mkdir /data/local/tmp/lldb_api_test_aarch64_ohos'
- 推送lldb-server到设备 adb push /builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/../lib/clang/12.0.1/bin/aarch64-linux-ohos/lldb-server /data/local/tmp/lldb_api_test_aarch64_ohos
- adb shell 'rm -rf /data/local/tmp/lldb_api_test_aarch64_ohos && mkdir /data/local/tmp/lldb_api_test_aarch64_ohos'
- adb_lldb_server_pid=4186
- 拉起设备上的lldb-server：adb shell /data/local/tmp/lldb_api_test_aarch64_ohos/lldb-server p --listen '*:1234' --server
- LLDB_TEST_USER_ARGS+='--excluded;/builds/rus-os-team/compilers/aosp/platform/manifest/root/llvm-ci/resources/disabled_tests/lldb-api-aarch64-linux-ohos.txt;'
- LLDB_TEST_USER_ARGS+='--platform-name;remote-android;'
- LLDB_TEST_USER_ARGS+='--platform-url;connect://:1234;'
- LLDB_TEST_USER_ARGS+='--platform-working-dir;/data/local/tmp/lldb_api_test_aarch64_ohos;'
- LLDB_TEST_USER_ARGS+='-E;--config /builds/rus-os-team/compilers/aosp/platform/manifest/root/aarch64-linux-ohos.cfg;'
- 结果写入目录： /builds/rus-os-team/compilers/aosp/platform/manifest/root
lit --xunit-xml-output /builds/rus-os-team/compilers/aosp/platform/manifest/root/test-results.xunit.xml --show-all -v test/API --timeout=600

## 样例1：
UNSUPPORTED: lldb-api :: android/platform/TestDefaultCacheLineSize.py 
Script:
--
/usr/bin/python3.8 /builds/rus-os-team/compilers/aosp/platform/manifest/root/third_party_llvm-project/lldb/test/API/dotest.py -u CXXFLAGS -u CFLAGS --env ARCHIVER=/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/llvm-ar --env OBJCOPY=/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/llvm-objcopy -v --env ARFLAGS=rvU --excluded /builds/rus-os-team/compilers/aosp/platform/manifest/root/llvm-ci/resources/disabled_tests/lldb-api-aarch64-linux-ohos.txt --platform-name remote-android --platform-url connect://:1234 --platform-working-dir /data/local/tmp/lldb_api_test_aarch64_ohos -E --config /builds/rus-os-team/compilers/aosp/platform/manifest/root/aarch64-linux-ohos.cfg --env LLVM_LIBS_DIR=/builds/rus-os-team/compilers/aosp/platform/manifest/root/third_party_llvm-project/./lib --arch aarch64 --build-dir /builds/rus-os-team/compilers/aosp/platform/manifest/root/lldb-test-build.noindex --lldb-module-cache-dir /builds/rus-os-team/compilers/aosp/platform/manifest/root/lldb-test-build.noindex/module-cache-lldb/lldb-api --clang-module-cache-dir /builds/rus-os-team/compilers/aosp/platform/manifest/root/lldb-test-build.noindex/module-cache-clang/lldb-api --executable /builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/lldb --compiler /builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang --dsymutil /builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/dsymutil --filecheck /builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/FileCheck --yaml2obj /builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/yaml2obj --server /data/local/tmp/lldb_api_test_aarch64_ohos/lldb-server --lldb-libs-dir /builds/rus-os-team/compilers/aosp/platform/manifest/root/./lib /builds/rus-os-team/compilers/aosp/platform/manifest/root/third_party_llvm-project/lldb/test/API/android/platform -p TestDefaultCacheLineSize.py
--
Exit Code: 0

Command Output (stdout):
--
lldb version 12.0.1 (/data/749157-merged/toolchain/llvm-project/lldb revision d6f4a5c4843c9186b3d8a687131edfeaaa7347e1)
  clang revision d6f4a5c4843c9186b3d8a687131edfeaaa7347e1
  llvm revision d6f4a5c4843c9186b3d8a687131edfeaaa7347e1
Setting up remote platform 'remote-android'
Connecting to remote platform 'remote-android' at 'connect://:1234'...
Connected.
Setting remote platform working directory to '/data/local/tmp/lldb_api_test_aarch64_ohos'...
Skipping following debug info categories: ['dsym', 'gmodules']
objc tests will be skipped because of unsupported platform
compiler=/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang

--
Command Output (stderr):
--

Configuration: arch=aarch64 compiler=/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang
----------------------------------------------------------------------
Collected 4 tests

1: test_cache_line_size_dsym (TestDefaultCacheLineSize.DefaultCacheLineSizeTestCase) ... skipped 'test case does not fall in any category of interest for this run'
UNSUPPORTED: LLDB (/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang-aarch64) :: test_cache_line_size_dsym (TestDefaultCacheLineSize.DefaultCacheLineSizeTestCase) (test case does not fall in any category of interest for this run) 
2: test_cache_line_size_dwarf (TestDefaultCacheLineSize.DefaultCacheLineSizeTestCase) ... skipped 'requires target to be Android'
UNSUPPORTED: LLDB (/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang-aarch64) :: test_cache_line_size_dwarf (TestDefaultCacheLineSize.DefaultCacheLineSizeTestCase) (requires target to be Android) 
3: test_cache_line_size_dwo (TestDefaultCacheLineSize.DefaultCacheLineSizeTestCase) ... skipped 'requires target to be Android'
UNSUPPORTED: LLDB (/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang-aarch64) :: test_cache_line_size_dwo (TestDefaultCacheLineSize.DefaultCacheLineSizeTestCase) (requires target to be Android) 
4: test_cache_line_size_gmodules (TestDefaultCacheLineSize.DefaultCacheLineSizeTestCase) ... skipped 'test case does not fall in any category of interest for this run'
UNSUPPORTED: LLDB (/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang-aarch64) :: test_cache_line_size_gmodules (TestDefaultCacheLineSize.DefaultCacheLineSizeTestCase) (test case does not fall in any category of interest for this run) 

----------------------------------------------------------------------
Ran 4 tests in 0.003s

RESULT: PASSED (0 passes, 0 failures, 0 errors, 4 skipped, 0 expected failures, 0 unexpected successes)
Session logs for test failures/errors/unexpected successes can be found in the test build directory

--


## 样例2
UNSUPPORTED: lldb-api :: commands/expression/call-throws/TestCallThatThrows.py (2 of 916)
Script:
--
/usr/bin/python3.8 /builds/rus-os-team/compilers/aosp/platform/manifest/root/third_party_llvm-project/lldb/test/API/dotest.py -u CXXFLAGS -u CFLAGS --env ARCHIVER=/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/llvm-ar --env OBJCOPY=/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/llvm-objcopy -v --env ARFLAGS=rvU --excluded /builds/rus-os-team/compilers/aosp/platform/manifest/root/llvm-ci/resources/disabled_tests/lldb-api-aarch64-linux-ohos.txt --platform-name remote-android --platform-url connect://:1234 --platform-working-dir /data/local/tmp/lldb_api_test_aarch64_ohos -E --config /builds/rus-os-team/compilers/aosp/platform/manifest/root/aarch64-linux-ohos.cfg --env LLVM_LIBS_DIR=/builds/rus-os-team/compilers/aosp/platform/manifest/root/third_party_llvm-project/./lib --arch aarch64 --build-dir /builds/rus-os-team/compilers/aosp/platform/manifest/root/lldb-test-build.noindex --lldb-module-cache-dir /builds/rus-os-team/compilers/aosp/platform/manifest/root/lldb-test-build.noindex/module-cache-lldb/lldb-api --clang-module-cache-dir /builds/rus-os-team/compilers/aosp/platform/manifest/root/lldb-test-build.noindex/module-cache-clang/lldb-api --executable /builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/lldb --compiler /builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang --dsymutil /builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/dsymutil --filecheck /builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/FileCheck --yaml2obj /builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/yaml2obj --server /data/local/tmp/lldb_api_test_aarch64_ohos/lldb-server --lldb-libs-dir /builds/rus-os-team/compilers/aosp/platform/manifest/root/./lib /builds/rus-os-team/compilers/aosp/platform/manifest/root/third_party_llvm-project/lldb/test/API/commands/expression/call-throws -p TestCallThatThrows.py
--
Exit Code: 0

Command Output (stdout):
--
lldb version 12.0.1 (/data/749157-merged/toolchain/llvm-project/lldb revision d6f4a5c4843c9186b3d8a687131edfeaaa7347e1)
  clang revision d6f4a5c4843c9186b3d8a687131edfeaaa7347e1
  llvm revision d6f4a5c4843c9186b3d8a687131edfeaaa7347e1
Setting up remote platform 'remote-android'
Connecting to remote platform 'remote-android' at 'connect://:1234'...
Connected.
Setting remote platform working directory to '/data/local/tmp/lldb_api_test_aarch64_ohos'...
Skipping following debug info categories: ['dsym', 'gmodules']
objc tests will be skipped because of unsupported platform
compiler=/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang

--
Command Output (stderr):
--

Configuration: arch=aarch64 compiler=/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang
----------------------------------------------------------------------
Collected 4 tests

1: test_dsym (TestCallThatThrows.ExprCommandWithThrowTestCase)
   Test calling a function that throws and ObjC exception. ... skipped 'test case does not fall in any category of interest for this run'
UNSUPPORTED: LLDB (/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang-aarch64) :: test_dsym (TestCallThatThrows.ExprCommandWithThrowTestCase) (test case does not fall in any category of interest for this run) 
2: test_dwarf (TestCallThatThrows.ExprCommandWithThrowTestCase)
   Test calling a function that throws and ObjC exception. ... skipped 'test case does not fall in any category of interest for this run'
UNSUPPORTED: LLDB (/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang-aarch64) :: test_dwarf (TestCallThatThrows.ExprCommandWithThrowTestCase) (test case does not fall in any category of interest for this run) 
3: test_dwo (TestCallThatThrows.ExprCommandWithThrowTestCase)
   Test calling a function that throws and ObjC exception. ... skipped 'test case does not fall in any category of interest for this run'
UNSUPPORTED: LLDB (/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang-aarch64) :: test_dwo (TestCallThatThrows.ExprCommandWithThrowTestCase) (test case does not fall in any category of interest for this run) 
4: test_gmodules (TestCallThatThrows.ExprCommandWithThrowTestCase)
   Test calling a function that throws and ObjC exception. ... skipped 'test case does not fall in any category of interest for this run'
UNSUPPORTED: LLDB (/builds/rus-os-team/compilers/aosp/platform/manifest/root/clang-749157/bin/clang-aarch64) :: test_gmodules (TestCallThatThrows.ExprCommandWithThrowTestCase) (test case does not fall in any category of interest for this run) 

----------------------------------------------------------------------
Ran 4 tests in 0.003s

RESULT: PASSED (0 passes, 0 failures, 0 errors, 4 skipped, 0 expected failures, 0 unexpected successes)
Session logs for test failures/errors/unexpected successes can be found in the test build directory



