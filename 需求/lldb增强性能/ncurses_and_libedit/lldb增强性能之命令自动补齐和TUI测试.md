**【SR】**： SR000HP004

**【说明】**
【需求来源】：手机工程机（含HMOS系统应用）

【需求场景价值】：当前系统开发者在系统调试时，使用lldb时环境配置复杂。且命令自动补齐，TUI等功能缺失，导致开发者使用网上下载得gdb调试，经常出现版本匹配问题。需要改善。

【需求描述】：

lldb调试支持自动命令行补齐、TUI调试、shell脚本配置调试功能

输入：lldb调试
处理：
1. 通过tab键触发命令自动补齐
2. 进入TUI模式
3. 开发者配置shell脚本并在调试过程调用

输出：
1. 自动补齐联想命令
2. 可以在TUI模式中可视化调试
3. 可以在调试过程中正确执行shell脚本中的代码逻辑

【功能描述】lldb调试功能扩展
【功能列表】：

【验收标准】：
1. 在输入单个或多个字符后，按tab键可自动补齐命令，如pl自动联想出platform
2. 在TUI模式中，可以同时在上下放显示代码和汇编命令，文本代码行与汇编代码行正确匹配
3. 在shell（实则python）脚本中配置环境变量与命令，在lldb中执行shell（实则python）脚本可以正确执行该批命令
【开发语言】：C++
【验收平台】：Phone/RK3568等
【外部依赖】：NA
【性能功耗指标】：NA

【交付日期】6月7号

**【补充信息】**
1. libedit支持自动补齐
火车头版本https://ostms.rnd.huawei.com/osmanageweb/#/software-detail?versionCode=0583044480&domain=1&releaseTrain=CleanSource%20V5.1
实体包下载地址：https://www.thrysoee.dk/editline/libedit-20210910-3.1.tar.gz
文件名称： libedit-20210910-3.1.tar.gz
疑问1：源码是否要放在llvm仓？看openharmony中其他的三方库都是创建了类似third_party_xxx的组件，是否要申请新的组件仓？申请流程是？
疑问2：libedit.so和libncurese放置的路径？要放在llvm工具链里面吗？
疑问3：库依赖方式（动态？静态）
疑问3：编译是否要加宏开关？
疑问4：平台支持，windows？linux？mac？

预计版本：https://ostms.rnd.huawei.com/osmanageweb/#/software-detail?versionCode=0583048635&domain=1&releaseTrain=CleanSource%20V5.0&keyword=libedit
官网地址：https://www.openeuler.org/
社区托管地址：https://gitee.com/src-openeuler/libedit.git
源码下载地址： https://repo.openeuler.org/openEuler-22.03-LTS/source/Packages/libedit-3.1-28.oe2203.src.rpm
https://repo.openeuler.org/openEuler-22.03-LTS-SP1/source/Packages/libedit-3.1-29.oe2203sp1.src.rpm

终版源码地址：https://www.thrysoee.dk/editline/libedit-20221030-3.1.tar.gz

火车头版本：https://repo.openeuler.org/openEuler-22.03-LTS-SP1/source/Packages/libedit-3.1-29.oe2203sp1.src.rpm 3.1-29.oe2203sp1

https://repo.openeuler.org/openEuler-22.03-LTS-SP1/source/Packages/libedit-3.1-29.oe2203sp1.src.rpm


2. libncurses支持TUI
https://gitee.com/openharmony/third_party_llvm-project/issues/I69MXD?from=project-issue
火车头版本：https://ostms.rnd.huawei.com/osmanageweb/#/software-detail?versionCode=0583050686&domain=1&releaseTrain=CleanSource%20V5.1
https://repo.openeuler.org/openEuler-22.03-LTS/source/Packages/ncurses-6.3-2.oe2203.src.rpm

终版源码地址：https://gitee.com/src-openeuler/ncurses.git


火车头版本： https://repo.openeuler.org/openEuler-22.03-LTS-SP1/source/Packages/ncurses-6.3-5.oe2203sp1.src.rpm 6.3-5.oe2203sp1

https://repo.openeuler.org/openEuler-22.03-LTS-SP1/source/Packages/ncurses-6.3-5.oe2203sp1.src.rpm 
```
./configure --with-shared;make
```


