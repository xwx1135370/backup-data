#!/usr/bin/env sh
current_dir=$(cd $(dirname $0) && pwd)
source_dir=$current_dir/ncurses-6.3
install_dir=$current_dir/install
build_dir=$current_dir/build

# Do a clean build
rm -rf $build_dir && rm -rf $install_dir && \
    mkdir -p $install_dir && mkdir -p $build_dir && \
    cd $build_dir

lld_ldflags="-fuse-ld=lld"
pic_cflags="-fPIC"
mac_sdk_root=$(xcrun --show-sdk-path)

CC=/System/Volumes/Data/data/llvm/builds/HsxB3o3v/0/rus-os-team/compilers/aosp/platform/manifest/prebuilts/clang/ohos/darwin-x86_64/clang-15.0.4/bin/clang
CXX=/System/Volumes/Data/data/llvm/builds/HsxB3o3v/0/rus-os-team/compilers/aosp/platform/manifest/prebuilts/clang/ohos/darwin-x86_64/clang-15.0.4/bin/clang++

export CC=${CC=clang}
export CXX=${CXX=clang++}
# export LDFLAGS="$LDFLAGS $lld_ldflags"
# echo $LDFLAGS
export CFLAGS="$CFLAGS $pic_cflags -isysroot$mac_sdk_root -Wl,-syslibroot,$mac_sdk_root $lld_ldflags"
export CXXFLAGS="$CXXFLAGS $CFLAGS"
export CPPFLAGS="-isysroot$mac_sdk_root"
export CXXPPFLAGS="$CPPFLAGS"
echo CC=$CC
echo CXX=$CXX
echo CFLAGS=$CFLAGS
echo CXXFLAGS=$CXXFLAGS

$source_dir/configure \
    --prefix=$install_dir \
    --with-shared \
    --with-terminfo-dirs=/usr/lib/terminfo:/lib/terminfo:/usr/share/terminfo

make -j$(nproc --all) install | tee build.log
