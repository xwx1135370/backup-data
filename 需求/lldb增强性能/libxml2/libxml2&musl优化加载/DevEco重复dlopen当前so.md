PR：
- https://gitee.com/openharmony/third_party_llvm-project/pulls/296
- https://gitee.com/openharmony/third_party_musl/pulls/1118


CMakeList.txt
```
# the minimum version of CMake.
cmake_minimum_required(VERSION 3.4.1)
project(OhNative10)

set(NATIVERENDER_ROOT_PATH ${CMAKE_CURRENT_SOURCE_DIR})

include_directories(${NATIVERENDER_ROOT_PATH}
                    ${NATIVERENDER_ROOT_PATH}/include)

add_library(entry SHARED hello.cpp)
target_link_libraries(entry
    PUBLIC libace_napi.z.so
    PRIVATE libhilog_ndk.z.so)
```

hello.cpp
```
#include "napi/native_api.h"

#include <array>
#include <chrono>
#include <cinttypes>
#include <cstdint>
#include <deque>
#include <dlfcn.h>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <queue>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#define LOG_TAG "com.example.ohnative10"
#include "hilog/log.h"

class Student {
public:
    explicit Student(const char *name, int id) : name(name), id(id) {}
    const std::string &getName() { return name; }
    int getId() { return id; }

private:
    std::string name;
    int id;
};

static void throw_exception() {
    throw std::runtime_error("a runtime error occurred");
}

static void test_try_catch() {
    try {
        throw_exception();
    } catch (const std::runtime_error &err) {
        std::cout << err.what() << std::endl;
    }
}

class Student;

static void test_cpp_stl_container() {
    std::queue<int> queue {};
    queue.push(1);
    queue.push(2);
    queue.push(3);
    queue.push(5);
    
    std::list<int> list { 4, 5 };
    list.push_back(6);
    list.push_front(3);
    list.push_front(2);
    list.push_front(1);
    
    std::deque<std::string> pendingUsers {};
    pendingUsers.emplace_front("user1");
    pendingUsers.emplace_back("user2");
    
    std::vector<unsigned long> vec { 1UL, 2UL };
    vec.push_back(3UL);
    vec.push_back(4UL);
    
    std::set<char> charSet {};
    charSet.insert('a');
    charSet.insert('b');
    
    std::unordered_set<char> charHashSet {};
    charHashSet.insert('a');
    charHashSet.insert('c');
    
    std::map<int, std::string> int2String {};
    int2String.emplace(1, "str1");
    int2String.emplace(2, "str2");
    
    std::unordered_map<int, Student> id2Student {};
    id2Student.emplace(1, Student("A", 1));
    id2Student.emplace(2, Student("B", 2));
    
    std::array<std::unordered_map<std::string, std::string>, 3> pathMappings {{
        {{ "patha", "PATHA"}},
        {{ "pathb", "PATHB"}},
        {{ "pathc", "PATHC"}},
    }};
}

static void test_class() {
    Student student1("A", 1);
    auto name = student1.getName();
    std::unique_ptr<Student> student2 = std::make_unique<Student>("B", 2);
}

template <typename IntType>
static IntType integer_mul(IntType a, IntType b) {
    return a * b;
}

template <typename IntType>
static IntType integer_add(IntType a, IntType b) {
    return a + b;
}

template <typename IntType>
static IntType integer_sub(IntType a, IntType b) {
    return a - b;
}

template <typename IntType>
static IntType integer_div(IntType a, IntType b) {
    return a / b;
}

static void test_integer() {
    auto res1 = integer_add<int>(1, 2);
    auto res2 = integer_sub<int>(1, 2);
    auto res3 = integer_mul<long>(3, 6);
    auto res4 = integer_div<unsigned>(9, 3);
}

class Timer {
public:
    explicit Timer(const char *msg) : m_msg(msg) { initTimer(); }
    Timer(const Timer&) = delete;
    Timer(Timer &&) = delete;
    
    ~Timer() {
        m_end = std::chrono::steady_clock::now();
        auto timeDiff = std::chrono::duration_cast<std::chrono::milliseconds>(m_end - m_start);
        std::int64_t seconds = timeDiff.count();
        OH_LOG_INFO(LogType::LOG_APP, "%{public}s: it took %{public}" PRId64 " milliseconds" ,
            m_msg, seconds);
    }

private:
    void initTimer() {
        m_start = std::chrono::steady_clock::now();
    }

    const char *m_msg;
    std::chrono::steady_clock::time_point m_start;
    std::chrono::steady_clock::time_point m_end;
};

static void test_dlopen(std::size_t ct) {
    Dl_info currentSoInfo;
    if (!dladdr(reinterpret_cast<const void *>(test_dlopen), &currentSoInfo)) {
        OH_LOG_ERROR(LogType::LOG_APP, "failed to find out information of the current so: %{public}s",
            dlerror());
    }
    std::ostringstream msgStream;
    msgStream << "dlopen and dlclose " << ct << " times";
    std::string msg(msgStream.str());
    Timer timer(msg.c_str());
    for (std::size_t i = 0; i < ct; i++) {
        void *soHandle;
        if (!(soHandle = dlopen(currentSoInfo.dli_fname, RTLD_LAZY))) {
            OH_LOG_ERROR(LogType::LOG_APP, "failed to dlopen the current so: %{public}s",
                dlerror());
            break;
        }
        if (dlclose(soHandle)) {
            OH_LOG_ERROR(LogType::LOG_APP, "failed to dlclose the current so: %s{public}s",
                dlerror());
            break;
        }
    }
}

static napi_value Add(napi_env env, napi_callback_info info)
{
    size_t requireArgc = 2;
    size_t argc = 2;
    napi_value args[2] = {nullptr};

    napi_get_cb_info(env, info, &argc, args , nullptr, nullptr);

    napi_valuetype valuetype0;
    napi_typeof(env, args[0], &valuetype0);

    napi_valuetype valuetype1;
    napi_typeof(env, args[1], &valuetype1);

    double value0;
    napi_get_value_double(env, args[0], &value0);

    double value1;
    napi_get_value_double(env, args[1], &value1);

    napi_value sum;
    napi_create_double(env, value0 + value1, &sum);
    
    test_try_catch();
    test_class();
    test_integer();
    test_cpp_stl_container();
    OH_LOG_INFO(LogType::LOG_APP, "start test_dlopen");
    test_dlopen(20);

    return sum;
}

EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports)
{
    napi_property_descriptor desc[] = {
        { "add", nullptr, Add, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    return exports;
}
EXTERN_C_END

static napi_module demoModule = {
    .nm_version =1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = Init,
    .nm_modname = "entry",
    .nm_priv = ((void*)0),
    .reserved = { 0 },
};

extern "C" __attribute__((constructor)) void RegisterEntryModule(void)
{
    napi_module_register(&demoModule);
}

```
要启动native或者运行调试才会生效，不调试的话，lldb不会介入


log enable lldb dyld -T -f D:\lldb.log

![输入图片说明](https://foruda.gitee.com/images/1696751880326137351/9ec911ee_10384282.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1696751844835183159/b7750435_10384282.png "屏幕截图")