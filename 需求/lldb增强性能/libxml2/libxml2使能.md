### 信息收集

https://lldb.llvm.org/resources/build.html?highlight=libxml2

Could NOT find LibXml2 (missing: LIBXML2_LIBRARY LIBXML2_INCLUDE_DIR) (found version "2.9.13")

openharmony中编译libxml2：
```
release动态库: ./build.sh --product-name rk3568 --build-target xml2
debug静态库: ./build.sh --product-name rk3568 --build-target static_libxml2 --gn-args is_debug=true

```

lldb集成libxml2，我能想到的两种方法：

1. 像Android一样，编译llvm源代码之前编译libxml2。参考 Android llvm工具链集成 https://android.googlesource.com/toolchain/llvm_android/+/refs/heads/main/do_build.py#117

2. 先编译和归档到蓝区，再以prebuilt方式集成到lldb。oh 应该已经支持编译windows、linux、mac版本libxml2，可以参考https://gitee.com/openharmony/third_party_libxml2/blob/master/BUILD.gn

### Android 
![输入图片说明](https://foruda.gitee.com/images/1690785712479658985/ff30b0f9_10384282.png "屏幕截图")
./stage2-install/lib/libxml2.so.2 和./lib/libxml2-linux/libxml2.so.2和./install/linux-x86/clang-dev/lib/libxml2.so.2一致
./stage2-install/lib/libxml2.so.2: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, not stripped