1.	在manifest仓all.xml加入自己仓（这个步骤一般由马明帅完成），并且新建想要搭建流水线仓的xml文件
https://gitee.com/openharmony-sig/manifest?_from=gitee_search
all.xml用来跑format check，自建.xml用来跑编译门禁
2.	提交流水线申请（需要依赖sig仓申请单号），可以参照已有的流水线填写
![输入图片说明](https://foruda.gitee.com/images/1697082004924433821/949a3974_10384282.png "屏幕截图")
举例一个hap编译，编译其余形态需要改编译命令：

一般都需要联合构建，（联合构建是指依赖openharmony代码进行编译）
参考申请单号：
85156170825728000
往openharmony代码打patch说明：https://gitee.com/openharmony-sig/cicd/tree/co_build/
打patch需要选择 是
![输入图片说明](https://foruda.gitee.com/images/1697082043475160049/0078ccfb_10384282.png "屏幕截图")
申请参数字段说明：https://gitee.com/openharmony-sig/cicd/blob/master/%E5%BC%80%E5%8F%91%E6%9D%BF%E8%BF%9B%E5%9C%BA/08%20%E5%B7%B2%E5%AD%B5%E5%8C%96%E5%8E%82%E5%95%86%E7%BC%96%E8%AF%91%E9%97%A8%E7%A6%81%E7%94%B3%E8%AF%B7%E6%8C%87%E5%AF%BCV1.0.md
![输入图片说明](https://foruda.gitee.com/images/1697082060668456261/88dd4ff0_10384282.png "屏幕截图")
看是否打patch，打patch需要选联合
![输入图片说明](https://foruda.gitee.com/images/1697082073999860515/73e6395f_10384282.png "屏幕截图")
看sig仓的名字对不对
![输入图片说明](https://foruda.gitee.com/images/1697082089388860793/04637c6d_10384282.png "屏幕截图")
看这个有没有少东西（一般都是一样的，都是这么多）