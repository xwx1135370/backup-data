镜像包
ABR：\\10.247.4.174\dev_rom\ABRQ_S （P50）
JAD高通：\\10.247.4.174\dev_rom\JADQ_S (P50 pro常用)
JAD海思：\\10.247.4.174\dev_rom\JAD_S (P50 pro)
NOH：\\10.247.4.174\dev_rom\NOH_S （mate40 pro）


这个除了jdwp的命令变成了下面的外，其他的需要你们去看看你们的场景对于半容器需要怎么适配，server还是以前的harmonyOs的server
 **ohjpid-control 取容器内pid** 
 **jdwp-control取容器外的pid** 

http://3ms.huawei.com/km/blogs/details/12796079


```
hdc shell bm install -p /sdcard/2048_oh/2048_oh.hap --文件名要全的吧 
```

```
hdc file send E:\code_temp\HalfContainer\mall.hap /sdcard/mall/mall.hap
hdc shell bm install -p /sdcard/mall/
hdc shell aa start -p "com.example.myapplication3" -n "com.example.myapplication3.MainAbility" -a action 
```

1. 获取pid
容器内：ohjpid-control 取容器内pid
容器外：jdwp-control取容器外的pid

2.容器内外路径映射关系
http://3ms.huawei.com/km/blogs/details/12796079
| 容器内 | 容器外 |
|-----|-----|
|   system  |/system/ohos/system |
|  /data   | /data/ohos_data    |
| /vendor    |/system/ohos/vendor     |
| /dev/socket    |/dev/socket     |

3. 当前半容器遵循“不使用、无消耗”原则，即用户不安装 hap 应用时，容器默认不激活运行，无任何性能损耗。为了激活容器，我们需要安装至少一个 hap 应用，可执行如下命令：
```
adb shell "bm install /system/ohos/vendor/test/entry.hap"
```
这里安装的 hap 应用也是版本预置的，无需额外下载/push 操作。
4. 进入容器shell
首先执行 adb shell 进入容器外 shell 环境，然后执行如下命令：
```
ohsh
```

判断容器是否启动
```
getprop persist.sys.enable_oh_container
hdc get-container-state
```

判断设备版本(9的半容器镜像可以跑api8及以下 release的应用，这个时候是跑在容器外，如果应用的apiVerison是9的，且设备是9，那一定是跑在容器内，你可以用这个条件判断)
```
hdc shell
HWJAD-Q:/ # getprop hw_sc.build.os.apiversion
8
```
判断应用的apiVersion
```

```


你root完，安装一个就有了
我发命令行给你

hdc.exe file send D:\project\entry-default-signed.hap /sdcard/
bm install /sdcard/entry-default-signed.hap
ohsh aa start -b com.example.myapplication -a MainAbility

mall.hap (4.59MB)
打开文件打开文件夹重新下载

你安装下这个hap

- 如果安装hap包时，提示“Failure[INSTALL_FAILED_INTERNAL_ERROR]”
![输入图片说明](https://foruda.gitee.com/images/1663549278175414654/bca52150_10384282.png "屏幕截图")
-->尝试ohsh进去容器执行toybox rm -rf /data/*    然后reboot下。可能是之前安装过其他应用，有的卸载不干净，签名不一致的问题。还是不行的话，na......


API9编译OHOS的可执行文件，上传到设备的/data/ohos_data目录
- 容器外还是hos应用
- 容器内是ohos应用
```
hdc file send F:\vt_code\ /data/ohos_data

hdc shell
#ohsh 进入容器
#cd /data/vt_code/build 进入容器目录（容器内外有映射关系）
#toybox chmod +x VT_Porject_64
./VT_Porject_64 （OHOS应用）
......（运行成功）
#
```

容器内查看pid：
```
# toybox ps -A
Can't connect to hilog server. Errno: 2
Can't connect to hilog server. Errno: 2
  PID TTY          TIME CMD
    1 ?        00:00:00 init
    3 ?        00:00:00 param_watcher
    4 ?        00:00:00 appspawn
    5 ?        00:00:00 installs
    6 ?        00:00:15 hiview
    7 ?        00:00:05 hidumper_servic
    8 ?        00:00:00 foundation
    9 ?        00:00:00 faultloggerd
   10 ?        00:00:00 distributeddata
   11 ?        00:00:00 accountmgr
   12 ?        00:00:09 accesstoken_ser
  333 ?        00:00:01 com.example.mya
  401 pts/0    00:00:00 sh
  406 pts/1    00:00:00 sh
  437 pts/0    00:00:00 INPUT
  438 pts/1    00:00:00 toybox
```
或
```
# toybox ps -ef
Can't connect to hilog server. Errno: 2
Can't connect to hilog server. Errno: 2
UID            PID  PPID C STIME TTY          TIME CMD
root             1     0 0 51:21 ?        00:00:00 oh_init
paramwatcher     3     1 0 51:21 ?        00:00:00 param_watcher
root             4     1 0 51:21 ?        00:00:00 appspawn
installs         5     1 0 51:21 ?        00:00:00 installs
hiview           6     1 0 51:21 ?        00:00:37 hiview
hidumper_se+     7     1 0 51:21 ?        00:00:12 hidumper_servic
foundation       8     1 0 51:21 ?        00:00:01 foundation
faultloggerd     9     1 0 51:21 ?        00:00:00 faultloggerd
ddms            10     1 0 51:20 ?        00:00:01 distributeddata
account         11     1 0 51:20 ?        00:00:00 accountmgr
access_token    12     1 0 51:20 ?        00:00:23 accesstoken_ser
u0_a3          333     4 0 36:11 ?        00:00:04 com.example.myapplication
root           406     0 0 46:33 pts/1    00:00:00 ohsh
root           580   406 0 11:20 pts/1    00:00:00 lldb-server p --server --listen *:8080
root           590   580 0 14:53 pts/1    00:00:00 lldb-server p --server --listen *:8080
root           611     0 0 27:35 pts/0    00:00:00 ohsh
root           618   611 4 31:34 pts/0    00:00:00 toybox ps -ef
#
```

或
```
# toybox ps -lefZ
LABEL                          UID            PID  PPID C STIME TTY          TIME CMD
u:r:ohco_init:s0               root             1     0 0 14:40 ?        00:00:00 oh_init
u:r:ohco_param_watcher:s0      paramwatcher     3     1 0 14:40 ?        00:00:00 param_watcher
u:r:ohco_appspawn:s0           root             4     1 0 14:40 ?        00:00:00 appspawn
u:r:ohco_quick_fix:s0          quickfixser+     5     1 0 14:40 ?        00:00:00 quick_fix
u:r:ohco_installs:s0           installs         6     1 0 14:40 ?        00:00:00 installs
u:r:ohco_hiview:s0             hiview           7     1 0 14:40 ?        00:00:24 hiview
u:r:ohco_hidumper_service:s0   hidumper_se+     8     1 0 14:40 ?        00:00:06 hidumper_servic
u:r:ohco_foundation:s0         foundation       9     1 0 14:40 ?        00:00:00 foundation
u:r:ohco_faultloggerd:s0       faultloggerd    10     1 0 14:40 ?        00:00:00 faultloggerd
u:r:ohco_distributeddata:s0    ddms            11     1 0 14:40 ?        00:00:00 distributeddata
u:r:ohco_accountmgr:s0         account         12     1 0 14:40 ?        00:00:00 accountmgr
u:r:ohco_accesstoken_ser:s0    access_token    13     1 0 14:40 ?        00:00:13 accesstoken_ser
u:r:su:s0                      root           363     0 0 00:45 pts/0    00:00:00 ohsh
u:r:ohco_appspawn:s0           20040003       387     4 0 16:44 ?        00:00:00 com.myapplication
u:r:su:s0                      root           430     0 0 16:46 136:1    00:00:00 sh /data/local/tmp/com.myapplication/lldb/bin/start_lldb_server.sh /data/local/tmp/com.myapplication/ll+
u:r:su:s0                      root           436   430 0 16:46 136:1    00:00:00 lldb-server platform --listen unix-abstract:///com.myapplication-0/platform-1673264598426.sock --log-fi+
u:r:su:s0                      root           437   436 0 16:47 136:1    00:00:01 lldb-server gdbserver unix-abstract:///data/local/tmp/com.myapplication/lldb/tmp/lldb/436/gdbserver.11a+
u:r:su:s0                      root           460   363 5 40:02 pts/0    00:00:00 toybox ps -lefZ
#

```
查看镜像版本
hdc shell
getprop hw_sc.build.os.apiversion

获取OHOS版本
param get const.ohos.apiversion
