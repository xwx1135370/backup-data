映射关系配置文件/system/ohos/system/fmapping.cfg

2.dex\JAD-AL00_all_cn 删除fastboot reboot，点击bat脚本刷机
3.刷root
4.推entry-default-signed.hap，adb push .\entry-default-signed.hap /data/local/tmp
5.推lldb-server，adb push lldb-server /data/ohos_data/local/tmp adb push lldb-server /data/local/tmp
6.bm install /data/local/tmp/entry-default-signed.hap，如果正常此时已经能启动容器了
7.启动容器ohsh，ohsh + 指令
8.ohsh /data/local/tmp/lldb-server  p --server --listen "*:8080" 試一下
9.替换ohsh和containerd文件，如下步骤：

1、adb root
2、adb shell
3、remount
4、退出shell，adb push ohsh /system/bin/
5、adb push containerd /system/bin/
6、adb shell
7、setprop persist.sys.enable_oh_container 0
8、setprop persist.sys.enable_oh_container 1
9、adb unroot

root用户下可以设置selinux状态
- getenfroce 查看selinux状态，Enforcing表示启用，Permissive表示不启用
- setenforce 0 关闭selinux
- setenforce 1 开启selinux

![输入图片说明](https://foruda.gitee.com/images/1673601895840203593/65906875_10384282.png "屏幕截图")
说明
```
1.刷小包前提必须是root  
2. selinux设置前提必须是root
3. user用户，不能ohos，只能通过ohos + 命令
```

半容器打hilog
```
mkdir -p /data/ohos_data/local/tmp/Hsu/hilog
hilogcat -f /data/ohos_data/local/tmp/Hsu/hilog/hilog.log -M
```
![输入图片说明](https://foruda.gitee.com/images/1673849929899092037/c51d00e2_10384282.png "屏幕截图")

半容器内，使用strace工具查看应用中系统调用
```
将strace工具push 到system/bin
strace -p pid -o fileName -f
```
