【SR】：
SR000HPMUA

【需求价值】：
用户使用lldb工具链，当前操作繁琐，文档分散，需要一个调试指导书，提供调试指导

【需求描述】：
提供lldb调试指导
输入：lldb工具链，待调试应用
处理：
1. 提供attach调试指导（代码正常举例即可）
  调试的文件来源
- [ ] DevEco编译的hap包
- attach方式 native调试 （c++调试）
- attach方式 混合调试 （js+c++调试）
- [ ] 自定义源码，使用SDK编译出的可执行文件
- attach方式 c++调试
- file + 可执行文件 c++调试
- [ ] 基于Openharmonys或HOS架构编译出来的子系统组件的hap包
attach方式 c++调试

2. 提供app应用fork开始段的调试指导
 鸿彬或者洪涛还在搞的一个需求，搞完了，直接和他两谁对齐就可以。
3. 提供系统服务的启动调试指导
输出：
断点有效，可以调试
【功能描述】：
可自动从SPLE导出，隶属于功能建模下的功能规格 提供调试指导书

【验收平台】：
Phone/RK3568等

【外部依赖】：
NA

【性能功耗指标】：
NA

【交付链接地址】：
https://gitee.com/openharmony/docs/tree/master/zh-cn/application-dev/tools

【交付日期】
第一版：5月10号之前合入

aa工具添加-N选项：https://gitee.com/openharmony/ability_ability_runtime/tree/master/tools/aa