 **1. Interworking ARM & Thumb** 
ARM（32 位）允许同时使用两种指令集，可能影响 OH LLDB 调试：

- shared library 被 strip 的情况下，无法判断函数使用何种指令集，导致无法反汇编。
- 无法确定指令集的情况下，断点设置可能出错，进而导致 LLDB 单步和 continue 异常。
***1.1 Thumb instruction sets***
 **[ARMv7-A](https://developer.arm.com/documentation/den0013/d/ARM-Architecture-and-Processors/Architectural-profiles) 支持：** 

- ARM instruction sets（指令长度为 32 位
- Thumb instruction sets（指令长度为 16 位或 32 位）

 **Thumb instruction sets 分为：** 

- [Thumb](https://developer.arm.com/documentation/ddi0210/c/CACBCAAE)（指令长度为 16 位）

- Thumb-2（Thumb 的扩展，指令长度为 16 位或 32 位；[ARMv7 支持 Thumb-2；ARMv7-a 是 ARMv7 Application profile](https://developer.arm.com/documentation/den0013/d/ARM-Architecture-and-Processors/Architecture-history-and-extensions/Thumb-2)，所以也支持 Thumb-2）
不特别指明的话，文中的 Thumb 指 Thumb-2。

很多常用的 Thumb 指令（如 add 和 mov）是 16 位的。因此，相比于使用 ARM 指令（长度固定为 32 位），使用 Thumb 指令可以增加 code density（更小的 code size），以及减少 ram 的使用。

以我写的 (demo)[https://gitee.com/hongbinj/oh-ndk-demo/tree/5c601b6/interworking_arm_thumb] 的函数 mul_int 为例，使用 Thumb 指令集时 code size 为 50 字节，使用 ARM 指令集时为 88 字节：

- 编译成 Thumb 指令
```
00007398 <mul_int>:
    7398: 70 b5        	push	{r4, r5, r6, lr}
    739a: 00 29        	cmp	r1, #0
    739c: 13 d0        	beq	0x73c6 <mul_int+0x2e>   @ imm = #38
    739e: 05 46        	mov	r5, r0
    73a0: 08 46        	mov	r0, r1
    73a2: 0c 46        	mov	r4, r1
    73a4: 48 bf        	it	mi
    73a6: 60 42        	rsbmi	r0, r4, #0
    73a8: 46 1c        	adds	r6, r0, #1
    73aa: 00 20        	movs	r0, #0
    73ac: 04 e0        	b	0x73b8 <mul_int+0x20>   @ imm = #8
    73ae: 00 f0 12 e8  	blx	0x73d4 <sub_int>        @ imm = #36
    73b2: 01 3e        	subs	r6, #1
    73b4: 01 2e        	cmp	r6, #1
    73b6: 05 d9        	bls	0x73c4 <mul_int+0x2c>   @ imm = #10
    73b8: 29 46        	mov	r1, r5
    73ba: 00 2c        	cmp	r4, #0
    73bc: f7 d4        	bmi	0x73ae <mul_int+0x16>   @ imm = #-18
    73be: 00 f0 0e e8  	blx	0x73dc <add_int>        @ imm = #28
    73c2: f6 e7        	b	0x73b2 <mul_int+0x1a>   @ imm = #-20
    73c4: 70 bd        	pop	{r4, r5, r6, pc}
    73c6: 00 20        	movs	r0, #0
    73c8: 70 bd        	pop	{r4, r5, r6, pc}
```
- 编译成ARM指令
```
00007398 <mul_int>:
    7398: 70 40 2d e9  	push	{r4, r5, r6, lr}
    739c: 00 00 51 e3  	cmp	r1, #0
    73a0: 10 00 00 0a  	beq	0x73e8 <mul_int+0x50>   @ imm = #64
    73a4: 00 50 a0 e1  	mov	r5, r0
    73a8: 01 00 a0 e1  	mov	r0, r1
    73ac: 01 40 a0 e1  	mov	r4, r1
    73b0: 00 00 64 42  	rsbmi	r0, r4, #0
    73b4: 01 60 80 e2  	add	r6, r0, #1
    73b8: 00 00 a0 e3  	mov	r0, #0
    73bc: 03 00 00 ea  	b	0x73d0 <mul_int+0x38>   @ imm = #12
    73c0: 0d 00 00 eb  	bl	0x73fc <sub_int>        @ imm = #52
    73c4: 01 60 46 e2  	sub	r6, r6, #1
    73c8: 01 00 56 e3  	cmp	r6, #1
    73cc: 04 00 00 9a  	bls	0x73e4 <mul_int+0x4c>   @ imm = #16
    73d0: 05 10 a0 e1  	mov	r1, r5
    73d4: 00 00 54 e3  	cmp	r4, #0
    73d8: f8 ff ff 4a  	bmi	0x73c0 <mul_int+0x28>   @ imm = #-32
    73dc: 08 00 00 eb  	bl	0x7404 <add_int>        @ imm = #32
    73e0: f7 ff ff ea  	b	0x73c4 <mul_int+0x2c>   @ imm = #-36
    73e4: 70 80 bd e8  	pop	{r4, r5, r6, pc}
    73e8: 00 00 a0 e3  	mov	r0, #0
    73ec: 70 80 bd e8  	pop	{r4, r5, r6, pc}
```
*** 1.2 Interworking
(Interworking)[https://developer.arm.com/documentation/dui0471/m/interworking-arm-and-thumb/about-interworking] 是指同时使用 ARM 和 Thumb 指令。本文只考虑以函数为最小单位的情况：

- ARM 函数调用 Thumb 函数。
- Thumb 函数调用 ARM 函数。
(调用不同指令集的函数时，CPU state 会转换)[https://developer.arm.com/documentation/dui0473/m/overview-of-the-arm-architecture/changing-between-arm--thumb--and-thumbee-state]：
> A processor that is executing ARM instructions is operating in ARM state. A processor that is executing Thumb instructions is operating in Thumb state.
State 转换需要借助特殊的函数调用和返回指令。用 (blx)[https://developer.arm.com/documentation/dui0489/i/arm-and-thumb-instructions/blx] 调用函数时：

- `blx label` (pc-relative) 翻转 state，state 从 ARM 切换成 Thumb，或从 Thumb 切换成 ARM。
- `blx Rm` (register-relative) 根据寄存器值的最后一位确定 state（state 可能不变）。最后一位是 0 时，state 变成 ARM；最后一位是 1 时，state 变成 Thumb。
将转换前的 state 编码到寄存器 `r14`（`r14` 用作 link register，别名为 lr；保存 call site 的下一条指令，用于返回 caller）。最后一位为 0 表示转换前的 state 为 ARM，1 表示 Thumb state。
 **用 bx 或 pop 返回 caller 时：** 

根据寄存器 lr 的值最后一位选择 state。

 **Demo 中一个 interworking 的例子：** 

- 函数 main 用 ARM 指令集编译，调用函数 mul_int。
- mul_int 用 Thumb 指令集编译，调用函数 sub_int。
- sub_int 用 ARM 指令集编译。

下图展示了上述例子的汇编代码和 state：
![输入图片说明](https://foruda.gitee.com/images/1693365408542427191/35501502_10384282.png "屏幕截图")

### 2. Interworking 对 LLDB 的影响
#### 2.1 LLDB 无法识别 OH 32 位动态库的指令集
##### 2.1.1 Software breakpints
LLDB 实现代码断点的方式之一是 (software breakpoint)[https://en.wikipedia.org/wiki/Breakpoint#Software]：

- 假设在内存 0xf4476950 上设置代码断点。
- LLDB 读取内存 0xf4476950 的值（汇编代码的二进制表示），并保存。
- LLDB 改写内存 0xf4476950 的值，设置为某个特殊指令（例如 x86 和 x86_64 硬件平台的 int3）。执行该指令会触发硬件中断或异常，进入操作系统内核，内核通知 LLDB 代码断点命中。
- LLDB 将内存 0xf4476950 恢复成正常代码。
上面两张图片源自 OALabs 的视频 (What is a Breakpoint - Debugging Explained)[https://www.youtube.com/watch?t=36&v=PVnjYgoX_ck&embeds_referring_euri=https%3A%2F%2Fwiki.huawei.com%2F&source_ve_path=OTY3MTQ&feature=emb_imp_woyt]

##### 2.1.2 识别函数的指令集
ARM Linux 内核定义了三个指令触发 software breakpoint：

- ARM 0x07f001f0
- Thumb 0xde01
- Thumb-2 0xf7f0a000
LLDB 在 ARM Linux 操作系统上使用了 ARM 0xe7f001f0（(Linux 忽略高 4 位)[https://gitee.com/openharmony/kernel_linux_5.10/blob/47adcf1/arch/arm/kernel/ptrace.c#L213]）和 Thumb 0xde01 来设置 software breakpoint，见 (NativeProcessLinux.cpp)[https://gitee.com/openharmony/third_party_llvm-project/blob/1c08bbc/lldb/source/Plugins/Process/Linux/NativeProcessLinux.cpp#L1505]。LLDB 在设置代码断点之前，要识别代码的指令集，因为 LLDB 使用的断点指令不兼容。

函数使用的指令集被编码在 elf 文件中，参考 (ELF for the Arm® Architecture)[https://github.com/ARM-software/abi-aa/blob/617079d8a0d45bec83d351974849483cf0cc66d5/aaelf32/aaelf32.rst]：

- 通过 symbol table (section .symtab) 判断。symbol type 为 STT_FUNC，(value 最后一位为 0 表示代码使用 ARM 指令，1 表示使用 Thumb)[https://github.com/ARM-software/abi-aa/blob/617079d8a0d45bec83d351974849483cf0cc66d5/aaelf32/aaelf32.rst#553symbol-values]。
- [symbol table 有特殊符号][https://github.com/ARM-software/abi-aa/blob/617079d8a0d45bec83d351974849483cf0cc66d5/aaelf32/aaelf32.rst#555mapping-symbols]，记录了代码使用的指令集。有三种特殊符号，如下表所示。
|  Name |Meaning   |
|---|---|
| $a or $a.XXX  |Start of a sequence of Arm instructions   |
| $d or $d.XXX  |Start of a sequence of data items (for example, a literal pool)   |
|$t or $t.XXX   | Start of a sequence of Thumb instructions  |
例如 1.2 Interworking 的例子 symbol table：
```
Symbol table '.symtab' contains 296 entries:
   Num:    Value  Size Type    Bind   Vis       Ndx Name
...
  186: 00007398     0 NOTYPE  LOCAL  DEFAULT     15 $t.0
  187: 000073ca     0 NOTYPE  LOCAL  DEFAULT     15 $t.1
...
  189: 000073d4     0 NOTYPE  LOCAL  DEFAULT     15 $a.0
  190: 000073dc     0 NOTYPE  LOCAL  DEFAULT     15 $a.1
...
  291: 00007399    50 FUNC    GLOBAL DEFAULT     15 mul_int
  292: 00000000     0 FUNC    GLOBAL DEFAULT    UND printf
  293: 000073cb     8 FUNC    GLOBAL DEFAULT     15 abs_wrapper
  294: 000073d4     8 FUNC    GLOBAL DEFAULT     15 sub_int
  294: 000073dc     8 FUNC    GLOBAL DEFAULT     15 add_int
```
对于 executable 和 shared library，还有一些地方区分 ARM 和 Thumb：

- executable elf header (e_entry)[https://github.com/ARM-software/abi-aa/blob/617079d8a0d45bec83d351974849483cf0cc66d5/aaelf32/aaelf32.rst#52elf-header]，通过最后 1 位区分。例如，Entry point address = 0x2F98。
```
ELF Header:
  Magic:   7f 45 4c 46 01 01 01 00 00 00 00 00 00 00 00 00
  Class:                             ELF32
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              DYN (Shared object file)
  Machine:                           ARM
  Version:                           0x1
  Entry point address:               0x2F98

```
- shared library dynamic symbols，相同的区分方式。
```
Symbol table '.dynsym' contains 1022 entries:
   Num:    Value  Size Type    Bind   Vis       Ndx Name
...
   486: 0020b9c5    40 FUNC    GLOBAL DEFAULT    15 panda::ecmascript::Method::ParseFunctionName() const
...
```
##### 2.1.3 影响
OpenHarmony RK3568 镜像的内置动态库（例如 /system/lib/platformsdk/libark_jsruntime.so）没有 symbol table，内部函数没有符号，直接导致：

- LLDB 无法识别 Thumb 指令。
- LLDB 无法反汇编系统 so（llvm-objdump 和 objdump 有同样的问题）。
 **当 LLDB 设置断点或者执行单步命令（LLDB 内部在执行单步时会设置内部断点）时，可能：** 

- 代码断点目标是 Thumb 指令，但是 LLDB 认为是 ARM。
- LLDB 将目标代码改为 0xe7f001f0。
- CPU 执行被修改的代码，结果不符合预期。
一个实际例子是，假设应用进程空间 0xf4476950 指向 /system/lib/platformsdk/libark_jsruntime.so 的 panda::Callback::RegisterCallback(panda::ecmascript::EcmaRuntimeCallInfo*) （相对地址为 0x00276950）。在没有符号表时，LLDB 设置（错误）断点的日志为：
```
1686565416.340970039 NativeProcessProtocol.cpp:SetSoftwareBreakpoint              addr = 0xf4476950, size_hint = 4
1686565416.341304064 NativeProcessProtocol.cpp:EnableSoftwareBreakpoint           Overwriting bytes at 0xf4476950: 0x6, 0x46, 0x99, 0xf8
1686565416.341737986 NativeProcessProtocol.cpp:EnableSoftwareBreakpoint           addr = 0xf4476950: SUCCESS
```
在 LLDB 上设置 settings append target.exec-search-paths /path/to/libark_jsruntime_dir，加载有符号表的 so 后，LLDB 可以正确设置断点：
```
1686566115.640642881 NativeProcessProtocol.cpp:SetSoftwareBreakpoint              addr = 0xf4476950, size_hint = 2
1686566115.641000986 NativeProcessProtocol.cpp:EnableSoftwareBreakpoint           Overwriting bytes at 0xf4476950: 0x6, 0x46
1686566115.641339064 NativeProcessProtocol.cpp:EnableSoftwareBreakpoint           addr = 0xf4476950: SUCCESS
```
上述例子，修改前的代码:
```
00276858 <panda::Callback::RegisterCallback(panda::ecmascript::EcmaRuntimeCallInfo*)>:
  ...
  27694e: 47d0         	blx	r10
  276950: 4606         	mov	r6, r0
  276952: f899 00c9    	ldrb.w	r0, [r9, #201]
  ...
```
修改后的代码中包含跳转指令，实际现象是应用一直执行某个 C++ 函数：
```
  27694e: 47d0         	blx	r10
  276950: 01f0         	lsls	r0, r6, #7
  276952: e7f0         	276952: e7f0         	b	0x276936 <panda::Callback::RegisterCallback(panda::ecmascript::EcmaRuntimeCallInfo*)+0xde> @ imm = #-32
  276954: 00c9         	lsls	r1, r1, #3
```
##### 2.1.4 方案
方案 1（Android），保留个别 so 的符号表（在编译构建上保留32位动态库的符号表.symtab）。Android 12 上至少以下 so 保留符号表（只列出 32 位，实际上 64 位也保留）:

- /apex/com.android.runtime/bin/linker, /apex/com.android.runtime/lib/bionic/libc.so
- /apex/com.android.art/lib/libart.so, /apex/com.android.art/lib/libartbase.so, /apex/com.android.art/lib/libprofile.so, /apex/com.android.art/lib/libdexfile.so

方案2：读取.gnu_debugdata。
 读 .gnu_debugdata 需要引入 libLZMA（编译选项LLDB_ENABLE_LZMA）https://gitee.com/openharmony-sig/chromium_third_party_minizip

### A. 问题复现
#### A.1 DevEco + RK 3568
步骤：

- 在 RK 3568 上烧录镜像（下载地址）。
- 用 DevEco 打开（或新建） native 应用 demo 工程，启动 native 调试，点击应用显示的 "Hello world"。
- DevEco 显示命中断点后，执行两次 step-out 命令即可复现问题。
#### A.2 Android Studio + armeabi-v7a